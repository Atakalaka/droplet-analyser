#%%
#Cellule importation
pathExperimentMD=""
import json
from importlib import reload
from os import makedirs, path, listdir
from sys import exc_info
from pandas.io.parsers import read_csv
from random import triangular

import numpy as np
import pandas as pd
import plotly.graph_objects as go
from numpy import linspace, log

import functionsArthur as fArt
import metaParametersSheet as metaParam  # paramètres qui ne dépendent pas de l'expérience

def convertDroplet_id(id):
# Pour convertir les numéros de goutte en int, ce qui n'est pas le cas par défaut avec le nouveau script de Guilhem.
    return int(float(id))

# %%
def extract(file):
    """Extract the binary file and return a pandas DataFrame.

    Args:
        file (str): A binary file with records of 26 bytes. Little endian.
            0-4: Time (in ms)
            4-14: Photomultipliers (in mV)
            14-24: Photodiodes (in mV)
            24-26: temperature (in 1/100C)
    """

    BINRAW_FORMAT = np.dtype([('time', '<i4'), ]+[(x, '<i2') for x in ['pmt1', 'pmt2', 'pmt3',
        'pmt4', 'pmt5', 'ptd1',
        'ptd2', 'ptd3', 'ptd4',
        'ptd5', 'temperature']])


    data = pd.DataFrame(np.fromfile(file, dtype=BINRAW_FORMAT))

    # Convert time to seconds:
    data['time'] /= 1000

    # Convert measures to volt:
    data.loc[:, ['pmt1', 'pmt2', 'pmt3', 'pmt4', 'pmt5',
                 'ptd1', 'ptd2', 'ptd3', 'ptd4', 'ptd5']] /= 1000

    # Convert temperatures to degrees:
    data['temperature'] /= 100
    return data

donneesRun=extract("C:\\Users\\Arthur\\ownCloud\\ExperiencesMD_Arthur\\Pikovskaya5gl_Pyra1prc_Resa5prm\\EXP20200810_1750 - {PS 030919} et {PS 251119} - Pikov Albatros - ALIGNEMENT\\Raw0019.raw")

#%%
fig = go.FigureWidget(layout=go.Layout(
    template='seaborn',
    xaxis=dict(zeroline=False,dtick=5, title='axe X'),
    yaxis=dict(zeroline=False, title = 'axe Y'),
    title={'text': 'Titre'}))

min=donneesRun["time"].min()

xAxis=(donneesRun["time"]-min)/60
yAxis1=donneesRun["pmt1"]
# yAxis2=donneesRun["pmt2"]
# yAxis3=donneesRun["pmt3"]
yAxis4=donneesRun["ptd5"]
yAxis5=donneesRun["pmt5"]

fig.add_trace(go.Scatter(
    x=xAxis,
    y=yAxis1,
    name='fluo1'))

fig.add_trace(go.Scatter(
    x=xAxis,
    y=yAxis4,
    name='detection_scattering'))

fig.add_trace(go.Scatter(
    x=xAxis,
    y=yAxis5,
    name='scattering'))

fig.show(renderer=metaParam.renderer)
# %%
# Pour renommer des fichiers par le numéro de goutte correspondant (pour les runs pairs)
from os import rename, listdir, makedirs, path

path="toto/Pictures/Run 68"
arrayDir=listdir(path)
print(arrayDir)

dropID=1
for file in arrayDir:
    rename(path+'/'+file,path+'/drop '+str(dropID)+'.jpg')
    dropID+=1
#%%
#Création de courbes de croissance artificielles 
"""
    "colToUseResa": [
        "run",
        "fluo_2_median",
        "fluo_2_std",
        "time",
        "droplet_id"
    ],
"""

# param=json.load(open("/Users/arthurgoldstein/Desktop/experiencesMilliDrop/EXPSimulation/analyseArthur/parameters.json"))
param=json.load(open("C:/Users/Arthur/Desktop/EXPSimulee/analyseArthur/parameters.json"))

figRegression=go.FigureWidget(layout=go.Layout(
    template='seaborn',
    width=metaParam.W, height=metaParam.H,
    xaxis=dict(zeroline=False,dtick=6, title='temps en heures'),
    yaxis=dict(zeroline=False, title = 'fluo 2'),
    title={'text': 'Titre'}))

realParametersArray={}

for ind in range(1,100):
    droplet_id=ind

    lag=triangular(5,36,12)
    a=triangular(0.4,10,1.5)
    epsilon=triangular(-0.1,0.1,0)
    lagTime=lag+(a/log(2)*log(param['seuilRegression']-epsilon))

    realParametersArray[droplet_id]=lag,a,epsilon,lagTime

    runRow=linspace(1,100, num=100)
    timeRow=0.5*runRow
    fluoRow=2**((timeRow-lag)/a) + epsilon

    extraInfo=str("lag : {} | TDiv : {} | epsi : {} <br> lagTime : {}").format(str(round(lag,3)),str(round(a,3)),str(round(epsilon,3)),str(round(lagTime)))

    figRegression.add_trace(go.Scatter(
        x=timeRow,
        y=fluoRow,
        name="drop " + str(droplet_id),
        hovertemplate =extraInfo
    ))

#Export en JSOn des paramètres réels
fname = "C:/Users/Arthur/Desktop/EXPSimulee/analyseArthur/realParametersArray.json"
with open(fname, 'w') as outfile:
    json.dump(realParametersArray, outfile, indent=3)

figRegression.update_layout(updatemenus=param['updatemenus'])

figRegression.show(renderer=metaParam.renderer)

# %%
#lancé de la régression sur toutes les manips. 
metaParam=reload(metaParam)

#On liste les expériences
folders=[]
for rootFolder in metaParam.rootFolders:
    folders = folders + [rootFolder +'/'+ f for f in listdir(rootFolder)]

for experimentPath in folders :
    experimentName=experimentPath.split('/')[-1] #juste la dernière partie du chemin
    if not ("CONTAMINE" in experimentName or "ECHEC" in experimentName or "ALIGNEMENT" in experimentName or "FAIBLE SCATTERING" in experimentName) and '{' in experimentName :
        print(experimentName)
        try :
            param=json.load(open(experimentPath+'/analyseArthur/parameters.json'))
            metaData=fArt.importMetadata(experimentPath)
            refDrop=json.load(open(experimentPath+"/analyseArthur/referenceDrops.json"))
            netDF=pd.DataFrame(pd.read_csv(experimentPath+'/analyseArthur/netDF.csv', sep=","))
            growingDrops=fArt.lookforGrowingDrops(netDF,metaData,metaParam.tMaxUniqueResa, canal="fluo_2_median", upThreshold=param['seuilRegression'])

            fArt.regression(experimentPath, netDF, growingDrops, param, metaParam, metaData, displayCurves=False, askOverwrite = False, checkStandardDev=False)
        except:
            print('Erreur sur la manip '+experimentName)
            print(exc_info()[1])

# %%
#quantité de particules 
dictExp={}
folders=[]
for rootFolder in metaParam.rootFolders:
    folders = folders + [f for f in listdir(rootFolder)]    

for nomExperience in folders:
    #Si l'expèrience contient 'CONTAMINE', on ne la compte pas
    if not ("CONTAMINE" in nomExperience or "ECHEC" in nomExperience or "ALIGNEMENT" in nomExperience):
        #On crée un array avec tous les noms entre accolades (1 ou 2 en général)
        print(nomExperience)
        if "Dindon" in nomExperience or "Albatros" in nomExperience:
            dictExp[fArt.getSimplePath(nomExperience)]=5
        else :
            dictExp[fArt.getSimplePath(nomExperience)]=1

# %%
# régression sur scattering
metaParam, fArt = reload(metaParam), reload(fArt)
pathOfCurrentExperiment=fArt.choixDossier(pathMilliDrop=metaParam.pathMilliDrop)

try :
    param=json.load(open(pathOfCurrentExperiment+'/analyseArthur/parameters.json'))
    metaData=fArt.importMetadata(pathOfCurrentExperiment)
    netDFScatt=pd.DataFrame(pd.read_csv(pathOfCurrentExperiment+'/analyseArthur/netDFScatt.csv', sep=","))
    netDFResa=pd.DataFrame(pd.read_csv(pathOfCurrentExperiment+'/analyseArthur/netDF.csv', sep=","))
    growingDrops=fArt.lookforGrowingDrops(netDFScatt,metaData,metaParam.tMaxUniqueResa,upThreshold=0.05, canal='scattering_median')

    fArt.regression(pathOfCurrentExperiment, netDFScatt, growingDrops, param, metaParam, metaData, channel="scattering_median" ,displayCurves=True, askOverwrite = True, checkStandardDev=False)

except:
    print("\nErreur")
    exception_type, exception_object, exception_traceback = exc_info()
    filename = exception_traceback.tb_frame.f_code.co_filename
    line_number = exception_traceback.tb_lineno
    print("Exception type: ", exception_type)
    print("Line number: ", line_number)
# %%
# Comparaison des temps de sortie résazurine et scattering, pour des manips sans particules
metaParam, fArt = reload(metaParam), reload(fArt)
pathOfCurrentExperiment=fArt.choixDossier(pathMilliDrop=metaParam.pathMilliDrop)

upThresholdScatt = 0.05

try :
    # param=json.load(open(pathOfCurrentExperiment+'/analyseArthur/parameters.json'))
    metaData=fArt.importMetadata(pathOfCurrentExperiment)
    # refDrop=json.load(open(pathOfCurrentExperiment+"/analyseArthur/referenceDrops.json"))
    netDFScatt=pd.DataFrame(pd.read_csv(pathOfCurrentExperiment+'/analyseArthur/netDFScatt.csv', sep=","))
    netDFResa=pd.DataFrame(pd.read_csv(pathOfCurrentExperiment+'/analyseArthur/netDF.csv', sep=","))
    growingDrops=fArt.lookforGrowingDrops(netDFScatt,metaData,metaParam.tMaxUniqueResa, upThreshold=0.05, canal='scattering_median')

    for drop in growingDrops:
        DFOverThreshold=netDFScatt[(netDFScatt.droplet_id == drop) * (netDFScatt.scattering_median > upThresholdScatt)]

except:
    print("\nErreur")
    exception_type, exception_object, exception_traceback = exc_info()
    filename = exception_traceback.tb_frame.f_code.co_filename
    line_number = exception_traceback.tb_lineno
    print("Exception type: ", exception_type)
    print("Line number: ", line_number)

#%%Fusion des bases de données
# On fusionne la base TimeOut avec les résultats des régressions et les minima trouvés avec histogrammesMinima
# Création et export d'une base de donnée contenant toutes les 
# informations connues pour chaque goutte : temps aux seuils, 
# temps de division, nom du lot, minimum des signaux de pyra
# et scattering...
DFTimeOut=pd.DataFrame(read_csv(metaParam.pathAnalyseTemporelle+'/dataFrameTimeOut.csv'))
DFMinima=pd.DataFrame(read_csv(metaParam.pathAnalyseTemporelle+'/DFMinima.csv'))
DFRegression=pd.DataFrame(read_csv(metaParam.pathMilliDrop+'/recapitulatifRegressions/mainregression2021-05-31.csv'))

for i, row in DFTimeOut.iterrows():
    DFTimeOut.at[i, 'EXP'] =row["EXP"][3:11]
for i, row in DFMinima.iterrows():
    DFMinima.at[i, 'EXP'] =row["EXP"][3:11]

DFMinima.rename(columns={"goutte":"droplet_id"}, inplace=True)
DFTimeOut.rename(columns={"goutte":"droplet_id", "name":"SolDilution"}, inplace=True)
DFRegression.rename(columns={'Experience':'EXP', "goutte":"droplet_id"}, inplace=True)

uniqueDF=pd.merge(DFMinima,DFTimeOut,on=("EXP", "droplet_id"), how="outer")

uniqueDF=uniqueDF.astype({'EXP':"float64"})
DFRegression=DFRegression.astype({'droplet_id': 'float64'})

finalDF=pd.merge(uniqueDF, DFRegression, on=("EXP", "droplet_id", "SolDilution"), how="outer")

finalDF.drop(["Unnamed: 0_x", "Unnamed: 0_y"], axis=1, inplace=True)

finalDF.to_csv(metaParam.pathAnalyseTemporelle+'/mainBaseDeDonneesParGoutte.csv')
# %% Animation : 
import plotly.express as px
df = px.data.gapminder()
px.scatter(df, x="gdpPercap", y="lifeExp", animation_frame="run", animation_group="country",
            color="continent", hover_name="country",
            log_x=True, size_max=55, range_x=[100,100000], range_y=[25,90])
#%%
#Suppression des runs problématiques

runToignore=[8,9,50,51]

pathOfCurrentExperiment=fArt.choixDossier(pathMilliDrop=metaParam.pathMilliDrop)

metaData=fArt.importMetadata(pathOfCurrentExperiment)
dir_path = path.dirname(path.realpath(__file__)) #chemin des scripts python

if path.exists(pathOfCurrentExperiment+'/analyseArthur/parameters.json'):
    print('You formerly saved parameters for this experiment. We are loading them.') 
    chosenPath=pathOfCurrentExperiment+'/analyseArthur'

else: #S'il n'y a pas de parameters.json, on charge ceux par défaut
    print('No parameters found. Loading the default ones.') 
    chosenPath=dir_path

param=json.load(open(chosenPath+'/parameters.json'))

try: 
    mainDataFrame=pd.DataFrame(pd.read_csv(pathOfCurrentExperiment+"/analysis/droplet_dynamics.csv", sep=",",
        converters={'droplet_id':convertDroplet_id},
        usecols=param['colToUse']))

except:
    mainDataFrame=pd.DataFrame(pd.read_csv(pathOfCurrentExperiment+"/analysis/droplet_dynamics.csv", sep=";",
        converters={'droplet_id':convertDroplet_id},
        usecols=param['colToUse']))


for runToI in runToignore:
    mainDataFrame=mainDataFrame[mainDataFrame.run != runToI]

mainDataFrame.to_csv('{}/analysis/droplet_dynamics.csv'.format(pathOfCurrentExperiment))
# %%
