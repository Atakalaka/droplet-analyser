# Droplet Analyser

This software is meant to produce nice plots of measurement data from millifluidic devices such as MilliDrop analyzers. 
It also allows for exponential and logistic fitting of the curves. 

It uses plotly.

## Getting Started

-Install python 3 <br>
-Install all the packages in the requirement.txt file <br>
-Change the "pathMilliDrop" variable in the metaParametersSheet<br>

-AffichageCourbesMD.py is to be considered as the "main", almost every features for data visualization and curve fitting are in there. 
-metaParametersSheet is a file made to specify data that are not related to the experiment you want to study: the dimension of the plots (depends on you screen), the functionnalities you want to use (curve fitting, data visualization...) and the path where your experiments are saved (although this is facultative)
-giveReferenceDrop.py is made to tell the software what drop is to be used as reference (that is as non-growing drop) for the fitting. It saves the drop in a separate file in the folder so you only have to run it once, just before launching the first fitting (you will need to re-run cell 1 after you specify reference drop in order to generate a "net dataframe", though)
-mergeAndExportRegResults merges the curve fitting results of every experiment in the metaParam.rootFolders.
-functionsArthur.py contains every functions used elsewhere

For now, every other file must be considered as work in progress or files that you should not change.

Some restrictions : 
-Soil names must not contain more than two "-" in total, else the regression results won't be taken into account in the merge results

## Authors

* **Arthur Goldstein** - *Initial work* - [Atakalaka](https://gitlab.com/Atakalaka)