#%%
#Ce script permet d'afficher l'apparition de colonies et de halos sur boîte au cours
#du temps, ainsi que l'apparition des différents posisitfs en gouttes au cours du temps 
#Cellule importation
import json
from importlib import reload
from sys import exc_info

import pandas as pd
import plotly.graph_objects as go
from numpy import float64

import metaDonneesExperiences as metaD  # informations sur mes expériences MilliDrop
import metaParametersSheet as metaParam  # paramètres qui ne dépendent pas de l'expérience
#%% -------Affichage----------- 
#------------------------------
metaParam=reload(metaParam)
metaD=reload(metaD)
dropsPerCondition=json.load(open(metaParam.pathMilliDrop+'/recapitulatifRegressions/comptage/dropsPerCondition.json'))

class lineAppearance:
    """
    Cette classe sert à générer un dictionnaire d'apparence des lignes : leur couleur et leurs éventuels pointillés
    """
    def __init__(self,dilution,color,conc):
        self.dilution=dilution
        self.color=color
        self.concentration=conc

    def appearanceDict(self):
        if self.concentration==5:
            w=5
        else:
            w=3
        if self.dilution == "1_1000":
            return dict(color=self.color, dash='dash', width=w)
        elif self.dilution == "1_10000":
            return dict(color=self.color, dash='dot', width=w)
        else:
            return dict(color=self.color, width=w)

for canal in 'Fluo_1','Fluo_2','Scattering':

    figure = go.Figure(layout=go.Layout(
        template='seaborn',
        font=metaParam.taillePolice,
        width=metaParam.W, height=metaParam.H,
        xaxis=dict(zeroline=False, dtick=12, title="""Temps d'incubation en heures"""),
        yaxis=dict(zeroline=False, title="Fraction des gouttes positives", range=[-0.05,1.05]),
        title={'text': 'Gouttes positives en fonction du temps - '+canal}))

    thresholdTimesDF=pd.DataFrame(pd.read_csv(
        '{}/{}.csv'.format(metaParam.pathAnalyseTemporelle,'positifs'+canal+'VSTemps'),
        sep=","))

    for condition in thresholdTimesDF.experimentID.unique():
        sampleName=condition.split('|')[1].split('-')[0][1:-1] #On extraie le nom de l'échantillon du nom de l'expérience
        dateEXP=condition.split('|')[0].split('_')[0][3:]
        try:
            totalDrops=dropsPerCondition[dateEXP][condition.split(' | ')[1]]
            dilution=condition.split('|')[1].split(' - ')[1]
            try : 
                figure.add_trace(go.Scatter(
                    x=thresholdTimesDF[thresholdTimesDF.experimentID == condition]['time']/3600,
                    y=thresholdTimesDF[thresholdTimesDF.experimentID == condition]['numberOfDrops']/totalDrops,
                    name=condition.split('|')[1].split('-')[0], 
                    hovertemplate=str('Echantillon <b>{}</b> <br>{}<br>{}<br>').format(sampleName,condition.split('|')[0].split('_')[0],condition.split('|')[1].split('-')[1][1:]) + "%{x} heures",
                    line=lineAppearance(dilution,
                        metaD.dictOfSamplesColors[sampleName],
                        metaD.dictOfExpData['EXP'+str(dateEXP)]).appearanceDict(),
                    mode='lines',
                    legendgroup=sampleName #On réunit par sol/souche
                ))
            except: 
                print("Erreur d\'affichage avec la condition "+condition)
                print(exc_info()[0],exc_info()[1],exc_info()[2])
        except:
            print("\nError - ")
            exception_type, exception_object, exception_traceback = exc_info()
            line_number = exception_traceback.tb_lineno
            print("Exception type: ", exception_type)
            print("Line number: ", line_number)
            print("exception_object:",exception_object)
           
    figure.update_layout(
        title={'x':0.4,
            'xanchor': 'center'})
    figure.show(editable = True, renderer=metaParam.renderer)

del figure, totalDrops, dateEXP, thresholdTimesDF, dropsPerCondition

#%% Boîtes de Petri
# --- I) : Importation et étapes préliminaires Petri
metaParam=reload(metaParam)
def convertIntoDateTime(stringDate): #Pour convertir une date Excel en date Python 
    from datetime import datetime
    try:
        dateList=stringDate.split(' ')
        dateList[0]=dateList[0].split('/')
        dateList[1]=dateList[1].split(':')
    except:
        print('Erreur dans la conversion de',stringDate)
    return datetime(day=int(dateList[0][0]),month=int(dateList[0][1]),year=int(dateList[0][2]),hour=int(dateList[1][0]),minute=int(dateList[1][1]))

#Le dataFrame source ; celui qui contient les informations entrées manuellement
DFAnalyseTemporelle=pd.DataFrame(pd.read_csv(
    '/Users/arthurgoldstein/OneDrive/LCMD/facteurTempsComptagePetri_080221.csv', sep=";",
    dtype={'colonies' : float64, 'halos' : float64},
    converters={'dateHeureInoculation':convertIntoDateTime,
                "dateHeureMesure":convertIntoDateTime}))#On utilise la fonction définie plus haut pour convertir les temps CSV en temps python

listOfUniqueBoxes=DFAnalyseTemporelle.clefBoite.unique() #Liste des identifiants de boîtes

# --- II) : Calcule des concentrations en cellules cultivables et en halos

# Des Dataframes destinés à contenir les estimations de concentration, 
# à la fois par expérience et individuellement
DFEstimaConcentrations=pd.DataFrame(columns=['sol', 'date', 'volume' ,'colonies', 'halos'])
DFEstimaConcentrationsUniques=pd.DataFrame(columns=['uniqueID','sol', 'date', 'dilution' ,'concentrationColonies', 'concentrationHalos', "borneConfBasCFU", "borneConfHautCFU", "borneConfBasHalos", "borneConfHautHalos"])

# On crée une colonne avec la durée d'incubation pluitôt que la date
emptyCol=['']*len(DFAnalyseTemporelle)
DFAnalyseTemporelle["dureeIncubation"]=emptyCol #On crée une colonne vide pour stocker la durée d'incubation
DFAnalyseTemporelle["deltaDateDeMesure"]=emptyCol #On crée une colonne vide pour stocker l'écart entre la durée d'incubatione et la durée à laquelle on souhaite garder la valeur pour analyse ultérieure

# On calcule la durée d'incubation.
# On ajoute un 'timeDelta' à chaque ligne du dataFrame : un écart entre la 
# durée d'incubation et la durée d'incubation cible précisée dans metaParam 
for i, row in DFAnalyseTemporelle.iterrows():
    timeDelta=DFAnalyseTemporelle.dateHeureMesure[i]-DFAnalyseTemporelle.dateHeureInoculation[i]
    DFAnalyseTemporelle.at[i,'dureeIncubation']=timeDelta.total_seconds()/3600
    DFAnalyseTemporelle.at[i,'deltaDateDeMesure']=abs(timeDelta.total_seconds()/3600-metaParam.tMinimalPetri*24)

#On convertit en numérique pour éviter un bug 
DFAnalyseTemporelle['dureeIncubation'] = pd.to_numeric(DFAnalyseTemporelle['dureeIncubation'])

DFAnalyseTemporelle['colonies'] = pd.to_numeric(DFAnalyseTemporelle['colonies'])


#%%
# ---------------------------------------------------------------------------
# --- II) : On crée des figures pour présenter les courbes de croissance ----
# ---------------------------------------------------------------------------

figCFU = go.Figure(layout=go.Layout(
    template='seaborn', font=metaParam.taillePolice, width=metaParam.W, height=metaParam.H,
    xaxis=dict(zeroline=False,dtick=1, title='Durée d\'incubation en jours'),
    yaxis=dict(zeroline=False, title = 'Nombre de colonies sur la boîte'),
    title={'text': 'CFU par boîte en fonction du temps'}))

figHalos = go.Figure(figCFU)
figHalos.update_layout(yaxis=dict(zeroline=False, title = 'Nombre de halos sur la boîte'),
    title={'text': 'Halos par boîte en fonction du temps pour différentes expériences'})

arrayFigures=[figCFU, "colonies"],[figHalos, "halos"] #pour gagner un peu de place

dictLines={'1_100':None, '1_1000':'dash', '1_10000':'dot'}

for clef in listOfUniqueBoxes:
    # * (DFAnalyseTemporelle.boite == "1_1000")
    queriedDF=DFAnalyseTemporelle[(DFAnalyseTemporelle.clefBoite == clef) ]
    dilution=queriedDF.boite.unique()[0].split(' |')[0]
    if dilution != '1_10000':
        try : 
            extraInfo=str("Date : {}<br>Sol : <b>{}</b><br>Particules : {}<br>{} boîte<br>Dilution :{}").format(queriedDF.dateHeureInoculation.unique()[0],
                queriedDF.sol.unique()[0] ,queriedDF.concHydroxyapatite.unique()[0],
                queriedDF.tailleBoite.unique()[0], dilution)

            symbol=201 if queriedDF.tailleBoite.unique()[0] == "grande" else 200

            for arrayFigure in arrayFigures:
                arrayFigure[0].add_trace(go.Scatter(
                    mode='lines+markers',
                    x=pd.Series([0]).append(queriedDF["dureeIncubation"])/24,
                    y=pd.Series([0]).append(queriedDF[arrayFigure[1]]),
                    legendgroup=queriedDF.sol.unique()[0], 
                    hovertemplate=extraInfo, 
                    line=dict(dash=dictLines[dilution], color=metaD.dictOfSamplesColors[queriedDF.sol.unique()[0]]),
                    marker_symbol=symbol,
                    marker=dict(size=15),
                    name=clef, showlegend=True))

        except: 
            print('\nErreur pour ' + clef)
            exception_type, exception_object, exception_traceback = exc_info()
            line_number = exception_traceback.tb_lineno
            print("Exception type: ", exception_type)
            print("Line number: ", line_number)
            print(exception_object)

# figCFU.add_shape(
#     # Ligne pour visualiser le timePetriCFU
#     dict(type="line", name="timePetriCFU",
#         x0=metaParam.timePetriCFU, y0=0,
#         x1=metaParam.timePetriCFU, y1=DFAnalyseTemporelle.colonies.max(),
#         line=dict(color="black",width=3)))

# figCFU.add_shape(
#     # Ligne pour visualiser le tMinimalPetri
#     dict(type="line", name="tMinimalPetri",
#         x0=metaParam.tMinimalPetri, y0=0,
#         x1=metaParam.tMinimalPetri, y1=DFAnalyseTemporelle.colonies.max(),
#         line=dict(color="red",width=3)))

figCFU.show(renderer=metaParam.renderer, editable = True)
figHalos.show(renderer=metaParam.renderer, editable = True)
