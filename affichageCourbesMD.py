"""
Ce script permet de travailer sur une expérience donnée. 
Il permet :
 - D'afficher les différentes grandeurs optiques au cours du temps : scattering, fluo_2 (résazurine) et fluo_1
 - De mener une régression sur le signal de fluo_2 (Les résultats sont automatiquement exportés)
 - de calculer les netDF de l'expérience et de les exporter (réalisé automatiquement si l'expérience n'en contient pas)
Il est pensé pour être contrôlé par le fichier metaParam, dans lequel on doit notamment indiquer quels graphes on souhaite représenter
"""
#%%
#Cellule importation
pathOfCurrentExperiment=""
import json
from importlib import reload
from os import makedirs, path
from sys import exc_info
from time import time

import pandas as pd
import plotly.graph_objects as go

import functionsArthur as fArt
import metaParametersSheet as metaParam  # paramètres qui ne dépendent pas de l'expérience
import metaDonneesExperiences as metaD

def convertDroplet_id(id):
# Pour convertir les numéros de goutte en int, ce qui n'est pas le cas par défaut avec le nouveau script de Guilhem.
    return int(float(id))

#%%
#Cellule 1 : choix de l'expérience
pathOfCurrentExperiment=fArt.choixDossier(pathMilliDrop=metaParam.pathMilliDrop)

try:
    metaData=fArt.importMetadata(pathOfCurrentExperiment)
    dir_path = path.dirname(path.realpath(__file__)) #chemin des scripts python

    if path.exists(pathOfCurrentExperiment+'/analyseArthur/parameters.json'):
        print('You formerly saved parameters for this experiment. We are loading them.') 
        chosenPath=pathOfCurrentExperiment+'/analyseArthur'

    else: #S'il n'y a pas de parameters.json, on charge ceux par défaut
        print('No parameters found. Loading the default ones.') 
        chosenPath=dir_path

    param=json.load(open(chosenPath+'/parameters.json'))

    #On affiche les différentes catégories, pour pouvoir les entrer si besoin
    print("""metaData["descriptionToWells"] : """)
    for group in metaData['descriptionToWells']:
        print("- "+group+"\n")

    try: 
        mainDataFrame=pd.DataFrame(pd.read_csv(pathOfCurrentExperiment+"/analysis/droplet_dynamics.csv", sep=",",
            converters={'droplet_id':convertDroplet_id},
            usecols=param['colToUse']))

    except:
        mainDataFrame=pd.DataFrame(pd.read_csv(pathOfCurrentExperiment+"/analysis/droplet_dynamics.csv", sep=";",
            converters={'droplet_id':convertDroplet_id},
            usecols=param['colToUse']))

    try: 
        mainDataFrameFitting=pd.DataFrame(pd.read_csv(pathOfCurrentExperiment+"/analysis/droplet_dynamics.csv", sep=",",
            usecols=param["colToUseResa"],
            converters={'droplet_id':convertDroplet_id}))
    except:
        mainDataFrameFitting=pd.DataFrame(pd.read_csv(pathOfCurrentExperiment+"/analysis/droplet_dynamics.csv", sep=";",
            usecols=param["colToUseResa"],
            converters={'droplet_id':convertDroplet_id}))  

        #On cherche les gouttes qui poussent
        #On va chercher si le dossier expérience contient une goutte de référence

    if fArt.checkReferenceDrop(pathOfCurrentExperiment,metaData) == False:
            print('There is no satisfying reference drop. \nYou can add one with giveReferenceDrop.py script')
            netDFDeclared=False
            netDFScattDeclared=False

    else:
        refDrop=json.load(open(pathOfCurrentExperiment+"/analyseArthur/referenceDrops.json"))
        netDFDeclared=False
        netDFScattDeclared=False
        #On crée un dataframe dans lequel on soustraie la valeur de résa de référence
        try:#On regarde s'il y en a déjà un
            netDF=pd.DataFrame(pd.read_csv(pathOfCurrentExperiment+'/analyseArthur/netDF.csv', sep=",", converters={'droplet_id':convertDroplet_id}))
            netDFDeclared=True #Un booléen pour savoir si on a bien un netDF
        except:
            netDF=fArt.createNetDataFrame(mainDataFrameFitting,refDrop,param['runMax'], toZero = True, firstRun=param["runMin"], newPipeline = path.exists(pathOfCurrentExperiment+"/analysis/temp"))
            netDF.to_csv('{}/analyseArthur/netDF.csv'.format(pathOfCurrentExperiment))
            netDFDeclared=True #Un booléen pour savoir si on a bien un netDF
        
        #on fait la même chose pour le scattering : 
        try:
            netDFScatt=pd.DataFrame(pd.read_csv(pathOfCurrentExperiment+'/analyseArthur/netDFScatt.csv', sep=",", converters={'droplet_id':convertDroplet_id}))
            netDFScattDeclared=True
        except:
            colToUseScatt=['run', 'scattering_median', 'scattering_std', 'time', 'droplet_id']
            try:
                mainDataFrameScatt=pd.DataFrame(pd.read_csv(pathOfCurrentExperiment+"/analysis/droplet_dynamics.csv", sep=",", usecols=colToUseScatt, converters={'droplet_id':convertDroplet_id}))  
            except:
                mainDataFrameScatt=pd.DataFrame(pd.read_csv(pathOfCurrentExperiment+"/analysis/droplet_dynamics.csv", sep=";", usecols=colToUseScatt, converters={'droplet_id':convertDroplet_id}))  

            netDFScatt=fArt.createNetDataFrame(mainDataFrameScatt,
                refDrop, param['runMax'],
                canal="scattering_median", firstRun=param["runMin"],  toZero = True, 
                newPipeline = path.exists(pathOfCurrentExperiment+"/analysis/temp"))
            netDFScatt.to_csv('{}/analyseArthur/netDFScatt.csv'.format(pathOfCurrentExperiment))
            netDFScattDeclared=True
            del mainDataFrameScatt

        #Et pour la pyra : 
        try:
            netDFPyra=pd.DataFrame(pd.read_csv(pathOfCurrentExperiment+'/analyseArthur/netDFPyra.csv', sep=",", converters={'droplet_id':convertDroplet_id}))
            netDFPyraDeclared=True
        except:
            colToUsePyra=['run', 'fluo_1_median', 'fluo_1_std', 'time', 'droplet_id']
            try: #Selon les versions, le séparateur peut être une virgule ou un point virgule
                mainDataFramePyra=pd.DataFrame(pd.read_csv(pathOfCurrentExperiment+"/analysis/droplet_dynamics.csv", sep=",", usecols=colToUsePyra, converters={'droplet_id':convertDroplet_id}))  
            except:
                mainDataFramePyra=pd.DataFrame(pd.read_csv(pathOfCurrentExperiment+"/analysis/droplet_dynamics.csv", sep=";", usecols=colToUsePyra, converters={'droplet_id':convertDroplet_id}))  

            netDFPyra=fArt.createNetDataFrame(mainDataFramePyra,
                refDrop, param['runMax'],
                canal="fluo_1_median", firstRun=param["runMin"],  toZero = True, 
                newPipeline = path.exists(pathOfCurrentExperiment+"/analysis/temp"))
            netDFPyra.to_csv('{}/analyseArthur/netDFPyra.csv'.format(pathOfCurrentExperiment))
            netDFPyraDeclared=True
            del mainDataFramePyra

    del mainDataFrameFitting

except:
    print('Folder is not compatible. Analysis stopped')
    print(exc_info()[0],exc_info()[1],exc_info()[2])

strSamples=''
for sample in fArt.getSoilName(pathOfCurrentExperiment):
    strSamples += ' '+sample

#%%
# ------------------------------------------
# Cellule 2 : tracé des graphes principaux et affichage dans le browser
#-------------------------------------------
fArt=reload(fArt)
metaParam=reload(metaParam)

#Pour info : 
print("Expérience : ")
print(pathOfCurrentExperiment)

#Création de la chaîne de légende de l'axe vertical
axeOrdonnees=""
if "5" in metaParam.activatedChannels:
    axeOrdonnees+="Lumière Diffusée"
    if "1" in metaParam.activatedChannels or "2" in metaParam.activatedChannels:
        axeOrdonnees+=" | Fluorescence"
else:
    axeOrdonnees+="Fluorescence"

listDrops=fArt.dropsWell(param['listOfDropsAndWells'],pathOfCurrentExperiment,metaData)

strTitle='Gouttes - microbes du sol\n'
for canal in metaParam.activatedChannels:
    strTitle+=" - "+param['dictChannels'][canal][2]
strTitle+=' | '+fArt.getSimplePath(pathOfCurrentExperiment)+'<br>Echantillons  : ' + strSamples 

if metaParam.drawMainPlot==True:
    try: #Mesure du temps d'execution
        startTime=time()
    except:
        pass

    idColor=0
    #On crée une figure pour le graphe principal
    fig = go.FigureWidget(layout=go.Layout(
            font=metaParam.taillePolice, autosize=False,
            template='seaborn',
            width=metaParam.W, height=metaParam.H,
            xaxis=dict(zeroline=False, dtick=6, title="""Temps d'incubation en heures"""),
            yaxis=dict(zeroline=False, title=axeOrdonnees, range=[0,5]),
            title={'text': strTitle}))

    for dropArray in listDrops:
        #On initialise un booléen pour ne montrer que la légende de la première goutte de chaque groupe
        firstDrop=True
        for drop in dropArray:
            #On crée une chaîne de query qui servira à faire une requête sur le dataFrame principal : 
            queryString="droplet_id == {0} & run > {1} & time < {2}".format(str(drop),str(param['runMin']),str(param['maxTime']*3600))
            
            #Un colorgroupe pour plotly : 
            nomDuGroupe=metaData['wellToDescription'][metaData['dropletToWell'][str(drop)]]

            #On traces la ou les lignes du graphe principal
            for canal in metaParam.activatedChannels:
                extraInfo=mainDataFrame.query('droplet_id == '+str(drop))["run"]
                try: 
                    xAxis=(mainDataFrame.query(queryString)["time"])/3600
                    yAxis=mainDataFrame.query(queryString)[param['dictChannels'][canal][0]]

                    fig.add_trace(go.Scatter(
                        x=xAxis, y=yAxis, text=extraInfo,
                        hovertemplate =
                            'Run: %{text}<br>Time: %{x}<br>Value: %{y}<br>Drop: '+str(drop),
                        legendgroup=str(dropArray)+' '+canal,
                        # legendgroup=str(drop),
                        error_y=dict(
                            type='data',
                            array=mainDataFrame.query(queryString)[param['dictErrorBars'][canal][0]],
                            visible=True
                            )if param['errorBarsActivated'] else dict(), #Un petit ternaire
                        line=dict(color=metaParam.matColors[idColor], width=4,dash=param['dictChannels'][canal][1]),
                        name=nomDuGroupe,
                        showlegend=True if firstDrop else False
                        ))
                    firstDrop=False
                except:
                    print("\nErreur avec la "+str(param['dictChannels'][canal][0])+" de la goutte "+str(drop))
                    print("Info sur l'erreur :", exc_info()[0],exc_info()[1],exc_info()[2])
        
        if idColor < len(metaParam.matColors):
            idColor+=1

    #On ajoute le boutton log/lin sur la figure
    if metaParam.displayLinLogButton:
        fig.update_layout(updatemenus=param['updatemenus'])

    #Réglage de l'échelle
    if param['scaleV'] != 'auto':
        fig.update_yaxes(range=param['scaleV'])

    # On affiche la figure - "renderer" précise le mode d'affichage, par exemple "browser"
    # Voir https://plot.ly/python/renderers/
    fig.show(renderer=metaParam.renderer, config={'editable': True})
    del fig

    try:
        #Mesure et affichage du temps d'execution
        endTime=time()
        print('\nExecution time of main loop : '+ str(round(endTime-startTime,3))+' seconds')
    except:
        pass

#-----------------------------------------------------
#--------- Affichage fluo 1 vs scattering ------------
#Si fluo1VsScattOn est activé, on crée une figure pour le graphe concerné
if metaParam.fluo1VsScattOn==True:
    nomDuLot=pathOfCurrentExperiment.split('/')[-2]
    seuilScatt, seuilF1 =metaD.dictOfEXPFolders[nomDuLot]['seuilScatt'], metaD.dictOfEXPFolders[nomDuLot]['seuilFluo1']
    fArt.drawVSplot(pathOfCurrentExperiment, 
        mainDataFrame, 
        param, 
        metaParam, 
        listDrops, 
        metaData, 
        seuilScatt, 
        seuilF1, 
        tMax=38, 
        normalizeScatt=False, 
        randomizeCol=True)
# pathOfCurrentExperiment, mainDataFrame, param, metaParam, listDrops, metaData, seuilScattering, seuilFluo1, normalizeScatt=False

#--------- Display of Fluo2 'net signal'----------
#-------------------------------------------------

if metaParam.displayNetFluo2Signal==True:
    idColor=0

    if fArt.checkReferenceDrop(pathOfCurrentExperiment,metaData) == False:
        print('There is no satisfying reference drop. Resazurin analysis aborted.\nYou can add one with giveReferenceDrop.py script')
    elif netDFDeclared==False:
        print('Please relaunch first cell to create a net DataFrame')
    else:
        dropRef=json.load(open(pathOfCurrentExperiment+"/analyseArthur/referenceDrops.json"))
    
        queryStringReference="droplet_id == {0} & run > {1} & time < {2}".format(dropRef,str(param['runMin']),str(param['maxTime']*3600))
        dfReference=mainDataFrame.query(queryStringReference).reset_index(drop=True)
        figAnalyseResa=go.Figure(layout=go.Layout(
            autosize=False,
            font=metaParam.taillePolice, width=metaParam.W, height=metaParam.H,
            template='seaborn',
            xaxis=dict(zeroline=False, dtick=6,title="""Temps d'incubation en heures"""),
            yaxis=dict(zeroline=False, title='Fluorescence de la résazurine "nette"    -'),
            title={
            'text': 'Signal "net" de la résazurine en fonction du temps | '+fArt.getSimplePath(pathOfCurrentExperiment),
            'y':0.98, 'x':0.5,
            'xanchor': 'center',
            'yanchor': 'top'}))
        #Si on a trouvé au moins une goutte de référence, 
        #on trace une ligne sur le graphe "analyse résa"
    
        for dropArray in listDrops:
            firstDrop=True
            for drop in dropArray:
                #On crée une chaîne de query qui servira à faire une requête sur le dataFrame principal : 
                queryString="droplet_id == {0} & run > {1} & time < {2}".format(str(drop),str(param['runMin']),str(param['maxTime']*3600))
        
                #Un colorgroupe pour plotly : 
                nomDuGroupe=metaData['wellToDescription'][metaData['dropletToWell'][str(drop)]]

                xAxisResa=(netDF.query(queryString)["time"])/3600
                yAxisResa=netDF.query(queryString)["fluo_2_median"]

                extraInfo=mainDataFrame.query(queryString)["run"]

                figAnalyseResa.add_trace(go.Scatter(
                    x=xAxisResa, y=yAxisResa, 
                    text=extraInfo,
                    hovertemplate=
                    'Run: %{text}<br>Time: %{x}<br>Value: %{y}<br>Drop: '+str(drop),                
                    legendgroup=str(dropArray),
                    line=dict(color=metaParam.matColors[idColor]),
                    showlegend=True if firstDrop else False,
                    name=nomDuGroupe))
                
                firstDrop=False
            
            if idColor < len(metaParam.matColors):
                idColor+=1
        
        figAnalyseResa.add_shape(
            # Ligne au delà de laquelle on ne peut pas trouver de lagTime
            dict(
                type="line",
                name='Seuil de regression',
                x0=0, y0=param["seuilRegressionResa"],
                x1=param["maxTime"],y1=param["seuilRegressionResa"],
                line=dict(color="black", width=3)))

        #On ajoute le boutton log/lin sur la figure
        figAnalyseResa.update_layout(updatemenus=param['updatemenus'])

        #Si on a bien trouvé au moins une goutte de référence, on trace la figure    
        figAnalyseResa.show(renderer=metaParam.renderer, config={'editable': True})

        del figAnalyseResa, xAxisResa, yAxisResa, extraInfo


#--------- Display of Fluo1 'net signal'----------
#-------------------------------------------------

if metaParam.displayNetFluo1Signal==True:
    idColor=0

    if fArt.checkReferenceDrop(pathOfCurrentExperiment,metaData) == False:
        print('There is no satisfying reference drop. Pyranin analysis aborted.\nYou can add one with giveReferenceDrop.py script')
    elif netDFPyraDeclared==False:
        print('Please relaunch first cell to create a net DataFrame')
    else:
        dropRef=json.load(open(pathOfCurrentExperiment+"/analyseArthur/referenceDrops.json"))
    
        queryStringReference="droplet_id == {0} & run > {1} & time < {2}".format(dropRef,str(param['runMin']),str(param['maxTime']*3600))
        dfReference=mainDataFrame.query(queryStringReference).reset_index(drop=True)
        figAnalysePyra=go.Figure(layout=go.Layout(
            autosize=False,
            font=metaParam.taillePolice, width=metaParam.W, height=metaParam.H,
            template='seaborn',
            xaxis=dict(zeroline=False, dtick=6,title="""Temps d'incubation en heures"""),
            yaxis=dict(zeroline=False, title='Fluorescence de la pyranine "nette"    -'),
            title={
            'text': 'Signal "net" de la pyranine en fonction du temps | '+fArt.getSimplePath(pathOfCurrentExperiment),
            'y':0.98, 'x':0.5,
            'xanchor': 'center',
            'yanchor': 'top'}))
        #Si on a trouvé au moins une goutte de référence, 
        #on trace une ligne sur le graphe "analyse résa"
    
        for dropArray in listDrops:
            firstDrop=True
            for drop in dropArray:
                #On crée une chaîne de query qui servira à faire une requête sur le dataFrame principal : 
                queryString="droplet_id == {0} & run > {1} & time < {2}".format(str(drop),str(param['runMin']),str(param['maxTime']*3600))
        
                #Un colorgroupe pour plotly : 
                nomDuGroupe=metaData['wellToDescription'][metaData['dropletToWell'][str(drop)]]

                xAxisPyra=(netDFPyra.query(queryString)["time"])/3600
                yAxisPyra=netDFPyra.query(queryString)["fluo_1_median"]

                extraInfo=mainDataFrame.query(queryString)["run"]

                figAnalysePyra.add_trace(go.Scatter(
                    x=xAxisPyra, y=yAxisPyra, 
                    text=extraInfo,
                    hovertemplate=
                    'Run: %{text}<br>Time: %{x}<br>Value: %{y}<br>Drop: '+str(drop),                
                    legendgroup=str(dropArray),
                    line=dict(color=metaParam.matColors[idColor]),
                    showlegend=True if firstDrop else False,
                    name=nomDuGroupe))
                
                firstDrop=False
            
            if idColor < len(metaParam.matColors):
                idColor+=1

        #On ajoute le boutton log/lin sur la figure
        figAnalysePyra.update_layout(updatemenus=param['updatemenus'])

        #Si on a bien trouvé au moins une goutte de référence, on trace la figure    
        figAnalysePyra.show(renderer=metaParam.renderer, config={'editable': True})

        del figAnalysePyra, xAxisPyra, yAxisPyra, extraInfo

#--------------------------------------------
#------ Display du signal scatt net : -------

if metaParam.displayNetScatteringSignal==True:
    idColor=0

    if fArt.checkReferenceDrop(pathOfCurrentExperiment,metaData) == False:
        print('There is no satisfying reference drop. Scattering analysis aborted.\nYou can add one with giveReferenceDrop.py script')
    elif netDFScattDeclared==False:
        print('Please relaunch first cell to create a net scattering DataFrame')
    else:
        dropRef=json.load(open(pathOfCurrentExperiment+"/analyseArthur/referenceDrops.json"))

        figNetScatt=go.Figure(layout=go.Layout(
            width=metaParam.W,
            height=metaParam.H,
            font=metaParam.taillePolice,
            template='seaborn',
            xaxis=dict(zeroline=False, dtick=6,title="""Temps d'incubation en heures"""),
            yaxis=dict(zeroline=False, title='Net Scattering signal'),
            title={
            'text': 'Scattering net | '+fArt.getSimplePath(pathOfCurrentExperiment)[0:11]+' | Echantillons  : ' + strSamples,
            'y':0.98, 'x':0.5,
            'xanchor': 'center',
            'yanchor': 'top'}))

        #Si on a trouvé au moins une goutte de référence, 
        #on trace une ligne sur le graphe "analyse résa"
    
        for dropArray in listDrops:
            firstDrop=True
            for drop in dropArray:
                #On crée une chaîne de query qui servira à faire une requête sur le dataFrame principal : 
                queryString="droplet_id == {0} & run > {1} & time < {2}".format(str(drop),str(param['runMin']),str(param['maxTime']*3600))
        
                #Un colorgroupe pour plotly : 
                nomDuGroupe=metaData['wellToDescription'][metaData['dropletToWell'][str(drop)]]

                xAxisScat=(netDFScatt.query(queryString)["time"])/3600
                yAxisScat=netDFScatt.query(queryString)["scattering_median"]

                extraInfo=mainDataFrame.query(queryString)["run"]

                figNetScatt.add_trace(go.Scatter(
                    x=xAxisScat, y=yAxisScat, 
                    text=extraInfo,
                    hovertemplate=
                    'Run: %{text}<br>Time: %{x}<br>Value: %{y}<br>Drop: '+str(drop),                
                    legendgroup=str(dropArray),
                    line=dict(color=metaParam.matColors[idColor]),
                    showlegend=True if firstDrop else False,
                    name=nomDuGroupe))
                
                firstDrop=False
            
            if idColor < len(metaParam.matColors):
                idColor+=1

        figNetScatt.add_shape(
            # Ligne au delà de laquelle on ne peut pas trouver de lagTime
            dict(
                type="line",
                name='Seuil de regression',
                x0=0, y0=param["seuilRegressionScatt"],
                x1=param["maxTime"],y1=param["seuilRegressionScatt"],
                line=dict(color="black", width=3)))

        #On ajoute le boutton log/lin sur la figure
        figNetScatt.update_layout(updatemenus=param['updatemenus'])

        #Si on a bien trouvé au moins une goutte de référence, on trace la figure    
        figNetScatt.show(renderer=metaParam.renderer, config={'editable': True})

        del figNetScatt, xAxisScat, yAxisScat, extraInfo

#%%
#--------------------------------------------------------------
#--------------- Analyse et graphe écart-type -----------------
#--------------------------------------------------------------
# Ce script affiche l'écart-type du scattering de chaque goutte pour chaque run
# et isole les courbes avec un ecart-type au dessus de seuils spécifiées dans parameters.json

metaParam=reload(metaParam)

if metaParam.stdAnalysisOn == True:
    try:
        growingDrops=fArt.lookforGrowingDrops(netDF,metaData,upThreshold=param['seuilRegression'])
        print('growingDrops trouvées avec succès.')

        # ----------------------------------------------
        # -- Affichage 3d scattering et écart-type -----
        fig3dOdd=go.Figure(layout=go.Layout(
                    font=metaParam.taillePolice, template='seaborn',
                    width=metaParam.W, height=metaParam.H,
                    title={'text': 'Analyse de l\'ecart-type - runs impairs <br>'+fArt.getSimplePath(pathOfCurrentExperiment)+' | Echantillons  : ' + strSamples }))
        fig3dEven=go.Figure(fig3dOdd)
        fig3dEven.update_layout(title={'text': 'Analyse de l\'ecart-type - runs pairs <br>'+fArt.getSimplePath(pathOfCurrentExperiment)+' | Echantillons  : ' + strSamples })

        idColor=0
        for dropArray in listDrops:
            firstDrop=True
            for drop in dropArray:
                nomDuGroupe=metaData['wellToDescription'][metaData['dropletToWell'][str(drop)]]
                if str(drop) in growingDrops[0].index:
                    queryStringEven="droplet_id == @drop & run % 2 == 0 & run != 0"
                    queryStringOdd="droplet_id == @drop & run % 2 == 1"
                    for arr in [fig3dEven, queryStringEven], [fig3dOdd, queryStringOdd]:
                        xAxis3D=mainDataFrame.query(arr[1]).time/3600
                        yAxis3D=mainDataFrame.query(arr[1]).scattering_median
                        zAxis3D=mainDataFrame.query(arr[1]).scattering_std
                        arr[0].add_trace(go.Scatter3d(
                            x=xAxis3D, y=yAxis3D, z=zAxis3D,
                            marker=dict(size=5),
                            legendgroup=str(dropArray),
                            showlegend=True if firstDrop else False,
                            name=nomDuGroupe,
                            line=dict(width=4, color=metaParam.matColors[idColor])
                            ))
                firstDrop=False
            idColor+=1

        fig3dEven.update_layout(scene = dict(
            xaxis_title='Temps en heures',
            yaxis_title='scattering',
            zaxis_title='Ecart-type'))
        fig3dOdd.update_layout(scene = dict(
            xaxis_title='Temps en heures',
            yaxis_title='scattering',
            zaxis_title='Ecart-type'))

        fig3dEven.show(renderer=metaParam.renderer, editable=True)
        fig3dOdd.show(renderer=metaParam.renderer, editable=True)
    except:
        print('Impossible de trouver les growingDrops pour l\'instant. Analyse de l\'écart-type annulée')
#%%
#------------------------------------------------
#--------- Cellule 3 : Curve fitting ------------
#------------------------------------------------
metaParam, fArt =reload(metaParam), reload(fArt)

dataSets=[
    {"name":"resazurine", "netDF":netDF, "channel":"fluo_2_median", "seuil":param['seuilRegressionResa']},
    {"name":"scattering", "netDF":netDFScatt, "channel":"scattering_median", "seuil": param['seuilRegressionScatt']},
    # {"name":"pyranine", "netDF":netDFPyra, "channel":"fluo_1_median", "seuil":param['seuilRegressionPyra']}
]

for dSet in dataSets:
    if metaParam.launchFitting:  
        try:
            growingDrops=fArt.lookforGrowingDrops(dSet["netDF"], metaData, 
                metaParam.tMaxUniqueResa,
                canal=dSet['channel'],
                upThreshold=dSet["seuil"],
                downThreshold= -2)
            print('growingDrops trouvées avec succès.')
        except:
            print('Impossible de trouver les growingDrops pour l\'instant.')

        fArt.regression(pathOfCurrentEXP=pathOfCurrentExperiment,
            netDF=dSet["netDF"],
            growingDrops=growingDrops,
            param=param,
            metaParam=metaParam,
            metaData=metaData,
            seuil=dSet["seuil"],
            channel = dSet["channel"],
            channelName=dSet["name"],
            displayCurves=True, askOverwrite =True, checkStandardDev=False)

# %%
#------------------------------------------------------
#---- Cellule 4 : export de la fiche de paramètres ----
#------------------------------------------------------

# param=json.load(open(chosenPath+'/parameters.json'))
newPath = pathOfCurrentExperiment+"/analyseArthur" #On crée un dossier dans l'expérience
fname = newPath+'/parameters.json' #nom du nouveau fichier json

#On demande à l'utilisateur s'il veut enregistrer ses nouveaux paramètres, ou garder les anciens

if not path.exists(newPath):
    print("""Pas de dossier 'analyseArthur' trouvé. Nous en créons un""")
    makedirs(newPath)

if not path.exists(fname):
    print('Pas de fiche de paramètres trouvée, nous sauvegardons les paramètres actuels.')
    
    with open(fname, 'w') as outfile:
        json.dump(param, outfile, indent=4)

else:
    print("Il existe déjà une fiche de paramètres pour cette expérience\nVoulez-vous la remplacer par les paramètres actuels ? ")
    ouiOuNon=''
    while not ouiOuNon in ['y','n']:
        ouiOuNon=input('y/n?')
    
    if ouiOuNon=='n':
        print('Aucune modification de parameters.json à faire, le script va fermer.')

    elif ouiOuNon=='y':
        print('C\'est noté, nous écrasons les anciens paramètres.')

        with open(fname, 'w') as outfile:
            json.dump(param, outfile, indent=4)

        #On demande à l'usager s'il veut recalcuer le NetDF suite à ses modifications de param 
        ouiOuNonNetDF=''
        print("\nSouhaitez-vous recalculer le netDataFrame (cela prendra quelques dizaines de secondes) ? ")
        
        while not ouiOuNonNetDF in ['y','n']:
            ouiOuNonNetDF=input('Souhaitez-vous recalculer le netDataFrame (cela prendra quelques dizaines de secondes) ? (y/n)')

        if ouiOuNonNetDF=='n':
            print('Aucune modification de parameters.json à faire, le script va fermer.')

        elif ouiOuNonNetDF=='y':
            mainDataFrameFitting=pd.DataFrame(pd.read_csv(pathOfCurrentExperiment+"/analysis/droplet_dynamics.csv", sep=",",
                usecols=param["colToUseResa"], converters={'droplet_id':convertDroplet_id}))
            netDF=fArt.createNetDataFrame(mainDataFrameFitting,refDrop,param['runMax'],  
                newPipeline = path.exists(pathOfCurrentExperiment+"/analysis/temp"))
            netDF.to_csv('{}/analyseArthur/netDF.csv'.format(pathOfCurrentExperiment))
            print("\nnetDataFrame recalculé avec succès ")
        
        ouiOuNonNetDFScatt=''
        print("\nSouhaitez-vous recalculer le netDataFrame du scattering (cela prendra quelques dizaines de secondes) ? ")

        while not ouiOuNonNetDFScatt in ['y','n']:
            ouiOuNonNetDFScatt=input('Souhaitez-vous recalculer le netDataFrame du scattering (cela prendra quelques dizaines de secondes) ? (y/n)')

        if ouiOuNonNetDFScatt=='n':
            print('Aucune modification de parameters.json à faire, le script va fermer.')

        elif ouiOuNonNetDFScatt=='y':
            colToUseScatt=['run', 'scattering_median', 'scattering_std', 'time', 'droplet_id']
            mainDataFrameScatt=pd.DataFrame(pd.read_csv(pathOfCurrentExperiment+"/analysis/droplet_dynamics.csv", sep=",", converters={'droplet_id':convertDroplet_id}, usecols=colToUseScatt))  
            netDFScatt=fArt.createNetDataFrame(mainDataFrameScatt,
                refDrop,param['runMax'], 
                canal="scattering_median", toZero = True, 
                newPipeline = path.exists(pathOfCurrentExperiment+"/analysis/temp"))
            netDFScatt.to_csv('{}/analyseArthur/netDFScatt.csv'.format(pathOfCurrentExperiment))
            netDFScattDeclared=True
            print("\nnetDataFrameScatt recalculé avec succès ")
            del mainDataFrameScatt

# # %%
# fig = go.Figure(
#     data=[go.Scatter(x=[0, 1], y=[0, 1])],
#     layout=go.Layout(
#         xaxis=dict(range=[0, 5], autorange=False),
#         yaxis=dict(range=[0, 5], autorange=False),
#         title="Start Title",
#         updatemenus=[dict(
#             type="buttons",
#             buttons=[dict(label="Play",
#                           method="animate",
#                           args=[None])])]
#     ),
#     frames=[go.Frame(data=[go.Scatter(x=[1, 2], y=[1, 2])]),
#             go.Frame(data=[go.Scatter(x=[1, 4], y=[1, 4])]),
#             go.Frame(data=[go.Scatter(x=[3, 4], y=[3, 4])],
#                      layout=go.Layout(title_text="End Title"))]
# )

# fig.show(renderer=metaParam.renderer, config={'editable': True})
# # %%

# %%
