#%%
pathExperimentMD=""
import pandas as pd
import plotly.graph_objects as go
import metaParametersSheet as metaParam #paramètres qui ne dépendent pas de l'experience
import metaDonneesExperiences as metaD
import glob
import os.path

#On va chercher le fichier csv le plus récent dans le dossier recapitulatifRegressions 
folder_path = metaParam.pathMilliDrop+'/recapitulatifRegressions'
file_type = "\*csv"
files = glob.glob(folder_path + file_type)
for elem in files: #On vérifie que le document contient la chaîne "mainregression"
    if not "mainregression" in elem:
        files.remove(elem)
max_file = max(files, key=os.path.getctime)

dataF=pd.DataFrame(pd.read_csv(
    max_file,
    decimal = '.', sep=","))

idColor=0
idShape=0

matColors=metaParam.matColors

arrayOfSoils=dataF.Sol.unique()
dictOfSoils={}

for element in arrayOfSoils:
    dictOfSoils[element]=idColor
    idColor+=1

arrayOfDilutions=dataF.dilution.unique()
dictOfDilutions={}

for element in arrayOfDilutions:
    dictOfDilutions[element]=idShape
    idShape+=1

idColor=0
arrayOfExp=dataF.Experience.unique()
dictOfExp={}

for element in arrayOfExp:
    arrayOfSoilsInExp=dataF.query('Experience == @element').Sol.unique()
    for Isoil in arrayOfSoilsInExp:
        dictOfExp[str(element)+ ' - '+Isoil]=[idColor, element, Isoil]
        idColor+=1

#Figure où chaque sol a une couleur qui lui est propre
fig = go.Figure(layout=go.Layout(
    template='seaborn',
    font=metaParam.taillePolice, autosize=False,
    width=metaParam.W, height=metaParam.H,
    xaxis=dict(zeroline=False, title='Temps de division (h)', type='log'),
    yaxis=dict(zeroline=False, title='Temps au seuil (h)'),
    title={'text': 'Nuages des paramètres des régressions, par sol'}))

for soil in arrayOfSoils:
    for dilution in arrayOfDilutions:
        querystring="Sol == @soil & dilution == @dilution"

        xAxis=dataF.query(querystring)["tempsDeDivision"].round(3) #on ne met que trois décimales
        yAxis=dataF.query(querystring)["tempsAuSeuil"].round(3)

        extraInfo=dataF.query(querystring)["goutte"]
        try: 
            fig.add_trace(go.Scatter(
                mode="markers",
                marker=dict(
                    color=metaD.dictOfSamplesColors[soil],
                    size=11,
                    ),
                marker_symbol=201+dictOfDilutions[dilution],
                x=xAxis, y=yAxis,
                text=extraInfo,
                hovertemplate='Goutte: %{text}<br>temps de Division: %{x} heures<br>latence: %{y} heures',
                legendgroup=soil+' - '+dilution,
                name=soil+'<br>'+dilution
                ))
        except:
            pass

fig.show(renderer=metaParam.renderer, config={'editable' : True})


#Figure où chaque sol.expérience a une couleur qui lui est propre (le même sol étudié à deux occasions aura deux couleurs différentes)
fig = go.Figure(layout=go.Layout(
    template='seaborn',
    font=metaParam.taillePolice, autosize=False,
    width=metaParam.W, height=metaParam.H,
    xaxis=dict(zeroline=False, title='Temps de division (h)', type='log'),
    yaxis=dict(zeroline=False, title='Temps au seuil (h)'),
    title={'text': 'Nuages des paramètres des régressions, par experience'}))

for uniqueSoil in dictOfExp:
    for dilution in arrayOfDilutions:
        exp=str(dictOfExp[uniqueSoil][1])
        sol=dictOfExp[uniqueSoil][2]
        querystring="Experience == @exp & Sol == @sol & dilution == @dilution"

        xAxis=dataF.query(querystring)["tempsDeDivision"].round(3)
        yAxis=dataF.query(querystring)["tempsAuSeuil"].round(3)

        extraInfo=dataF.query(querystring)["goutte"]

        fig.add_trace(go.Scatter(
            mode="markers",
            marker=dict(
                color=matColors[dictOfExp[uniqueSoil][0]],
                size=11,
                ),
            marker_symbol=201+dictOfDilutions[dilution],
            text=extraInfo,
            hovertemplate='Goutte: %{text}<br>temps de Division: %{x} heures<br>latence: %{y} heures',
            x=xAxis, y=yAxis,
            legendgroup=str(dictOfExp[uniqueSoil][1])+' - '+dictOfExp[uniqueSoil][2]+' - '+dilution,
            name=str(dictOfExp[uniqueSoil][1])+' - '+dictOfExp[uniqueSoil][2]+' - '+dilution
            ))

fig.show(renderer=metaParam.renderer, config={'editable' : True})
del fig
# %%
