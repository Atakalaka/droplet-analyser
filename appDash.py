import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.express as px

pathExperimentMD="" 
import json
from importlib import reload
from math import exp
from os import makedirs, path
from sys import exc_info
from time import time
from tkinter import messagebox

import numpy as np
import pandas as pd
import plotly.graph_objects as go
from numpy import array, linspace, log
from scipy.optimize import curve_fit

import functionsArthur as fArt
import metaParametersSheet as metaParam  # paramètres qui ne dépendent pas de l'expérience

# mainDataFrame = pd.DataFrame(pd.read_csv('C:/Users/Arthur/ownCloud/ExperiencesMD_Arthur/Pikovskaya_Pyra1prc_Resa5prm_Gain32/EXP20200610_1248 - {PS 160819} et {Coli UMN} - Pikov/analyseArthur/resultsOfRegressionEXP20200610_1248.csv'))

#Cellule 1 : choix de l'expérience
pathExperimentMD=fArt.choixDossier(pathMilliDrop=metaParam.pathMilliDrop)
metaData=fArt.importMetadata(pathExperimentMD)

dir_path = path.dirname(path.realpath(__file__)) #chemin des scripts python

if path.exists(pathExperimentMD+'/analyseArthur/parameters.json'):
    print('You formerly saved parameters for this experiment. We are loading them.') 
    chosenPath=pathExperimentMD+'/analyseArthur'

else: #S'il n'y a pas de parameters.json, on charge ceux par défaut
    print('No parameters found. We are loading the default ones.') 
    chosenPath=dir_path

param=json.load(open(chosenPath+'/parameters.json'))

#On affiche les différentes catégories, pour pouvoir les entrer si besoin
print("""metaData["descriptionToWells"] : """)
for group in metaData['descriptionToWells']:
    print("- "+group+"\n")

# mainDataFrame=pd.DataFrame(pd.read_csv(pathExperimentMD+"/analysis/droplet_dynamics.csv", sep=";",decimal=",",
mainDataFrame=pd.DataFrame(pd.read_csv(pathExperimentMD+"/analysis/droplet_dynamics.csv", sep=",",
    usecols=param['colToUse']))

listDrops=fArt.dropsWell(param['listOfDropsAndWells'],pathExperimentMD,metaData)

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

app.layout = html.Div([
    dcc.Graph(id='VSGraph'),

    dcc.Slider(
        id='run-slider',
        min=4,
        max=mainDataFrame['run'].max(),
        value=mainDataFrame['run'].max(),
        marks={str(run): str(run) for run in mainDataFrame['run'].unique()},
        step=None)
    ])

@app.callback(
    Output('VSGraph', 'figure'),
    [Input('run-slider', 'value')])
def update_figure(maxRun):
    filteredmainDataFrame = mainDataFrame.query("run <= @maxRun")

    figFluo1VSScatt=go.Figure(layout=go.Layout(
        template='seaborn',
        xaxis=dict(zeroline=False, title='Fluo 1', type='log'),
        yaxis=dict(zeroline=False, title='Scattered light', range=[0,1.1]),
        title={'text': 'titre'}))

    idColor=0

    for dropArray in listDrops:
        # firstDrop=True
        for drop in dropArray:
            #On crée une valeur moyenne initiale pour chaque goutte (moyenne des 4 premiers runs)
            queryString4Runs="droplet_id == {0} & run < 5 & run > 0".format(str(drop))

            initialValue=mainDataFrame.query(queryString4Runs)["scattering_median"].mean()

            #On crée une chaîne de query qui servira à faire une requête sur le dataFrame principal : 
            queryString="droplet_id == {0} & run < {1}".format(str(drop),str(maxRun))

            #Un colorgroupe pour plotly : 
            nomDuGroupe=metaData['wellToDescription'][metaData['dropletToWell'][str(drop)]]
            try:
                extraInfo=mainDataFrame.query(queryString)["droplet_id"]

                figFluo1VSScatt.add_trace(go.Scatter(
                    x=filteredmainDataFrame.query(queryString)["fluo_1_median"],
                    y=filteredmainDataFrame.query(queryString)['scattering_median']/initialValue,
                    legendgroup=str(dropArray), 
                    line=dict(color=param['matColors'][idColor]),
                    # showlegend = True if firstDrop else False,
                    text=extraInfo,
                    hovertemplate='Drop : %{text}',
                    name=nomDuGroupe))
                # firstDrop=False
            except:
                print("Erreur avec la ligne fluo vs scatt de la goutte "+str(drop))
                print("Info sur l'erreur :", exc_info()[0],exc_info()[1],exc_info()[2])
        
        if idColor < len(param['matColors']):
            idColor+=1

    figFluo1VSScatt.update_layout(transition_duration=200)

    return figFluo1VSScatt


if __name__ == '__main__':
    app.run_server(debug=True)
