#%%
#Calcul des netDataframe pour toutes les expériences, sur les trois cannaux.
#Cellule importation
pathExperimentMD=""
import json
from importlib import reload
from os import path, listdir
import pandas as pd

import functionsArthur as fArt
import metaParametersSheet as metaParam  # paramètres qui ne dépendent pas de l'expérience

def convertDroplet_id(id):
# Pour convertir les numéros de goutte en int, ce qui n'est pas le cas par défaut avec le nouveau script de Guilhem.
    return int(float(id))

#%%
# Calcul des netDF pour toutes les expériences. 
metaParam=reload(metaParam)
#On liste les expériences
folders=[]
colToUseScatt=['run', 'scattering_median', 'scattering_std', 'time', 'droplet_id']
for rootFolder in metaParam.rootFolders:
    folders = folders + [rootFolder +'/'+ f for f in listdir(rootFolder)]

for experimentPath in folders :
    if not ("CONTAMINE" in experimentPath or "ECHEC" in experimentPath or "ALIGNEMENT" in experimentPath) and '{' in experimentPath:
        try :
            refDrop=json.load(open(experimentPath+"/analyseArthur/referenceDrops.json"))
            param = json.load(open(experimentPath+"/analyseArthur/parameters.json"))
            # Fluo 2 (résazurine)
            try: 
                mainDataFrameResa=pd.DataFrame(pd.read_csv(experimentPath+"/analysis/droplet_dynamics.csv", sep=",",
                    usecols=param["colToUseResa"],
                    converters={'droplet_id':convertDroplet_id}))
            except:
                mainDataFrameResa=pd.DataFrame(pd.read_csv(experimentPath+"/analysis/droplet_dynamics.csv", sep=";",
                    usecols=param["colToUseResa"],
                    converters={'droplet_id':convertDroplet_id}))
            
            netDFResa=fArt.createNetDataFrame(mainDataFrameResa,
                refDrop,
                param['runMax'], 
                firstRun=param["runMin"],
                toZero = True, 
                newPipeline = path.exists(experimentPath+"/analysis/temp"))
            netDFResa.to_csv('{}/analyseArthur/netDF.csv'.format(experimentPath))
            print('Résa OK pour', fArt.getSimplePath(experimentPath))
            # Scattering 
            try:
                mainDataFrameScatt=pd.DataFrame(pd.read_csv(experimentPath+"/analysis/droplet_dynamics.csv", 
                    sep=",", 
                    usecols=colToUseScatt, converters={'droplet_id':convertDroplet_id})) 
            except:
                mainDataFrameScatt=pd.DataFrame(pd.read_csv(experimentPath+"/analysis/droplet_dynamics.csv", 
                    sep=";", 
                    usecols=colToUseScatt, converters={'droplet_id':convertDroplet_id})) 
                    
            netDFScatt=fArt.createNetDataFrame(mainDataFrameScatt,
                refDrop, param['runMax'],
                canal="scattering_median",
                firstRun=param["runMin"], toZero = True, newPipeline = path.exists(experimentPath+"/analysis/temp"))
            netDFScatt.to_csv('{}/analyseArthur/netDFScatt.csv'.format(experimentPath))
            print('Scattering OK pour', fArt.getSimplePath(experimentPath))
        except:
            print('Erreur avec',fArt.getSimplePath(experimentPath))

#%%
# Partie Fluo 1
# Créer un DFFluo1 pour toutes les expériences 
# On liste les expériences
folders=[]
for rootFolder in metaParam.rootFolders:
    folders = folders + [rootFolder +'/'+ f for f in listdir(rootFolder)]

for experimentPath in folders :
    if not ("CONTAMINE" in experimentPath or "ECHEC" in experimentPath or "ALIGNEMENT" in experimentPath) and '{' in experimentPath:
        try :
            param = json.load(open(experimentPath+"/analyseArthur/parameters.json"))
            try:
                DFFluo1=pd.DataFrame(pd.read_csv(experimentPath+"/analysis/droplet_dynamics.csv", sep=",",
                    converters={'droplet_id':convertDroplet_id},
                    usecols=['time','fluo_1_median','droplet_id','run']))
            except:
                DFFluo1=pd.DataFrame(pd.read_csv(experimentPath+"/analysis/droplet_dynamics.csv", sep=";",
                    converters={'droplet_id':convertDroplet_id},
                    usecols=['time','fluo_1_median','droplet_id','run']))

            #On soustraie un deltatime pour ramener toutes les expériences au même temps initial
            deltaTime=(DFFluo1.iloc[0].time/3600)-DFFluo1.run.min()*0.5
            if path.exists(pathExperimentMD+"/analysis/temp"): #s'il s'agit d'une expérience avec le nouveau pipeline, on décale les runs de 1
                deltaTime+=0.5
            
            ind, length = 0, len(DFFluo1)
            while ind<length:
                DFFluo1.loc[ind, "time"]-=deltaTime
                ind+=1

            #On enlève le run 0 et les runs après runMax
            DFFluo1=DFFluo1[(DFFluo1.run > 0) * (DFFluo1.run<param['runMax'])]
            DFFluo1.to_csv('{}/analyseArthur/DFFluo1.csv'.format(experimentPath), index=False)
            print(fArt.getSimplePath(experimentPath), "- Fluo 1 OK")
        except:
            print('erreur pour '+experimentPath)

# %%
