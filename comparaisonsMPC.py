#%%
#Cellule importation
from importlib import reload

import pandas as pd
import plotly.graph_objects as go
from numpy import array, linspace, log, log10, exp, sqrt, float64
from scipy.optimize import curve_fit
from scipy.stats import gmean, pearsonr, spearmanr

import numpy as np
import functionsArthur as fArt
import metaParametersSheet as metaParam  # paramètres qui ne dépendent pas de l'expérience
import metaDonneesExperiences as metaD

templateFig=go.Figure(layout=go.Layout(
    template='seaborn', font=metaParam.taillePolice,
    width=metaParam.W, height=metaParam.H,title={'text': 'Title'},
    xaxis=dict(zeroline=False, title='xAxis'), yaxis=dict(zeroline=False, title = 'yAxis')))

def linear_func(x,a):
    return a*x

def cropDate(EXPdate_hhhh): 
#Enlève l'heure après le nom d'expérience : EXP20200702_1254 => EXP20200702
    return EXPdate_hhhh.split('_')[0]

matColors=metaParam.matColors
metaParam=reload(metaParam)

#On crée une array avec tous les noms de sol, basée sur metaD : 
arraySoils=[]
for soil in metaD.dictOfSamplesColors:
    if not soil in metaD.souches:
        arraySoils.append(soil)

# -------- Définition des DataFrame de MPC 'Uniques' ---------
# On importe les résultats des calculs de MPC Uniques : 
DFMPCUniqueFluo_2=pd.DataFrame(pd.read_csv(str('{}/MPCFluo_2Uniques.csv').format(metaParam.pathAnalyseTemporelle), converters={'EXP':cropDate}))
DFMPCUniqueScatt=pd.DataFrame(pd.read_csv(str('{}/MPCScatteringUniques.csv').format(metaParam.pathAnalyseTemporelle), converters={'EXP':cropDate}))
DFMPCUniqueFluo_1=pd.DataFrame(pd.read_csv(str('{}/MPCFluo_1Uniques.csv').format(metaParam.pathAnalyseTemporelle), converters={'EXP':cropDate}))

#On fusionne les DF :
mergedDFMPCUniques=pd.merge(DFMPCUniqueFluo_2, DFMPCUniqueScatt, on=['EXP','Sample','Dilution','concentration'], suffixes=(None, 'Scatt'))
mergedDFMPCUniques=pd.merge(mergedDFMPCUniques, DFMPCUniqueFluo_1, on=['EXP','Sample','Dilution','concentration'], suffixes=(None, 'Fluo_1'))

#On crée une list avec les différentes concentrations : 
arrayOfDilutions=mergedDFMPCUniques.Dilution.unique()

#Un dictionnaire avec les concentrations et un indice de forme associé à chaque concentration en particules, qui servira sur les graphes : 
idShape=0
arrayOfConcentrations=list(mergedDFMPCUniques.concentration.unique())
dictOfConcentrations={}

for element in arrayOfConcentrations:
    dictOfConcentrations[element]=idShape
    idShape+=3

#On calcule les fractions des solubilisateurs pour chaque mode de culture : 
for i, row in mergedDFMPCUniques.iterrows():
    mergedDFMPCUniques.at[i, 'fractionScatt']=(100*row['MPCScatt']/row['MPC'])
    mergedDFMPCUniques.at[i, 'fractionF1']=(100*row['MPCFluo_1']/row['MPC'])

# -------- Définition des DataFrame de MPC 'normaux' ---------
DFMPCFluo_2=pd.DataFrame(pd.read_csv(str('{}/MPCFluo_2.csv').format(metaParam.pathAnalyseTemporelle), converters={'EXP':cropDate}))
DFMPCScatt=pd.DataFrame(pd.read_csv(str('{}/MPCScattering.csv').format(metaParam.pathAnalyseTemporelle), converters={'EXP':cropDate}))
DFMPCFluo_1=pd.DataFrame(pd.read_csv(str('{}/MPCFluo_1.csv').format(metaParam.pathAnalyseTemporelle), converters={'EXP':cropDate}))

#On fusionne les DF :
mergedDFMPC=pd.merge(DFMPCFluo_2, DFMPCScatt, on=['EXP','Sample','concentration'], suffixes=(None, 'Scatt'))
mergedDFMPC=pd.merge(mergedDFMPC, DFMPCFluo_1, on=['EXP','Sample','concentration'], suffixes=(None, 'Fluo_1'))

#On calcule les fractions des solubilisateurs pour chaque mode de culture : 
for i, row in mergedDFMPC.iterrows():
    try:
        mergedDFMPC.at[i, 'fractionScatt']=(100*row['MPCScatt']/row['MPC'])
    except:
        pass

#%%
# ----------------------------------------------------------
# ----------------- Affichage Fluo 1 VS scatt --------------
# ----------------------------------------------------------
figPyraVSScatt=go.Figure(templateFig)
figPyraVSScatt.update_layout(
    xaxis=dict(zeroline=False, title='Microbes faisant chuter le scattering par ml - gouttes'),
    yaxis=dict(zeroline=False, title = 'Microbes acidificateurs par ml - gouttes'),
    title={'text': 'Concentration en microbes acidificateurs en fonction de la<br>concentration en microbes faisant baisser le scattering'})

figFractionsF1VSScatt=go.Figure(templateFig)
figFractionsF1VSScatt.update_layout(
    title={'text': 'Fraction des gouttes acidifiées en fonction de<br>la fraction ayant franchi le seuil de scattering'}, 
    xaxis=dict(zeroline=False,range=[-1, 100], title='Fraction des gouttes faisant chuter le scattering (%)'),
    yaxis=dict(zeroline=False,range=[-1, 100], title='Fraction des gouttes acidifiées(%)'))

#On réalise une régression linéaire Sur les fractions:
DFRegressionFractions=mergedDFMPCUniques[(mergedDFMPCUniques.fractionF1.notna()) & (mergedDFMPCUniques.fractionScatt .notna())] #on enlève les nan sinon la régression plante

DFRegressionMPC=mergedDFMPCUniques[(mergedDFMPCUniques.MPCScatt !=0 ) & (mergedDFMPCUniques.MPCScatt.notna() ) & (mergedDFMPCUniques.MPCFluo_1.notna()) & (mergedDFMPCUniques.MPCFluo_1!=0)]
penteScattVSF1, RdeuxScattVSF1= fArt.regressionLineaire(log10(DFRegressionMPC.MPCScatt), log10(DFRegressionMPC.MPCFluo_1))

# Les axes des fractions :
FScattAxis, FF1Axis = DFRegressionFractions.fractionScatt, DFRegressionFractions.fractionF1
k, Rdeux= fArt.regressionLineaire(FScattAxis, FF1Axis)

for sol in arraySoils:
    for dilut in arrayOfDilutions:
        for conc in arrayOfConcentrations : 
            querriedDF=mergedDFMPCUniques[(mergedDFMPCUniques.Sample == sol) * (mergedDFMPCUniques.concentration == conc)* (mergedDFMPCUniques.Dilution == dilut)]
            extraInfo=querriedDF['EXP']

            xAxis=querriedDF.MPCScatt
            yAxis=querriedDF.MPCFluo_1
            figPyraVSScatt.add_trace(go.Scatter(
                x=xAxis, y=yAxis, text=extraInfo,
                marker_symbol=200,
                name=sol,
                showlegend=False,
                marker=dict(color=metaD.dictOfSamplesColors[sol],size =12),
                hovertemplate='%{text}<br>%{y} cellules/ml Fluo 1<br>%{x} cellules/ml Scattering<br>Sol : '+sol+'<br>'+str(conc)+' g/l de particules<br>'+str(dilut),
                mode='markers'))

            xAxisFrac=(querriedDF.fractionScatt).round(2)
            yAxisFrac=(querriedDF.fractionF1).round(2)
            figFractionsF1VSScatt.add_trace(go.Scatter(
                x=xAxisFrac, y=yAxisFrac, text=extraInfo,
                marker_symbol=200,
                name=sol,
                showlegend=False,
                marker=dict(color=metaD.dictOfSamplesColors[sol],size =12),
                hovertemplate='%{text}<br>%{y}%Fluo 1<br>%{x}%Scattering<br>Sol : '+sol+'<br>'+str(conc)+' g/l de particules<br>'+str(dilut),
                mode='markers'))

figFractionsF1VSScatt.add_shape(
    # Ligne régression linéaire
    dict(type="line",
        x0=0, y0=0,
        x1=100, y1=100,
        name="regression",
        line=dict(color="black",width=1)))

figPyraVSScatt.add_shape(
    # Ligne régression linéaire
    dict(type="line",
        x0=10, y0=10,
        x1=mergedDFMPCUniques.MPCScatt.max(), y1=RdeuxScattVSF1*mergedDFMPCUniques.MPCScatt.max(),
        name="regression",
        line=dict(color="black",width=1)))

figPyraVSScatt.update_yaxes(type="log")
figPyraVSScatt.update_xaxes(type="log")
figPyraVSScatt.show(config={'editable':True}, renderer=metaParam.renderer)
figFractionsF1VSScatt.show(config={'editable':True}, renderer=metaParam.renderer)

del figPyraVSScatt, querriedDF, xAxis, yAxis, figFractionsF1VSScatt, DFRegressionFractions
# %%
# -----------------------------------------
# ------Affichage MPC en boxplots ---------

figMPC = go.Figure(layout=go.Layout(
    template='seaborn', font=metaParam.taillePolice, width=metaParam.W, height=metaParam.H,
    xaxis=dict(zeroline=False, title='Sol'),
    yaxis=dict(zeroline=False, title = 'CPP (cellules/gramme de sol)'),
    title={'text': 'Concentration la plus probable en cellules cultivables<br>pour plusieurs sols - gouttes'}))

figMPCScatt, figFracBox, figMPCFLuo_1 = go.Figure(figMPC), go.Figure(figMPC), go.Figure(figMPC)

figMPCScatt.update_layout(
    title={'text': 'Concentration la plus probable en microbes faisant<br>chuter le scattering pour plusieurs sols - gouttes'})
figMPCFLuo_1.update_layout(
    title={'text': 'Concentration la plus probable en microbes acidificateurs<br>pour plusieurs sols - gouttes'})
figFracBox.update_layout(
    yaxis=dict(zeroline=False, title='Fraction des microbes (%)'),
    title={'text': 'Fractions de microbes faisant chuter le scattering<br>pour plusieurs sols - gouttes'})

arrays=(['MPC',figMPC, 5, 'cellules/g'],
['MPCScatt', figMPCScatt, 5, 'cellules/g'],
['MPCFluo_1', figMPCFLuo_1, 5, 'cellules/g'],
['fractionScatt',figFracBox, 1, '%'])

#On crée un dataframe pour stocker les Coefficients de variation
DFCoeffVar=pd.DataFrame(columns=['sol','MPC','MPCScatt','MPCFluo_1','fractionScatt'])

for sample in arraySoils:
    line={'sol':sample}
    for arr in arrays:
        extraInfo=mergedDFMPC[mergedDFMPC.Sample == sample].EXP

        arr[1].add_trace(go.Box(
            y=arr[2]*mergedDFMPC[mergedDFMPC.Sample == sample][arr[0]],
            text=extraInfo,
            hovertemplate='%{text}<br>%{y} '+arr[3],
            marker_color=metaD.dictOfSamplesColors[sample], 
            name=sample))
        line[arr[0]]=(arr[2]*mergedDFMPC[mergedDFMPC.Sample == sample][arr[0]].std())/(arr[2]*mergedDFMPC[mergedDFMPC.Sample == sample][arr[0]].mean())
    DFCoeffVar=DFCoeffVar.append(line, ignore_index=True)

#On exporte DFCoeffVar
DFCoeffVar.to_csv(metaParam.pathMilliDrop+'/DFCoeffVar.csv')

for arr in arrays:
    arr[1].update_traces(boxpoints='all', jitter=0.3)
    arr[1].update_yaxes(type="log")
    arr[1].show(editable=True, renderer=metaParam.renderer)
# %%
# Calcul des moyennes
meanDataFrameMD=mergedDFMPC[mergedDFMPC.Sample.isin(arraySoils)]
meanDataFrameMD=meanDataFrameMD.groupby('Sample').mean()
meanDataFrameMD.to_csv(metaParam.pathMilliDrop+'/meanDataFrameMD.csv')

#%%
# Comparaison des concentrations uniques MD avec 
# les concentrations uniques sur Petri calculées 
# à partir de detectionPositifsVSTemps  
def addEXPbeforeDate(date):
    return 'EXP'+str(date)

DFUniquesPetri=pd.DataFrame(pd.read_csv(str('{}/concentrationsPetriUniques.csv').format(metaParam.pathAnalyseTemporelle),converters={'date':addEXPbeforeDate}))

mergedDFMPCUniques=mergedDFMPCUniques[mergedDFMPCUniques.MPC.notna() & mergedDFMPCUniques.MPCScatt.notna()]

#On crée un dataframe dans lequel on combine les données individuelles calculées à partir de Python sur boîtes et en gouttes
DFComparePetriMDUniques=pd.merge(DFUniquesPetri,
    mergedDFMPCUniques, 
    right_on=['EXP','Sample','Dilution'], left_on=['date', 'sol', 'dilution'])
#On supprime les colonnes en double :
DFComparePetriMDUniques.drop(['date', 'Dilution', 'Sample'], axis = 1, inplace=True)

#------------------ Regressions linéaires ----------------------
from sklearn.linear_model import LinearRegression

#On enlève les zéros, sinon ça bloque la régression
# DFRegressionPhosphate=DFComparePetriMDUniques[(DFComparePetriMDUniques.concentrationHalos.notna()) * (DFComparePetriMDUniques.concentrationHalos != 0) * (DFComparePetriMDUniques.MPCScatt != 0)]
# DFRegressionCFU=DFComparePetriMDUniques[(DFComparePetriMDUniques.concentrationColonies.notna()) * (DFComparePetriMDUniques.concentrationColonies != 0) * (DFComparePetriMDUniques.MPC != 0)]
# DFRegressionFrac=DFComparePetriMDUniques[(DFComparePetriMDUniques.concentrationColonies.notna())]

# #On met les axes au bon format pour sklearn
# PetriAxisCFU, MDAxisCFU=np.array(log10(DFRegressionCFU.concentrationColonies)), np.array(log10(DFRegressionCFU.MPC)).reshape(-1,1)
# PetriAxisHalos, MDAxisHalos=np.array(log10(DFRegressionPhosphate.concentrationHalos)), np.array(log10(DFRegressionPhosphate.MPCScatt)).reshape(-1,1)
# PetriAxisFrac, MDAxisFrac=np.array(DFRegressionPhosphate.fractionHalos.round(4)*100), np.array(DFRegressionPhosphate.fractionScatt.round(2)).reshape(-1,1)

# # On fite
# modelCFU = LinearRegression(fit_intercept=False).fit(MDAxisCFU, PetriAxisCFU)
# RdeuxCFU = pearsonr(log10(DFRegressionCFU.concentrationColonies), log10(DFRegressionCFU.MPC))[0]
# modelHalos = LinearRegression(fit_intercept=False).fit(MDAxisHalos, PetriAxisHalos)
# RdeuxHalos = pearsonr(log10(DFRegressionPhosphate.concentrationHalos), log10(DFRegressionPhosphate.MPCScatt))[0]
# modelFrac = LinearRegression(fit_intercept=False).fit(MDAxisFrac, PetriAxisFrac)
# RdeuxFrac= pearsonr(DFRegressionFrac.fractionHalos, DFRegressionFrac.fractionScatt)[0]

# print('Slope CFU:', modelCFU.coef_, 'RdeuxCFU :',RdeuxCFU)
# print('Slope Halos:', modelHalos.coef_, 'RdeuxHalos :', RdeuxHalos)
# print('Slope Frac:', modelFrac.coef_, 'RdeuxFrac :', RdeuxFrac)
# del DFRegressionPhosphate, DFRegressionCFU

# On enlève les zéros, sinon ça bloque la régression
DFRegressionPhosphate=DFComparePetriMDUniques[(DFComparePetriMDUniques.concentrationHalos.notna()) * (DFComparePetriMDUniques.concentrationHalos != 0) * (DFComparePetriMDUniques.MPCScatt != 0)]
DFRegressionCFU=DFComparePetriMDUniques[(DFComparePetriMDUniques.concentrationColonies.notna()) * (DFComparePetriMDUniques.concentrationColonies != 0) * (DFComparePetriMDUniques.MPC != 0)]
DFRegressionFrac=DFComparePetriMDUniques[(DFComparePetriMDUniques.concentrationColonies.notna())]

#On met les axes au bon format pour sklearn
MDAxisCFU, PetriAxisCFU=np.array(log10(DFRegressionCFU.MPC)), np.array(log10(DFRegressionCFU.concentrationColonies)).reshape(-1,1)
MDAxisHalos, PetriAxisHalos=np.array(log10(DFRegressionPhosphate.MPCScatt)), np.array(log10(DFRegressionPhosphate.concentrationHalos)).reshape(-1,1)
MDAxisFrac, PetriAxisFrac =np.array(DFRegressionFrac.fractionScatt.round(2)), np.array(DFRegressionFrac.fractionHalos.round(4)*100).reshape(-1,1)

# On fite
modelCFU = LinearRegression(fit_intercept=False).fit(PetriAxisCFU, MDAxisCFU)
RdeuxCFU = pearsonr(log10(DFRegressionCFU.concentrationColonies), log10(DFRegressionCFU.MPC))[0]
modelHalos = LinearRegression(fit_intercept=False).fit(PetriAxisHalos, MDAxisHalos)
RdeuxHalos = pearsonr(log10(DFRegressionPhosphate.concentrationHalos), log10(DFRegressionPhosphate.MPCScatt))[0]
modelFrac = LinearRegression(fit_intercept=False).fit(PetriAxisFrac, MDAxisFrac)
RdeuxFrac= pearsonr(DFRegressionFrac.fractionHalos, DFRegressionFrac.fractionScatt)[0]

print('Slope CFU:', modelCFU.coef_, 'RdeuxCFU :',RdeuxCFU)
print('Slope Halos:', modelHalos.coef_, 'RdeuxHalos :', RdeuxHalos)
print('Slope Frac:', modelFrac.coef_, 'RdeuxFrac :', RdeuxFrac)
del DFRegressionPhosphate, DFRegressionCFU
#----------------------------------------------------------------

figureCompareFrac=go.Figure(layout=go.Layout(
    template='seaborn', font=metaParam.taillePolice, width=metaParam.W, height=metaParam.H,
    xaxis=dict(zeroline=False, title= 'Fraction Petri (%)', range=[-5,105]),
    yaxis=dict(zeroline=False, title = 'Fraction en gouttes (%)', range=[-5,105]),
    title={'text': 'Fractions de microbes faisant chuter le scattering en gouttes<br>en fonction de la fraction de microbes formant un halo sur boîte (%)'}))

figureresaVSCFU, figureScatVshalos, figureFluo_1Vshalos = go.Figure(templateFig), go.Figure(templateFig), go.Figure(templateFig)

figureresaVSCFU.update_layout(
    xaxis=dict(title= 'Colonies par ml - boîte', range=[0,5]), yaxis=dict(title = 'Positifs résazurine par ml - gouttes', range=[0,5]),
    title={'text': 'Concentration en colonies (boîtes) vs concentration en<br>positifs à la résazurine (gouttes) - log10'})
figureScatVshalos.update_layout(
    xaxis=dict(title= 'Halos par ml - boîte', range=[0,5]), yaxis=dict(title = 'Positifs scattering par ml - gouttes', range=[0,5]),
    title={'text': 'Concentration en Halos (boîtes) vs concentration en<br>positifs au scattering (gouttes) - log10'})
figureFluo_1Vshalos.update_layout(
    xaxis=dict(title= 'Halos par ml - boîte', range=[0,5]), yaxis=dict(title = 'Positifs pyranine par ml - gouttes', range=[0,5]),
    title={'text': 'Concentration en Halos (boîtes) vs concentration en<br>positifs à la pyranine (gouttes) - log10'})

#comparaison des halos avec les acidificateurs en gouttes
figureCompareFracF1 = go.Figure(figureCompareFrac)
figureCompareFracF1.update_layout(title={'text': 'Fractions de microbes acidificateurs en gouttes<br>en fonction de la fraction de microbes formant un halo sur boîte (%)'})

for sample in arraySoils:
    for dilut in ('1_100', '1_1000', '1_10000'):
        if dilut == '1_1000' :
            sizeMarker=10
            symbolMarker=200
            showL=True
        else :
            sizeMarker=15
            symbolMarker=203
            showL=False

        querriedDF=DFComparePetriMDUniques[(DFComparePetriMDUniques.sol == sample) * (DFComparePetriMDUniques.dilution == dilut)]
        extraInfo=querriedDF.uniqueID
        figureCompareFrac.add_trace(go.Scatter(
            x=querriedDF.fractionHalos.round(4)*100,
            y=querriedDF.fractionScatt.round(2),
            text=extraInfo,
            legendgroup=sample,
            showlegend=showL,
            marker_symbol=symbolMarker,
            hovertemplate='%{text}<br>Petri : %{x}%<br>MD : %{y}%',
            marker=dict(color=metaD.dictOfSamplesColors[sample],size = sizeMarker),
            name=sample,
            mode='markers'))
        
        figureCompareFracF1.add_trace(go.Scatter(
            x=querriedDF.fractionHalos.round(4)*100,
            y=querriedDF.fractionF1.round(2),
            text=extraInfo,
            legendgroup=sample,
            showlegend=showL,
            marker_symbol=symbolMarker,
            hovertemplate='%{text}<br>Petri : %{x}%<br>MD : %{y}%',
            marker=dict(color=metaD.dictOfSamplesColors[sample],size = sizeMarker),
            name=sample,
            mode='markers'))
        
        figureresaVSCFU.add_trace(go.Scatter(
            x=log10(querriedDF.concentrationColonies),
            y=log10(querriedDF.MPC),
            error_x=dict(
                type='data',symmetric=False, thickness=1,
                array=log10(querriedDF.concentrationColonies+querriedDF.borneConfHautCFU)-log10(querriedDF.concentrationColonies),
                arrayminus=log10(querriedDF.concentrationColonies)-log10(querriedDF.concentrationColonies-querriedDF.borneConfBasCFU)),
            error_y=dict(
                type='data',symmetric=False, thickness=1,
                array=log10(exp(2*sqrt(querriedDF.sigmaSquare))),
                arrayminus=log10(querriedDF.MPC)-log10(querriedDF.MPC*(exp(-2*sqrt(querriedDF.sigmaSquare)))),
                ),
            text=extraInfo,
            legendgroup=sample,
            showlegend=showL,
            marker_symbol=symbolMarker,
            hovertemplate='%{text}<br>Petri : %{x}<br>MD : %{y}',
            marker=dict(color=metaD.dictOfSamplesColors[sample], size = sizeMarker),
            name=sample,
            mode='markers'))
        
        xAxisScatt=log10(querriedDF.concentrationHalos)
        yAxisScatt, yAxisFluo_1=log10(querriedDF.MPCScatt), log10(querriedDF.MPCFluo_1)
        figureScatVshalos.add_trace(go.Scatter(
            x=xAxisScatt,
            y=yAxisScatt,
            text=extraInfo,
            error_x=dict(
                type='data', symmetric=False, thickness=1,
                array=log10(querriedDF.concentrationHalos+querriedDF.borneConfHautHalos)-log10(querriedDF.concentrationHalos),
                arrayminus=log10(querriedDF.concentrationHalos)-log10(querriedDF.concentrationHalos-querriedDF.borneConfBasHalos)),
            error_y=dict(
                type='data', symmetric=False, thickness=1,
                array=log10(exp(2*sqrt(querriedDF.sigmaSquareScatt))),
                arrayminus=yAxisScatt - log10(querriedDF.MPCScatt*(exp(-2*sqrt(querriedDF.sigmaSquareScatt)))),
                ),
            legendgroup=sample,
            showlegend=showL,
            marker_symbol=symbolMarker,
            hovertemplate='%{text}<br>Petri : %{x}<br>MD : %{y}',
            marker=dict(color=metaD.dictOfSamplesColors[sample],size = sizeMarker),
            name=sample,
            mode='markers'))

        figureFluo_1Vshalos.add_trace(go.Scatter(
            x=xAxisScatt,
            y=yAxisFluo_1,
            text=extraInfo,
            error_x=dict(
                type='data', symmetric=False, thickness=1,
                array=log10(querriedDF.concentrationHalos+querriedDF.borneConfHautHalos)-log10(querriedDF.concentrationHalos),
                arrayminus=log10(querriedDF.concentrationHalos)-log10(querriedDF.concentrationHalos-querriedDF.borneConfBasHalos)),
            error_y=dict(
                type='data', symmetric=False, thickness=1,
                array=log10(exp(2*sqrt(querriedDF.sigmaSquareFluo_1))),
                arrayminus=yAxisFluo_1 - log10(querriedDF.MPCFluo_1*(exp(-2*sqrt(querriedDF.sigmaSquareFluo_1)))),
                ),
            legendgroup=sample,
            showlegend=showL,
            marker_symbol=symbolMarker,
            hovertemplate='%{text}<br>Petri : %{x}<br>MD : %{y}',
            marker=dict(color=metaD.dictOfSamplesColors[sample],size = sizeMarker),
            name=sample,
            mode='markers'))

figureresaVSCFU.add_trace(go.Scatter(
    x=[1.5], y=[3],
    text=[str("Y = {}X<br>R² = {}").format(str((1*modelCFU.coef_[0]).round(3)),str(RdeuxCFU.round(3)))],
    mode="text", showlegend=False,
    textfont=dict(size=16, color="black")))

figureresaVSCFU.add_shape(
    # Ligne régression linéaire
    dict(type="line",
        x0=0.5, y0=0.5*modelCFU.coef_[0],
        x1=log10(DFComparePetriMDUniques.concentrationColonies.max()), y1=log10(DFComparePetriMDUniques.concentrationColonies.max())*modelCFU.coef_[0],
        name="regression",
        line=dict(color="black",width=3)))

figureScatVshalos.add_trace(go.Scatter(
    x=[1], y=[3.5],
    text=[str("Y = {}X<br>R² = {}").format(str((1*modelHalos.coef_[0]).round(3)),str(RdeuxHalos.round(3)))],
    mode="text", showlegend=False,
    textfont=dict(size=16, color="black")))

figureScatVshalos.add_shape(
    # Ligne régression linéaire
    dict(type="line",
        x0=0.5, y0=0.5*modelHalos.coef_[0],
        x1=log10(DFComparePetriMDUniques.concentrationHalos.max()), y1=log10(DFComparePetriMDUniques.concentrationHalos.max())*modelHalos.coef_[0],
        name="regression",
        line=dict(color="black",width=3)))

# figureCompareFrac.add_trace(go.Scatter(
#     x=[40], y=[60],
#     text=[str("Y = {}X<br>R² = {}").format(str((1*modelFrac.coef_[0]).round(3)),str(RdeuxFrac.round(3)))],
#     mode="text", showlegend=False,
#     textfont=dict(size=16, color="black")))

# figureCompareFrac.add_shape(
#     # Ligne régression linéaire
#     dict(type="line",
#         x0=0.5, y0=0.5*modelFrac.coef_[0],
#         x1=DFComparePetriMDUniques.fractionHalos.max()*100, y1=DFComparePetriMDUniques.fractionHalos.max()*100*modelFrac.coef_[0],
#         name="regression",
#         line=dict(color="black",width=3)))

figureCompareFrac.add_trace(go.Scatter(
    x=[70], y=[80],
    text="Y=X",
    mode="text", showlegend=False,
    textfont=dict(size=20, color="black")))

figureCompareFrac.add_shape(
    # Ligne Y=X
    dict(type="line",
        x0=0, y0=0,
        x1=100, y1=100,
        name="regression",
        line=dict(color="black",width=3)))

figureCompareFrac.show(renderer=metaParam.renderer, config={'editable':True})
figureCompareFracF1.show(renderer=metaParam.renderer, config={'editable':True})
figureresaVSCFU.show(renderer=metaParam.renderer, config={'editable':True})
figureScatVshalos.show(renderer=metaParam.renderer, config={'editable':True})
figureFluo_1Vshalos.show(renderer=metaParam.renderer, config={'editable':True})

DFComparePetriMDUniques.to_csv(metaParam.pathMilliDrop+'/DFComparePetriMDUniques.csv')
# %%
# Affichage Concentrations Petri calculée à partir de detectionPositifsVSTemps en boxplots, 
figColonies = go.Figure(layout=go.Layout(
    template='seaborn', font=metaParam.taillePolice,
    width=metaParam.W, height=metaParam.H,
    xaxis=dict(zeroline=False, title='Sol'),
    yaxis=dict(zeroline=False, title = 'CPP (cellules/gramme de sol)'),
    title={'text': 'Concentration en cellules cultivables pour plusieurs sols - Petri'}))

figHalos, figFracPetri = go.Figure(figColonies), go.Figure(figColonies)

figHalos.update_layout(
    title={'text': 'Concentrations en halos pour plusieurs sols - Petri'})

figFracPetri.update_layout(
    yaxis=dict(zeroline=False, title='Fraction de halos (%)'),
    title={'text': 'Fractions de halos pour plusieurs sols - Petri'})

dataFrameConcPetri=pd.DataFrame(pd.read_csv('{}/concentrationsPetri.csv'.format(metaParam.pathAnalyseTemporelle)))

#On crée une array avec tous les noms de sol, basée sur metaD : 
arraySoils=[]
for soil in metaD.dictOfSamplesColors:
    if not soil in metaD.souches:
        arraySoils.append(soil)

arrays=(['concentrationColonies',figColonies, 5],
['concentrationHalos', figHalos, 5],
['fractionHalos',figFracPetri, 1])

#On crée un dataframe pour stocker les Coefficients de variation
DFCoeffVarPetri=pd.DataFrame(columns=['sol','concentrationColonies','concentrationHalos','fractionHalos'])

for sample in arraySoils:
    line={'sol':sample}
    for arr in arrays:
        extraInfo=dataFrameConcPetri[dataFrameConcPetri.sol == sample].date
        arr[1].add_trace(go.Box(
            y=arr[2]*dataFrameConcPetri[dataFrameConcPetri.sol == sample][arr[0]],
            text=extraInfo,
            hovertemplate='EXP%{text}<br>%{y} cellules/gramme',
            marker_color=metaD.dictOfSamplesColors[sample], 
            name=sample))
        line[arr[0]]=(arr[2]*dataFrameConcPetri[dataFrameConcPetri.sol == sample][arr[0]].std())/(arr[2]*dataFrameConcPetri[dataFrameConcPetri.sol == sample][arr[0]].mean())
    DFCoeffVarPetri=DFCoeffVarPetri.append(line, ignore_index=True)

DFCoeffVarPetri.to_csv(metaParam.pathMilliDrop+'/DFCoeffVar.csv')

for arr in arrays:
    arr[1].update_traces(boxpoints='all', jitter=0.3)
    arr[1].update_yaxes(type="log")
    arr[1].show(config={'editable': True}, renderer=metaParam.renderer)
