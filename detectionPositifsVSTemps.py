#%%
#Cellule importation
import bisect
import json
from datetime import date
from importlib import reload
from math import isnan
from os import listdir, makedirs, path
from sys import exc_info

import pandas as pd
from numpy import float64
from scipy.stats import chi2

import functionsArthur as fArt  # fonctions personnelles
import metaDonneesExperiences as metaD  # informations sur mes expériences MilliDrop
import metaParametersSheet as metaParam  # paramètres qui ne dépendent pas de l'expérience

#Cette page sert à calculer les dates de détection de croissance
# microbienne en goutte et sur boîte de Petri. En gouttes, on
# peut déteter à la fois la croissance (signal résa) et la disso-
# -lution des particules (chuttes de scattering).
#A partir de ces dates, on calcule également les concentrations
#les plus probables en microorganismes cultivables et en micro-
#-organismes solubilisateurs, à partir des équations du papier 
#de Jarvis. 

#%% --------- En gouttes ----------------------------------------
# Détection automatique des posititfs résa et plot du temps de sortie 
# Pensez à changer les path dans metaParam avant de lancer le script

#Là où aller chercher les dossiers expérience
metaParam=reload(metaParam)
securityRuns=3 #nombre de runs sous le seuil pour qu'on confirme que la goutte est bien positives

#On crée un dataFrame qui sert à stocker les date auxquelles on détecte un positif
dataFrameTimeOut=pd.DataFrame(columns=['EXP', 'goutte', 'name', 'temps_fluo_1_median', 'temps_fluo_2_median','temps_scattering_median'])
dataFrameTimeOut.set_index(['EXP', 'goutte'], inplace=True)

# for canal in 'scattering_median','fluo_2_median':
for canal in 'fluo_1_median','scattering_median','fluo_2_median':
# for canal in 'fluo_2_median':
    dataFrameMPC=pd.DataFrame(columns=['EXP', 'Sample', 'resultsDicts','MPC','sigmaSquare','rarity','concentration'])
    dataFrameUniqueMPC=pd.DataFrame(columns=['EXP', 'Sample','Dilution', 'resultsDict','MPC','sigmaSquare','concentration'])

    #On liste les expériences
    folders=[]
    for rootFolder in metaParam.rootFolders:
        folders = folders + [rootFolder +'/'+ f for f in listdir(rootFolder)]

    thresholdTimesDF=pd.DataFrame(columns=['experimentID', 'numberOfDrops', 'time'])

    for experimentPath in folders :
        #on vérifie que l'expérience contient au moins un crochet et qu'elle n'est pas dans la liste des expériences à problème        
        experimentName=experimentPath.split('/')[-1] #juste la dernière partie du chemin
        shortName=fArt.getSimplePath(experimentName) #Un nom court format : EXPXXXXXXXX_XXXX
        if not ("CONTAMINE" in experimentName or "ECHEC" in experimentName or "ALIGNEMENT" in experimentName or "FAIBLE SCATTERING" in experimentName) and '{' in experimentName:

            #On charge les paramètres
            param=json.load(open(experimentPath+'/analyseArthur/parameters.json'))
            
            #On va cherche le seuilFluo1 dans metaD
            seuilFluo1=metaD.dictOfEXPFolders[experimentPath.split('/')[-2]]['seuilFluo1']
            seuilScatt = metaD.dictOfEXPFolders[experimentPath.split('/')[-2]]['seuilScatt']

            boolNoPartic=False
            if "Koala" or "Difco" in experimentPath.split('/')[-2]: #Si on a à faire à une expérience sans particule
                print("Expérience détectée comme sans particule. Le seuil scattering de l'expérience écrase celui du lot")
                seuilScatt=param['seuilRegressionScatt']
                boolNoPartic=True

            if canal=='fluo_2_median':
                growingDrops=[] #On crée une liste, qui servira à contenir les gouttes positives à la résa

            soilsNames=fArt.getSoilName(experimentName)
            dilutions = '1_100', '1_1000'

            lines={}
            linesUnique={}
            for sampleID in soilsNames :
                lines[sampleID]={'EXP' : shortName, 'Sample' : sampleID, 'MPC' :  float("nan"), 'resultsDicts' : tuple(), 'sigmaSquare' : float("nan")}
                linesUnique[sampleID]={}
                for dilut in dilutions : 
                    linesUnique[sampleID][dilut]={'EXP' : shortName, 'Sample' : sampleID, 'Dilution' : dilut, 'MPC' :  float("nan"), 'resultsDict' : tuple(), 'sigmaSquare' : float("nan")}

            try : 
                if canal == 'scattering_median': 
                    pathNetDF=experimentPath+'/analyseArthur/netDFScatt.csv'
                    tMax=metaParam.tMaxUniqueScatt
                elif canal == 'fluo_2_median': 
                    pathNetDF=experimentPath+'/analyseArthur/netDF.csv'
                    tMax=metaParam.tMaxUniqueResa
                elif canal == 'fluo_1_median':
                    pathNetDF=experimentPath+'/analyseArthur/netDFPyra.csv'
                    tMax=metaParam.tMaxFluo1

                netDF=pd.DataFrame(pd.read_csv(pathNetDF, sep=","))

                if netDF.time.max() > metaParam.tempsMinimal*3600:
                    netDF=netDF[netDF.time < tMax*3600]

                    endTime=min(netDF.time.max(),tMax*3600)

                    metaData=fArt.importMetadata(experimentPath)
                    seuilResa=json.load(open(experimentPath+'/analyseArthur/parameters.json'))['seuilRegressionResa']
                    try : 
                        #On initialise un arrayTimes, dictionnaire destiné à contenir les dates auxquelles une goutte est sortie
                        arrayTimes={}
                        for condition in metaData['descriptionToWells']: 
                            arrayTimes[condition]=[0]

                        #Pour chaque goutte, on va chercher le temps auquel elle est sortie. On le met dans arrayTimes
                        for drop in netDF.droplet_id.unique():
                            dictTimeOut={
                                'EXP' : shortName,
                                'goutte' : drop}
                            if canal == "fluo_2_median":
                                try : 
                                    timeThreshold=netDF[(netDF.droplet_id == drop) & ((netDF.fluo_2_median > seuilResa) | (netDF.fluo_2_median < -0.11))]['time'].min()

                                except:
                                    print("Erreur avec la goutte "+str(drop)+" de l\'expérience "+experimentName)

                            elif canal == 'scattering_median':
                                try :
                                    if boolNoPartic == True: #S'il s'agit d'une manip sans particule
                                        DFrameDropUnderThresh=netDF[(netDF.droplet_id == drop) & (netDF.scattering_median > seuilScatt)]
                                    else:
                                        DFrameDropUnderThresh=netDF[(netDF.droplet_id == drop) & (netDF.scattering_median < seuilScatt)]   

                                    #Si plus de quatre points sont sous le seuil scattering
                                    if len(DFrameDropUnderThresh)>securityRuns :
                                        timeThreshold=DFrameDropUnderThresh['time'].min()
                                    else: 
                                        timeThreshold=float('nan')
                                except:
                                    print("Erreur avec la goutte "+str(drop)+" de l\'expérience "+experimentName)

                            elif canal == 'fluo_1_median':
                                try:
                                    DFrameDropUnderThresh=netDF[(netDF.droplet_id == drop) & (netDF.fluo_1_median > seuilFluo1)]
                                    # timeThreshold=netDF[(netDF.droplet_id == drop) & (netDF.fluo_1_median < seuilFluo1)]['time'].min()
                                    if len (DFrameDropUnderThresh)>securityRuns:
                                        timeThreshold=DFrameDropUnderThresh['time'].min()
                                    else:
                                        timeThreshold=float('nan')
                                except:
                                    print("Erreur avec la goutte "+str(drop)+" de l\'expérience "+experimentName)   

                            if not isnan(timeThreshold) :
                                if timeThreshold<endTime :
                                    try:
                                        condition=metaData['wellToDescription'][metaData['dropletToWell'][str(drop)]]
                                        #On insert la date en classant la liste
                                        bisect.insort(arrayTimes[condition],int(round(timeThreshold,0)))
                                        try:
                                            dataFrameTimeOut.at[(shortName,drop), 'temps_'+canal]=(timeThreshold/3600).round(2)
                                            dataFrameTimeOut.at[(shortName,drop), 'name']=fArt.replaceTemplateNameWithSoilName(condition, *soilsNames)
                                        except: 
                                            print('Impossible d\'ajouter la goutte',drop,'à dataFrameTimeOut', shortName)

                                        #On crée un tableau avec le nom des gouttes qui poussent d'après le signal de résa
                                        if canal == "fluo_2_median":
                                            growingDrops.append(str(drop))

                                    except:
                                        print("Erreur avec la goutte "+str(drop))
                                        exception_type, exception_object, exception_traceback = exc_info()
                                        filename = exception_traceback.tb_frame.f_code.co_filename
                                        line_number = exception_traceback.tb_lineno
                                        print("Exception type: ", exception_type)
                                        print("Line number: ", line_number)

                        print("\n")
                        print(experimentName)
                        #arrayTimesCondition : liste des temps pour une condition donnée sur une EXP donnée
                        for arrayTimesCondition in arrayTimes:
                            arrayTimes[arrayTimesCondition].append(endTime) #On ajoute un point pour représenter la fin de l'expérience
                            try : 
                                print(arrayTimesCondition+" : "+str(len(arrayTimes[arrayTimesCondition])-2)+" gouttes positives" )# On enlève deux pour prendre en compte le point de début (0) et le point de fin (endTime)

                                #Si on a bien à faire à un sol ou une souche, on crée un dataframe à partie de arrayTimes
                                if fArt.replaceTemplateNameWithSoilName(arrayTimesCondition, *soilsNames) != "divers" : 
                                    #On crée une colonne avec le nom de l'expérience et le nom de la condition séparés par un pipe
                                    columnExperimentID=[str("{} | {}").format(
                                        shortName,
                                        fArt.replaceTemplateNameWithSoilName(arrayTimesCondition, *soilsNames))] * len(arrayTimes[arrayTimesCondition])

                                    #On crée une colonne avec le nombre de goutte à chaque temps : 
                                    columnNumberOfDrops=list(range(len(arrayTimes[arrayTimesCondition])))
                                    columnNumberOfDrops[-1] += -1
                                    
                                    #On crée un dataFrame à partir des trois colonnes crées
                                    uniqueConditionDF=pd.DataFrame(data={'experimentID':columnExperimentID,'numberOfDrops':columnNumberOfDrops,'time':arrayTimes[arrayTimesCondition]})

                                    thresholdTimesDF=thresholdTimesDF.append(uniqueConditionDF,ignore_index=True)

                                    #Travail pour le dataFrame des MPC : 
                                    sampleDilution=columnExperimentID[0].split(' | ')[1].split(' - ')

                                    totalDrops= 0
                                    for well in metaData['descriptionToWells'][arrayTimesCondition]:
                                        totalDrops += len(metaData['wellToDroplets'][well])
                                        
                                    resultDict={
                                        'dilut' : sampleDilution[1],
                                        'positives' : len(arrayTimes[arrayTimesCondition])-2,
                                        'totalDrops' : totalDrops}
                                    lines[sampleDilution[0]]['resultsDicts'] += (resultDict,)
                                    
                                    #Pour les dilutions uniques, on considère que la solution n'est pas diluée
                                    resultDictUnique={
                                        'dilut' : '1_1' ,
                                        'positives' : len(arrayTimes[arrayTimesCondition])-2,
                                        'totalDrops' : totalDrops}
                                    linesUnique[sampleDilution[0]][sampleDilution[1]]['resultsDict']=resultDictUnique
                            except : 
                                print("erreur avec la condition "+arrayTimesCondition)
                                print(exc_info())

                        for sample in lines:
                            dataFrameMPC=dataFrameMPC.append(lines[sample], ignore_index=True)
                        del lines

                        for sample in linesUnique: 
                            for dil in dilutions:
                                dataFrameUniqueMPC=dataFrameUniqueMPC.append(linesUnique[sample][dil], ignore_index=True)

                    except:
                        print("\nErreur avec "+experimentName)
                        exception_type, exception_object, exception_traceback = exc_info()
                        filename = exception_traceback.tb_frame.f_code.co_filename
                        line_number = exception_traceback.tb_lineno
                        print("Exception type: ", exception_type)
                        print("Line number: ", line_number)
                else:
                    print("L'expérience "+shortName+" n'a pas duré assez longtemps ("+str((netDF.time.max()/3600).round(2))+" heures)")
            except:
                print("\n ---- Erreur : ")
                exception_type, exception_object, exception_traceback = exc_info()
                line_number = exception_traceback.tb_lineno
                print("Exception type: ", exception_type)
                print("Line number: ", line_number)
                print("exception_object:",exception_object)
                print("Pas de netDF ou de metaData dans le dossier "+experimentName)

            if canal == "fluo_2_median":
                #On exporte la liste des gouttes positives à la résazurine
                fname = experimentPath+'/analyseArthur/growingDrops.json' 
                with open(fname, 'w') as outfile:
                    json.dump(growingDrops, outfile, indent=2)   
    
    #Calcul de la concentration la plus probable pour chaque expérience
    print('\n----- Calcul des CPP ' + canal + ': \n')
    for rowIndex, row in dataFrameMPC.iterrows(): 
        try : 
            print(dataFrameMPC.at[rowIndex, 'EXP'] + " - " + dataFrameMPC.at[rowIndex, 'Sample'])

            #Si c'est une souche, on prend en compte toutes les dilutions, sinon on s'en remet à metaParam
            rem10000 = False if dataFrameMPC.at[rowIndex, 'Sample'] in metaD.souches else metaParam.remove10000

            MostPC, sigmaSquare, r=fArt.mostProbableNumber(metaParam.remove10000, 0.0007, *dataFrameMPC.resultsDicts[rowIndex])
            dataFrameMPC.at[rowIndex, 'MPC'] = round(MostPC,1)
            dataFrameMPC.at[rowIndex, 'sigmaSquare'] = sigmaSquare
            dataFrameMPC.at[rowIndex, 'rarity'] = r
            try : 
                dataFrameMPC.at[rowIndex, 'concentration'] = metaD.dictOfExpData[dataFrameMPC.at[rowIndex, 'EXP'][0:-5]]
            except: 
                dataFrameMPC.at[rowIndex, 'concentration'] = float('nan')
        except:
            print('Le calcul du MPN a échoué pour '+dataFrameMPC.EXP[rowIndex]+'. Vérifiez que le nom de l\'espérience est dans metaD\n')
            exception_type, exception_object, exception_traceback = exc_info()
            filename = exception_traceback.tb_frame.f_code.co_filename
            line_number = exception_traceback.tb_lineno
            print("Exception type: ", exception_type)
            print("Line number: ", line_number," Objet : ",exception_object)
    del rowIndex, row

    print('\n----- Calcul des CPP uniques ' + canal + ' : \n')

    for rowIndex, row in dataFrameUniqueMPC.iterrows():
        try : 
            print(dataFrameUniqueMPC.at[rowIndex, 'EXP'] + " - " + dataFrameUniqueMPC.at[rowIndex, 'Sample'])

            #Si c'est une souche, on prend en compte toutes les dilutions, sinon on s'en remet à metaParam
            rem10000 = False if dataFrameUniqueMPC.at[rowIndex, 'Sample'] in metaD.souches else metaParam.remove10000

            MostPC, sigmaSquare, r=fArt.mostProbableNumber(rem10000, 0.0007, dataFrameUniqueMPC.resultsDict[rowIndex])
            dataFrameUniqueMPC.at[rowIndex, 'MPC'] = round(MostPC,1)
            dataFrameUniqueMPC.at[rowIndex, 'sigmaSquare'] = sigmaSquare
            try:
                dataFrameUniqueMPC.at[rowIndex, 'concentration'] = metaD.dictOfExpData[dataFrameUniqueMPC.at[rowIndex, 'EXP'][0:-5]]
            except:
                dataFrameUniqueMPC.at[rowIndex, 'concentration'] = float('nan')
        except:
            print('Le calcul du MPN unique a échoué pour '+dataFrameUniqueMPC.EXP[rowIndex]+'. Vérifiez que le nom de l\'espérience est dans metaD\n')
            exception_type, exception_object, exception_traceback = exc_info()
            filename = exception_traceback.tb_frame.f_code.co_filename
            line_number = exception_traceback.tb_lineno
            print("Exception type: ", exception_type)
            print("Line number: ", line_number," Objet : ",exception_object)

    if not path.exists(metaParam.pathAnalyseTemporelle):
        print("""Pas de dossier analyseTemporelle trouvé. Nous en créons un""")
        makedirs(metaParam.pathAnalyseTemporelle)

    canalName=canal.split('_m')[0].capitalize() # On Prend le nom du canal avant "_median" et on le capitalise
    thresholdTimesDF.to_csv(str('{}/positifs'+canalName+'VSTemps.csv').format(metaParam.pathAnalyseTemporelle), index=False)
    dataFrameMPC.to_csv(str('{}/MPC'+canalName+'.csv').format(metaParam.pathAnalyseTemporelle), index=False, columns = ('EXP', 'Sample', 'MPC','sigmaSquare','rarity','concentration'))
    dataFrameUniqueMPC.to_csv(str('{}/MPC'+canalName+'Uniques.csv').format(metaParam.pathAnalyseTemporelle), index=False, columns = ('EXP', 'Sample','Dilution', 'MPC','sigmaSquare','concentration'))

dataFrameTimeOut.to_csv(metaParam.pathAnalyseTemporelle+'/dataFrameTimeOut.csv')
# %% -------------------------------------------------------------
# -------------- Sur boîtes --------------------------------------

#------------------------------------------------------------
# Analyse de l'apparition des colonies et des halos sur 
# boîte en fonction du temps
#------------------------------------------------------------

# # --- I) : Importation et étapes préliminaires 
# metaParam=reload(metaParam)
# def convertIntoDateTime(stringDate): #Pour convertir une date Excel en date Python 
#     from datetime import datetime
#     try:
#         dateList=stringDate.split(' ')
#         dateList[0]=dateList[0].split('/')
#         dateList[1]=dateList[1].split(':')
#     except:
#         print('Erreur dans la conversion de',stringDate)
#     return datetime(day=int(dateList[0][0]),month=int(dateList[0][1]),year=int(dateList[0][2]),hour=int(dateList[1][0]),minute=int(dateList[1][1]))

# #Le dataFrame source ; celui qui contient les informations entrées manuellement
# DFAnalyseTemporelle=pd.DataFrame(pd.read_csv(
#     metaParam.pathMilliDrop+"/facteurTempsComptagePetri_040621.csv", sep=";",
#     dtype={'colonies' : float64},
#     # dtype={'colonies' : float64, 'halos' : float64},
#     converters={'dateHeureInoculation':convertIntoDateTime,
#                 "dateHeureMesure":convertIntoDateTime}))#On utilise la fonction définie plus haut pour convertir les temps CSV en temps python

# listOfUniqueBoxes=DFAnalyseTemporelle.clefBoite.unique() #Liste des identifiants de boîtes

# # --- II) : Calcule des concentrations en cellules cultivables et en halos

# # Des Dataframes destinés à contenir les estimations de concentration, 
# # à la fois par expérience et individuellement
# DFEstimaConcentrations=pd.DataFrame(columns=['sol', 'date', 'volume' ,'colonies'])
# DFEstimaConcentrationsUniques=pd.DataFrame(columns=['uniqueID','sol', 'date', 'dilution' ,'concentrationColonies', 'concentrationHalos', "borneConfBasCFU", "borneConfHautCFU", "borneConfBasHalos", "borneConfHautHalos"])

# # On crée une colonne avec la durée d'incubation plutôt que la date
# emptyCol=['']*len(DFAnalyseTemporelle)
# DFAnalyseTemporelle["dureeIncubation"]=emptyCol #On crée une colonne vide pour stocker la durée d'incubation
# DFAnalyseTemporelle["deltaDateDeMesure"]=emptyCol #On crée une colonne vide pour stocker l'écart entre la durée d'incubatione et la durée à laquelle on souhaite garder la valeur pour analyse ultérieure

# # On calcule la durée d'incubation.
# # On ajoute un 'timeDelta' à chaque ligne du dataFrame : un écart entre la 
# # durée d'incubation et la durée d'incubation cible précisée dans metaParam 
# for i, row in DFAnalyseTemporelle.iterrows():
#     timeDelta=DFAnalyseTemporelle.dateHeureMesure[i]-DFAnalyseTemporelle.dateHeureInoculation[i]
#     DFAnalyseTemporelle.at[i,'dureeIncubation']=timeDelta.total_seconds()/3600
#     DFAnalyseTemporelle.at[i,'deltaDateDeMesure']=abs(timeDelta.total_seconds()/3600-metaParam.tMinimalPetri*24)

# #On convertit en numérique pour éviter un bug 
# DFAnalyseTemporelle['dureeIncubation'] = pd.to_numeric(DFAnalyseTemporelle['dureeIncubation'])

# DFAnalyseTemporelle['colonies'] = pd.to_numeric(DFAnalyseTemporelle['colonies'])

# for clef in listOfUniqueBoxes:
#     queriedDF=DFAnalyseTemporelle[(DFAnalyseTemporelle.clefBoite == clef)].reset_index()

#     if queriedDF.dureeIncubation.max() < metaParam.tMinimalPetri*24:
#         print("\nLa boîte "+clef+" termine trop tôt -", (queriedDF.dureeIncubation.max()/24).round(1), 'jours')
#         print(queriedDF.dateHeureInoculation.unique()[0])
#     else:
#         #On supprime les points qui contiennent trop de colonies
#         keepBox=True

#         # Valeurs que l'on retient (la première après tMinimalPetri jours d'incubation) : 
#         finalValueCFU=queriedDF[queriedDF.dureeIncubation > metaParam.tMinimalPetri*24].colonies.min() #On prend le premier point après tMinimalPetri
#         # finalValueHalos=queriedDF[queriedDF.dureeIncubation > metaParam.tMinimalPetri*24].halos.min()
#         if queriedDF.tailleBoite.unique()[0] == 'petite':
#             if finalValueCFU>300 :
#                 print('La boîte '+clef+' a trop de colonies.', finalValueCFU)
#                 keepBox=False
#         elif queriedDF.tailleBoite.unique()[0] == 'grande':
#             if finalValueCFU>600 :
#                 print('La boîte '+clef+' a trop de colonies.', finalValueCFU)
#                 keepBox=False
#         if len(queriedDF.boite.unique())<2:
#             dilutionString = queriedDF.boite.unique()[0].split(' |')[0]
#             facteurDilution=int(dilutionString.split('_')[1])

#             if keepBox:
#                 tailleBoite=queriedDF.tailleBoite.unique()[0]

#                 volumePrise=0.1 if tailleBoite=='petite' else 0.2
#                 volume=volumePrise/facteurDilution #Volume de la suspension initiale qui s'est effectivement retrouvé sur la boîte

#                 sol = queriedDF.sol.unique()[0]
#                 date = str(queriedDF.dateHeureInoculation.unique()[0])[:10].replace('-','')

#                 intervalConfianceCFU=[(chi2.ppf([0.025], finalValueCFU*2)[0]/(2*volumePrise)).round(2),
#                 (chi2.ppf([0.975], 2+finalValueCFU*2)[0]/(2*volumePrise)).round(2)]

#                 # intervalConfianceHalos=[(chi2.ppf([0.025], finalValueHalos*2)[0]/(2*volumePrise)).round(2), (chi2.ppf([0.975], 2+finalValueHalos*2)[0]/(2*volumePrise)).round(2)]

#                 concCFU=finalValueCFU/volumePrise
#                 # concHalos=finalValueHalos/volumePrise

#                 DFEstimaConcentrations=DFEstimaConcentrations.append({
#                     'sol' : sol, 
#                     'volume' : volume, 
#                     'date' :  date, 
#                     'colonies' : finalValueCFU}, 
#                     # 'halos' : finalValueHalos}, 
#                     ignore_index=True)
                
#                 DFEstimaConcentrationsUniques=DFEstimaConcentrationsUniques.append({
#                     'uniqueID':"EXP{} - {} - {}".format(date, sol, dilutionString),
#                     'sol' : sol, 
#                     'date' :  date, 
#                     'dilution' : dilutionString,
#                     'concentrationColonies' : concCFU,
#                     'borneConfBasCFU':concCFU-intervalConfianceCFU[0] if not isnan(intervalConfianceCFU[0]) else float(0),
#                     'borneConfHautCFU':intervalConfianceCFU[1]-concCFU,
#                     # 'concentrationHalos' : finalValueHalos/volumePrise, 
#                     # 'borneConfBasHalos':concHalos-intervalConfianceHalos[0] if not isnan(intervalConfianceHalos[0]) else float(0),
#                     # 'borneConfHautHalos':intervalConfianceHalos[1]-concHalos,
#                     # 'fractionHalos' : finalValueHalos/finalValueCFU
#                     }, 
#                     ignore_index=True)
#                 # print(intervalConfianceHalos[0] if not isnan(intervalConfianceHalos[0]) else 0)
#         else:
#             print('Problème avec la boîte',clef,': plus d\'un nom de boîte pour cette clef')

# #On additionne toutes les valeurs de volume, de colonies et de halos, en groupant par sol et date : 
# DFEstimaConcentrations = DFEstimaConcentrations.groupby(['sol', 'date']).sum()
# DFEstimaConcentrationsUniques=DFEstimaConcentrationsUniques.groupby(['uniqueID','sol','date','dilution']).mean()
# DFEstimaConcentrationsUniques.reset_index(level=['sol','date','dilution','uniqueID'], inplace=True)

# #On calcule ensuite une concentration en colonies et en halos ; on ajoute des identifiants:
# for rowIndex, row in DFEstimaConcentrations.iterrows():
#     DFEstimaConcentrations.at[rowIndex, 'uniqueID'] = "EXP{} - {}".format(rowIndex[1],rowIndex[0])
#     DFEstimaConcentrations.at[rowIndex, 'sol'], DFEstimaConcentrations.at[rowIndex, 'date'] = rowIndex
#     DFEstimaConcentrations.at[rowIndex, 'concentrationColonies'] = row['colonies']/row['volume']
#     # DFEstimaConcentrations.at[rowIndex, 'concentrationHalos'] = row['halos']/row['volume']
#     # DFEstimaConcentrations.at[rowIndex, 'fractionHalos'] = 100*row['halos']/row['colonies']

# #On exporte en CSV:
# DFEstimaConcentrations.to_csv(str('{}/concentrationsPetri.csv').format(metaParam.pathAnalyseTemporelle), index=False)
# DFEstimaConcentrationsUniques.to_csv(str('{}/concentrationsPetriUniques.csv').format(metaParam.pathAnalyseTemporelle), index=False)

# %%
