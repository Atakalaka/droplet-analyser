#%%
import json
from math import isnan
from os import path
from re import findall
from sys import exc_info
from tkinter import filedialog
from scipy.optimize import curve_fit
import colour as co
from random import uniform

import pandas as pd
import plotly.graph_objects as go

def linear_func(x,a):
    return a*x

def dropsWell(wellsArray, pathDirectory, metaData):
    """
    Cette fonction prend en entrée une liste de nombres 
    et de noms de puits destinée à être affiché, et donne 
    en sortie des listes avec les numéros de gouttes 
    correspondants 
    """
    listDropsWell=[]
    listOfDescriptions=metaData["descriptionToWells"]

    for colorGroup in wellsArray :
        listDropsColorG=[] #On initie une array avec toutes les gouttes qu'on veut de la même couleur

        for element in colorGroup :
            if type(element) is int: #Si l'entrée contient des entiers, ils sont interprétés comme des numéros des gouttes
                listDropsColorG+=[element]
                print(str(element)+" ajouté avec succès")

            elif type(element) is str: #Si l'entrée contient des chaînes, elles sont interprétées comme des puits ou comme des descriptions
                if element in listOfDescriptions: #Si l'entrée est interprétée comme un nom de description
                    # print(element +' reconnu comme faisant partie de listOfDescriptions')
                    for well in metaData["descriptionToWells"][element]: 
                        for drop in metaData["wellToDroplets"][well]: 
                            listDropsColorG+=[drop]
                    print(str(element)+" ajouté avec succès")        
                else:
                    if element in metaData['wellToDroplets']:#Si l'élément est reconnu comme étant un puits
                        try:
                            for drop in metaData['wellToDroplets'][element]:
                                listDropsColorG+=[drop]
                            print(str(element)+" ajouté avec succès")
                        except:
                            print('Problème rencontré avec le nom de puits ou goutte ('+ str(element) +')')
                    # else : 
                    #     print(element + ' non trouvé')
        listDropsWell.append(listDropsColorG)
    
    return listDropsWell

def choixDossier(pathMilliDrop):
    """
    Une fonction qui ouvre une fenêtre pour parcourir les dossiers
    et sélectionner le dossier d'expériences désiré
    """
    try:
        pathExp=filedialog.askdirectory(initialdir=pathMilliDrop, title="Dossier de l'experience désirée")
    except:
        pathExp=filedialog.askdirectory(title="Dossier de l'experience désirée")    

    return pathExp    

def importMetadata (pathEXP):
    """
    Importe le fichier metadata créé par DropSignal
    Attention : les gouttes sont réindexées pour partir de 1 ; il n'y a pas de goutte 0
    """
    metaData=dict()
    with open(pathEXP+'/analysis/metadata.json') as json_file:
        metadataPython = json.load(json_file)
        
        #On bâtit une liste des noms de catégorie (ex : "controle", "sol_1:100")
        listOfDescriptions=[]
        for category in metadataPython["color_group"]: 
            listOfDescriptions.append(category)
        
        metaData["listOfDescriptions"]=listOfDescriptions

        #On bâtit un dictionnaire qui associe à chaque goutte un puits
        dropletToWell={} #un dictionnaire 
        for droplet in metadataPython["droplet_to_well"]:
            dropletToWell[str(int(float(droplet))+1)]=metadataPython["droplet_to_well"][droplet]
        metaData["dropletToWell"]=dropletToWell

        #On bâtit un dictionnaire qui associe à chaque puits la description de son contenu
        wellToDescription={}
        for well in metadataPython["well_to_group"]:
            wellToDescription[well]=metadataPython["well_to_group"][well]
        metaData["wellToDescription"]=wellToDescription

        #On bâtit un dictionnaire qui associe à une description les puits correspondants
        descriptionToWells={}
        for drop in metadataPython["droplets"]:
            descriptionToWells[drop["label"]]=drop["well"]
        metaData["descriptionToWells"]=descriptionToWells

        #On bâtit un dictionnaire qui associe à chaque puits un ensemble de gouttes
        wellToDroplets={}
        for well in metadataPython["well_to_droplets"]:
            wellToDroplets[well]=[x+1 for x in metadataPython["well_to_droplets"][well]]#On implémente de 1 les gouttes par rapport au JSON orginal
        metaData["wellToDroplets"]=wellToDroplets

        #On extrait le mode de visite (hiccup ou snake)
        metaData['visitMode']=metadataPython["visit_mode"]

    return metaData

def getSimplePath(pathEXP):
    """
    Donne le titre de l'expérience sans le chemin complet, 
    notamment afin d'être utilisé pour les titres de figures
    """

    simplePath = pathEXP.split("/")[-1]
    simplePath = simplePath[:16]
    return simplePath

def checkReferenceDrop(pathEXP, metaData):
    """
    Cette fonction renvoie True si elle trouve une goutte de référence, False sinon. 
    """
    if not path.exists(pathEXP+'/analyseArthur/referenceDrops.json'):
        print('Unable to find any reference drop in the experiment folder')
        return False
    else:
        refDrop=json.load(open(pathEXP+'/analyseArthur/referenceDrops.json'))
        if refDrop in metaData['dropletToWell']:
            return True
        else: 
            print('There is a reference drop ID, but it is not a correct one for this experiment')
            return False

def createNetDataFrame(DataF,refDrop,runMax, canal="fluo_2_median", firstRun=0, toZero=True, newPipeline=False):
    colToUseNetDF=["time",canal,"droplet_id","run"]
    # On crée un Dataframe qui contient tous les points de DataF avant runMax
    netDF=DataF[colToUseNetDF].query('run <= '+str(runMax)).reset_index(drop=True).copy()
    dfReference=DataF.query('droplet_id == {} and run <= {}'.format(refDrop,str(runMax))).reset_index(drop=True)

    # ----Ajustement du temps pour faire partir la manip du premier run mesuré :
    timeMin=(netDF.iloc[0].time/3600).round(2)
    runMin=netDF.run.min()
    deltaTime=(timeMin-runMin*0.5)*3600 #La quantité de temps à enlever à chaque valeur de temps
    if newPipeline: #Avec le nouveau pipeline, je suis obligé de décaler les runs de 1. Je compense cela ici
        deltaTime+=0.5*3600
    
    # On enlève les runs avant le firstRun spécifié dans les paramètres.
    netDF=netDF.query('run >= '+str(firstRun)).reset_index(drop=True)

    lengthRef=len(dfReference) #Ca va être égal au nombre de points de mesure de la goutte de reference
    length=len(netDF)

    # Partie 1 : on soustraie au signal le signal de la goutte de référence
    # On crée un dictionnaire ; chaque entrée correspond à un run, 
    # et à la fluo de la goutte de référence à ce run
    dictReference={}
    i=0
    for i in range (lengthRef):
        dictReference[dfReference.loc[i,'run']]=dfReference.loc[i,canal]

    i=0
    for i in range (length):
        netDF.loc[i,canal]-=dictReference[netDF.loc[i,'run']]
        netDF.loc[i,"time"]-=deltaTime

    #Partie 2 : on soustraie à chaque goutte son signal initial (moyenne des 4 premiers runs)
    if toZero:
        dictInitialValues={}
        for drop in netDF.droplet_id.unique():
            dictInitialValues[drop]=netDF.query("droplet_id == {0} & run < {1} & run > {2}".format(str(drop), str(firstRun+5), str(firstRun)))[canal].mean()

        for index, row in netDF.iterrows():
            netDF.at[index,canal] = netDF.at[index,canal] - dictInitialValues[row['droplet_id']]
    
    #Bonus : on enlève la colonne parasite qui se crée
    netDF= netDF.loc[:, ~netDF.columns.str.contains('^Unnamed')]

    del dictReference, dfReference
    return netDF

def lookforGrowingDrops(netDataF,metaData,TMax, canal="fluo_2_median", upThreshold=0.05, downThreshold=-0.15): 
    """
    A partir d'un dataframe auquel on a soustrait le signal de résa de gouttes de 
    référence, on détermine quelles sont les gouttes dont le signal dépasse un seuil
    (par défaut 0.05). 
    Sortie : un dictionnaire avec le nom de goutte en index des valeurs "max",
    "maxDate", "tDiv", "pseudoLag" et "paramEpsilon" avec des valeurs par défaut
    TMax : temps d'incubation maximal en heures, en dessous duquel on coupe (conçu pour être
    metaParam.tMaxUniqueResa)
    """
    growingDrops={}
    emptyDrops=[]
    fallingDrops=[]
    netDataF=netDataF[netDataF.time<TMax*3600] #On supprime tous les points après TMax

    for drop in metaData['dropletToWell']:
        dropMax=netDataF.query('droplet_id == @drop')[canal].max()
        try:
            if dropMax > upThreshold:
                growingDrops[drop]={'maximum':dropMax,
                'lastRun':float(0),
                'tDiv':float('nan'),
                'pseudoLag':float('nan'),
                'paramEpsilon':float('nan'),
                'lagTime':float('nan'),
                'variancetDiv':float('nan'),
                'varianceLag':float('nan'),
                'varianceEpsi':float('nan'),
                'description':metaData['wellToDescription'][metaData['dropletToWell'][drop]]}
            else:
                dropMin=netDataF.query('droplet_id == @drop')[canal].min()
                #Certaines gouttes poussent mais font directement descendre la résazurine, je les compte dans fallingDrops.
                # Dans emptyDrops, j'ajoute donc les goutte qui ne passent ni sous downThreshold ni au dessus de upThreshold : 
                if dropMin > downThreshold:
                    emptyDrops.append(int(drop))
                else: #Les gouttes qui poussent mais qui font chuter le signal de résa. 
                    fallingDrops.append(int(drop))
        except:
            print("Drop "+str(drop)+" could not be analyzed by lookForGrowingDrops. ")

    dfgrowingDrops=pd.DataFrame.from_dict(growingDrops, columns=['description','maximum','lastRun','tDiv','pseudoLag','paramEpsilon','variancetDiv', 'varianceLag', 'varianceEpsi', 'lagTime'], orient='index')

    return dfgrowingDrops, emptyDrops, fallingDrops

def isInTheGroup(dropOrWell,description,metaData):
    """
    Retourne True si le puits ou la goutte correspond à la description en 
    entrée. False sinon. 
    """
    try:
        dropOrWell=str(dropOrWell)
    except:
        print('dropOrWell could not be converted into a string')
        return False
    
    if dropOrWell in metaData['dropletToWell']:
        if metaData['wellToDescription'][metaData['dropletToWell'][dropOrWell]]==description:
            return True

    elif dropOrWell in metaData['wellToDescription']:
        if metaData['wellToDescription'][dropOrWell]==description:
            return True

    else:
        print(dropOrWell+' ne fait pas partie des gouttes ni des puits de l\'expérience')
        return False

def timeMaxToRunMax(timeMax,DataF): 
    """
    Cette fonction convertit un temps maximum en run maximum sur une expérience donnée
    """

    df2=DataF.query('time <= '+str(timeMax*3600))
    runMax=df2.max()['run']

    return runMax

def drawVSplot(pathOfCurrentEXP, mainDataFrame, param, metaParam, listDrops, metaData, seuilScattering, seuilFluo1, tMax =38, normalizeScatt=False, randomizeCol=False):
    experiments=getSoilName(pathOfCurrentEXP) #nom des expériences d'après le nom du dossier d'expériences
    strExperiments=""
    for experiment in experiments:
        strExperiments=strExperiments + " " + experiment

    stringTitle=str('Lumière diffusée en fonction de la fluorescence de la pyranine | {}<br>maxTime : {}h | {}').format(getSimplePath(pathOfCurrentEXP),str(tMax),strExperiments)
    idColor=0
    
    figFluo1VSScatt=go.Figure(layout=go.Layout(
        template='seaborn', font=metaParam.taillePolice,
        width=metaParam.W, height=metaParam.H,
        xaxis=dict(zeroline=False, title='Fluorescence de la pyranine (UA)', type='log'),
        yaxis=dict(zeroline=False, title='Scattering (UA)', range=[0,1.1] if normalizeScatt else [-4, 0.5] ),
        title={'text': stringTitle}))

    for dropArray in listDrops:
        firstDrop=True
        for drop in dropArray:
            #On crée une valeur moyenne initiale pour chaque goutte (moyenne des 4 premiers runs)
            queryString4Runs="droplet_id == {0} & run < 5 & run > 0".format(str(drop))
            # initialValue=1
            initialValue=mainDataFrame.query(queryString4Runs)["scattering_median"].mean()

            #On crée une chaîne de query qui servira à faire une requête sur le dataFrame principal : 
            queryString="droplet_id == {0} & run > {1} & time < {2}".format(str(drop),str(param['runMin']),str(tMax*3600))

            #Un colorgroupe pour plotly : 
            nomDuGroupe=metaData['wellToDescription'][metaData['dropletToWell'][str(drop)]]
            try:
                extraInfo=mainDataFrame.query(queryString)["droplet_id"]

                if randomizeCol:
                    couleurLigne=randomizeColor(metaParam.matColors[idColor])
                else:
                    couleurLigne=metaParam.matColors[idColor]

                figFluo1VSScatt.add_trace(go.Scatter(
                    x=mainDataFrame.query(queryString)["fluo_1_median"],
                    y=mainDataFrame.query(queryString)['scattering_median']/initialValue if normalizeScatt else mainDataFrame.query(queryString)['scattering_median']-initialValue ,
                    legendgroup=str(dropArray), 
                    line=dict(color=couleurLigne, width=2),
                    # mode='lines+markers',
                    showlegend = True if firstDrop else False,
                    text=extraInfo,
                    hovertemplate='Drop : %{text}',
                    name=nomDuGroupe))
                firstDrop=False
            except:
                print("Erreur avec la ligne fluo vs scatt de la goutte "+str(drop))
                print("Info sur l'erreur :", exc_info()[0],exc_info()[1],exc_info()[2])
        
        if idColor < len(metaParam.matColors):
            idColor+=1
    
    figFluo1VSScatt.add_shape(dict(
        type="line", name='Seuil de regression',
        x0=0.01, y0=seuilScattering,
        x1=4.5, y1=seuilScattering,
        line=dict(color="black",width=2)))
    
    figFluo1VSScatt.add_shape(dict(
        type="line", name='Seuil de regression',
        x0=seuilFluo1, y0=-4,
        x1=seuilFluo1, y1=0.5,
        line=dict(color="black",width=2)))

    # On affiche la figure - "renderer" précise le mode d'affichage, par exemple "browser"
    # Voir https://plot.ly/python/renderers/
    figFluo1VSScatt.show(renderer=metaParam.renderer, config={'editable': True})

def getSoilName(experimentPath):
    """
    Cette fonction cherche les nom entre accolades dans la chaîne en entrée. 
    """
    searchStringSoil="\{.{1,19}\}"
    arrayNames=findall(searchStringSoil,experimentPath)
    
    #On enlève les crochets
    i=0
    for soilName in arrayNames:
        arrayNames[i]=soilName[1:-1]
        i+=1
    return arrayNames

def replaceTemplateNameWithSoilName(templateName, sol1Name, sol2Name="divers", sol3Name="divers", sol4Name="divers"):
    """
    Cette fonction permet de remplacer un nom de condition de template (par exemple "Sol 2 - 1_100") par un nom de sol ou d'échantillon propre (par exemple "E Coli - 1_100"). 
    Il retourne "divers" s'il ne trouve pas la chaîne "1_" ou s'il ne trouve pas 
    les chaînes "2", "3", "4", "Sol" ou "Souche"...
    """
    #Exemples : numericalPart : "1_1000", soilNamePart : "TS 020120"
    soilNamePart = ""
    if "1_" in templateName:
        numericalPart = templateName.split("1_")[1] # On va récupérer la partie après "1_" dans templateName - Exemple : "100"
        #Si il ne s'agit pas de nombres (par exemple si on a "1_100 - sans particule") on retourne "divers"
        if not numericalPart.isnumeric():
            return("divers")
        else : 
            numericalPart="1_"+numericalPart #On remet le "1_"
            templateLeftPart = templateName.split("1_")[0] #Exemple : "Sol 1 - "
            if "2" in templateLeftPart:
                soilNamePart=sol2Name
            elif "3" in templateLeftPart:
                soilNamePart=sol3Name
            elif "4" in templateLeftPart:
                soilNamePart=sol4Name
            elif "Sol" in templateLeftPart or "sol" in templateLeftPart or "Souche" in templateLeftPart or "EGM" in templateLeftPart :
                soilNamePart=sol1Name
            else:
                return("divers")
            return("{} - {}".format(soilNamePart, numericalPart))
    else:
        return("divers")

def mostProbableNumber(exclude1_10000=False, volume = 0.0007, *dictResults):
    """
    Cette fonction prend un tuple de dictionnaires de valeurs de dilution, de gouttes totales et de gouttes pleines et retourne la concentration la plus probable
    Exemple de tuple de dictionnaires : 
    {'dilut':'1_100', 'totalDrops' : 110, 'positives' : 73},
    {'dilut':'1_1000', 'totalDrops' : 110, 'positives' : 9}
    exclude1_10000 sert à exclure les dilutions à 1:10 000
    """
    from math import exp, log

    import numpy as np
    from scipy.optimize import fsolve
    from scipy.special import comb #factoriel
    e=exp(1)

    onlyZeroes=True #Un booléen pour si on n'a que des zéros
    onlyOnes=True #Un booléen pour si on a que des gouttes positives

    def internalFunc(z):
        x = z[0]
        F = np.zeros((1))
        for index, conditionDict in enumerate(dictResults):
            if not (exclude1_10000 and conditionDict['dilut'] == '1_10000'):
                n=conditionDict['totalDrops']
                d=1/float(conditionDict['dilut'].split('_')[1])
                xi=min(conditionDict['positives'],conditionDict['totalDrops'])
                w=volume
                dw=d*w

                F[0] += xi*dw/(1-e**(-dw*x)) - n*dw

        return F
    #On crée une valeur calculée grossièrement qui sert de point de départ à l'optimisation 
    try :
        initialValue=dictResults[0]['positives']/(dictResults[0]['totalDrops']*(1/float(dictResults[0]['dilut'].split('_')[1]))*volume )
    except:
        initialValue=100

    try :
        zGuess = np.array([initialValue])
        MostPC = fsolve(internalFunc,zGuess)[0]

        #Calcul de sigma carré et de l'indice de rareté
        somme=0
        Llist=[]
        LlistMax=[] #liste des likelihoods avant passage à l'exponentielle
        for index, conditionDict in enumerate(dictResults):
            if not (exclude1_10000 and conditionDict['dilut'] == '1_10000'):
                d=1/float(conditionDict['dilut'].split('_')[1])
                xi=conditionDict['positives']
                w=volume
                dw=d*w
                n=conditionDict['totalDrops']

                somme+=exp(-dw*MostPC)*xi*dw**2/((1-exp(-dw*MostPC))**2)

                Li=log(comb(n,xi,exact=True))+xi*log(1-e**(-dw*MostPC))-(n-xi)*dw*MostPC
                
                xiM=int((n+1)*(1-e**(-dw*MostPC))) 
                #le nombre de gouttes positives avec la vraissemblance la plus élevée sachant le MPN trouvé
                LiMax=log(comb(n,xiM,exact=True))+xiM*log(1-e**(-dw*MostPC))-(n-xiM)*dw*MostPC

                Llist.append(Li)
                LlistMax.append(LiMax)

        r=e**sum(Llist)/e**sum(LlistMax) #L'indice de rareté
        sigmaS=1/(somme*MostPC**2)

        if MostPC == initialValue :
            MostPC = float('nan')
            sigmaS = float('nan')
            r=float('nan')
    
    except: 
        print('Détermination du MPN impossible\n')
        for index, conditionDict in enumerate(dictResults):
            if not (exclude1_10000 and conditionDict['dilut'] == '1_10000'):
                print('total : ',conditionDict['totalDrops'],'positives :', conditionDict['positives'])
            
        MostPC = float('nan')
        sigmaS = float('nan')
        r=float('nan')
    
    for index, conditionDict in enumerate(dictResults):
        if not (exclude1_10000 and conditionDict['dilut'] == '1_10000'):
            xi=conditionDict['positives']
            n=conditionDict['totalDrops']
            if xi>0:
                onlyZeroes=False
            
            if xi<n:
                onlyOnes=False

    if onlyZeroes:
        MostPC = float(0)
        sigmaS = float(0)
        r=float(1)
    elif onlyOnes:
        MostPC = float('nan')
        sigmaS = float('nan')
        r=float('nan')

    return MostPC, sigmaS, r

def regression(pathOfCurrentEXP, netDF, growingDrops, param, metaParam ,metaData, seuil, channel = "fluo_2_median", channelName="resazurine", displayCurves=True, askOverwrite = True, checkStandardDev=True):
    """
    Cette fonction réalise une régression sur des signaux de fluorescence de résazurine. 
    pathOfCurrentEXP : chemin de l'expérience
    netDF : dataframe contenant les signaux de résazurine 'nets', c'est à dire auxquels on a soustrait le signal de la refDrop
    param : les paramètres de l'expériences, contenus dans un JSON par défaut
    metaParam : les métaParamètres (principalement utilisé pour le display des graphes)
    displayCurves : souhaite-t-on afficher les résultats de la régression ? 
    askOverwrite : Demander avant d'écraser les précédents résultats ? 
    checkStandardDev : Supprimer les courbes dont les valeurs de variance sont à plus de 3 écart-types de la moyenne de variance
    """
    from numpy import linspace, log, log10

    #On définit les fonctions pour le fitting : 
    def exponencial_func_epsilon_log(t, a, lag, epsilon):
        return log(2**((t-lag)/a) + epsilon)
    def exponencial_func_epsilon(t, a, lag, epsilon):
        return 2**((t-lag)/a) + epsilon

    frames=[]
    #On stocke les gouttes positives qui n'ont pas pu être fittées dans failedDrops
    failedDrops=[]

    #on enlève les runs après tMaxUniqueResa, afin que toutes les manips ait été arrétées au même temps
    netDF=netDF[netDF.time < metaParam.tMaxUniqueResa*3600]

    for i, row in growingDrops[0].iterrows():

        dfDrop=netDF[(netDF.droplet_id == int(i)) & (getattr(netDF, channel) > seuil)].copy()
        firstRun=dfDrop['run'].min() #Le premier run au dessus du seuil
        dfDrop=dfDrop[dfDrop.run<=(firstRun+param['maxNumbersOfRuns'])]

        #On cherche la dérivée maximale, sur runs de même parité (d'ou périods=2), on prend soit cette valeur là, soit 
        # le run avant le run du point le plus haut, si celui-ci a lieu avant (signe qu'il y a eu un pb de type coalescence)
        # soit les param['maxNumbersOfRuns'] runs qui suivent le dépassement du seuil "para['.']seuilRegression" 

        # growingDrops[0].at[i,'lastRun'] = dfDrop.query(channel+' > '+str(seuil))['run'].min()+param['maxNumbersOfRuns']
        # try:
        if dfDrop.droplet_id.size >= 4: #Si plus de trois lignes dans le DF
            lastRun=int(dfDrop.loc[dfDrop.diff(periods=2)[channel].idxmax()]['run'])
            growingDrops[0].at[i,'lastRun'] = lastRun
            print (i,"- lastRun : ",lastRun)

            dfBeforePeak=dfDrop[(dfDrop.run <= lastRun) & (dfDrop.run >= param['runMin'])]
        #-------- Parsing des points post-pic et régression 

            if len(dfBeforePeak)>3:
                #On calcule la régression, sur des dataframes des gouttes avant leur pic
                timeAxis=dfBeforePeak['time']/3600
                fluoAxis=log(dfBeforePeak[channel])
                try:
                    #La régression à proprement parler
                    #p0 : valeurs initiales ; bounds : limites imposées pour chaque paramètre
                    popt, pcov = curve_fit(exponencial_func_epsilon_log, timeAxis,
                    fluoAxis, p0=param['p0ExpEps'], bounds=param['boundsExpEps'])

                    growingDrops[0].at[i,'tDiv']=popt[0]
                    growingDrops[0].at[i,'pseudoLag']=popt[1]
                    growingDrops[0].at[i,'paramEpsilon']=popt[2]
                    growingDrops[0].at[i,'lagTime']=popt[1]+(popt[0]/log(2)*log(seuil-popt[2]))
                    growingDrops[0].at[i,'variancetDiv']=pcov[0,0]
                    growingDrops[0].at[i,'varianceLag']=pcov[1,1]
                    growingDrops[0].at[i,'varianceEpsi']=pcov[2,2]

                except RuntimeError:
                    print('La régression a pris trop de temps pour la goutte '+str(i))
                    failedDrops.append(int(float(i)))

                except ValueError:
                    print('ValueError on droplet '+str(i)+'. Try to change seuilRegression')
                    print(exc_info()[1])
                    failedDrops.append(int(float(i)))

                frames.append(dfBeforePeak)

            else:
                failedDrops.append(int(float(i)))

        else:
            print('Drop '+i+' has too few points over detection threshold to conduct a proper fitting')
            failedDrops.append(int(float(i)))

    print('Number of failed drops:'+str(len(failedDrops)))

    dataFrameBeforePeaks=pd.concat(frames, ignore_index=True)

    del frames, dfBeforePeak

    if checkStandardDev:
        #On supprime les outliers en matière de varianceDiv et pseudolag : tous les points
        #à plus de 3 ecart-types de la moyenne, après passage en log10
        meanVartDiv, meanVarPseudoLag=log10(growingDrops[0].variancetDiv).mean(), log10(growingDrops[0].varianceLag).mean()
        stdDVartDiv, stdDVarPseudoLag=log10(growingDrops[0].variancetDiv).std(), log10(growingDrops[0].varianceLag).std()
        thresholdtDiv, thresholdPseudoLag=meanVartDiv+3*stdDVartDiv, meanVarPseudoLag+3*stdDVarPseudoLag

        for i, row in growingDrops[0].iterrows(): # On parcourt les lignes à la recherche de variances anormalement élevées.
            if log10(row.variancetDiv) > thresholdtDiv or log10(row.varianceLag)> thresholdPseudoLag:
                print("row",str(i),"supprimée car variance trop élevée")
                growingDrops[0].drop(i, inplace=True)
                failedDrops.append(int(float(i)))

    # -- On trie growingDrops[0] par description
    growingDrops[0].sort_values(by=['description'],inplace=True,kind='mergesort')
    
    if displayCurves:
        # -------------- On visualise
        figResaBeforePeak=go.Figure(layout=go.Layout(
            autosize=False,
            font=metaParam.taillePolice,
            template='seaborn',
            width=metaParam.W, height=metaParam.H,
            xaxis=dict(zeroline=False, dtick=3,title="""Temps d'incubation en heures""", range = [0, metaParam.tMaxUniqueResa]),
            yaxis=dict(zeroline=False, title='Fluorescence de la résazurine'),
            yaxis_type="linear"))

        for description in metaData["descriptionToWells"]:
            atLeastOneDrop=False #S'il n'y a pas de goutte positive, on n'affiche rien
            figResaBeforePeak.data = []
            descriptionDF=growingDrops[0]['description'] == description

            #On affiche chaque courbe dans une fenêtre du browser différente
            for i, row in growingDrops[0][descriptionDF].iterrows():
                #On crée un dataframeBeforePeak avec uniquement la goutte i
                iDropDFBeforePeak=dataFrameBeforePeaks[dataFrameBeforePeaks.droplet_id == int(i)]
                if not iDropDFBeforePeak.empty:
                    extraInfo=iDropDFBeforePeak["run"]
                
                    xAxis=(iDropDFBeforePeak["time"])/3600
                    yAxis=iDropDFBeforePeak[channel]

                    # J'affiche les param['dottedRuns'] points suivants la fin de la régression en pointillés
                    dfDotted=netDF[(netDF.droplet_id == i) & (netDF.run >= row['lastRun']) & (netDF.run < (row['lastRun']+param['dottedRuns']))]

                    extraInfoDotted=dfDotted["run"]
                    xAxisDotted=(dfDotted["time"])/3600
                    yAxisDotted=dfDotted[channel]

                    #On crée un axe de 20 valeurs régulières dont la première correspond à la
                    #date du premier point de mesure de la goutte i et la dernière, au dernier
                    try:
                        xAxisRegression=linspace(
                            (iDropDFBeforePeak["time"].iloc[0])/3600,
                            (iDropDFBeforePeak["time"].iloc[-1])/3600, num = 20)
                        
                        yAxisRegression=exponencial_func_epsilon(xAxisRegression,row['tDiv'],row['pseudoLag'],row['paramEpsilon'])

                        #On trace les valeurs mesurées
                        figResaBeforePeak.add_trace(go.Scatter(
                            x=xAxis, y=yAxis,
                            text=extraInfo,
                            legendgroup='drop '+str(i),
                            hovertemplate=
                                'Run: %{text}<br>Time: %{x}<br>Value: %{y}<br>variancetDiv : '+str(row['variancetDiv'])[0:4],
                            mode='lines+markers',                
                            line=dict(color=metaParam.matColors[0]),
                            name='drop '+str(i)+' - signal'))
                        
                        #On trace en pointillés les premiers points non-fittés
                        figResaBeforePeak.add_trace(go.Scatter(
                            x = xAxisDotted, y = yAxisDotted,
                            legendgroup = 'drop '+str(i),
                            text = extraInfoDotted,
                            showlegend = False,
                            hovertemplate = 'Run: %{text}<br>Time: %{x}<br>Value: %{y}',
                            mode = 'lines',                
                            line = dict(color=metaParam.matColors[0], dash ='dot'),
                            name = 'drop '+str(i)))
                        
                        #On trace les régressions en jaune
                        figResaBeforePeak.add_trace(go.Scatter(
                            x=xAxisRegression,
                            y=yAxisRegression,
                            hovertemplate='',
                            line=dict(color=metaParam.matColors[1]),
                            name='drop '+str(int(float(i)))+' - régression'))

                        #Ajout d'une droite pour représenter le seuil de régression 
                        figResaBeforePeak.add_shape(
                            # Ligne au delà de laquelle on ne peut pas trouver de lagTime
                            dict(
                                type="line",
                                name='Seuil de regression',
                                x0=0, y0=seuil,
                                x1=param["maxTime"],y1=seuil,
                                line=dict(color="black", width=3)))

                        atLeastOneDrop=True
                    except:
                        print("\nErreur d\'affichage pour i =",i)
                        exception_type, exception_object, exception_traceback = exc_info()
                        line_number = exception_traceback.tb_lineno
                        print("Exception type: ", exception_type)
                        print("Line number: ", line_number)
                        print("exception_object:",exception_object)
                else:
                    print("iDropDFBeforePeak is empty for drop",i)

            #On affiche dans le browser
            if atLeastOneDrop:
                #On ajoute la description au titre
                figResaBeforePeak.update_layout(    
                    title={
                    'text': 'Traitement du signal de {} "net" | {} '.format(channel,description)
                    })

                #On ajoute le boutton log/lin sur la figure
                figResaBeforePeak.update_layout(updatemenus=param['updatemenus'])
                
                figResaBeforePeak.show(renderer=metaParam.renderer, config={'editable': True})

        #-------------------------------------------
        #--------------Graphe "nuage"---------------
        #On trace un graphe avec les paramètres de la régression en fonction du groupe
        #on trie par lagTime pour éviter de se retrouver avec des Nan en première ligne, ce qui cause un bug d'affichage
        growingDrops[0].sort_values(by=['description'],inplace=True,kind='mergesort')

        fig = go.Figure(layout=go.Layout(
            autosize=False, 
            template='seaborn',
            font=metaParam.taillePolice,
            width=metaParam.W, height=metaParam.H,
            xaxis=dict(zeroline=False, title='Doubling Time', type='log'),
            yaxis=dict(zeroline=False, title='Time at threshold'),
            title={
            'text': 'Paramètres de la régression exponentielle | '+getSimplePath(pathOfCurrentEXP)}))
        
        #Une figure pour afficher les epsilon (paramEpsilon)
        figRes = go.Figure(layout=go.Layout(
            autosize=False, 
            font=metaParam.taillePolice,
            template='seaborn',
            width=metaParam.W, height=metaParam.H,
            xaxis=dict(zeroline=False, title='Drop ID'),
            yaxis=dict(zeroline=False, title='paramEpsilon'),
            title={
            'text': 'Epsilon de la régression exponentielle | '+getSimplePath(pathOfCurrentEXP)}))

        #on initialise un indice de couleur
        idColor=0

        for description in metaData['descriptionToWells']:
            queryString='description == @description & lagTime > 0'
            textOnMarker=growingDrops[0].query(queryString).index
            
            fig.add_trace(go.Scatter(        
                x=growingDrops[0].query(queryString)['tDiv'],
                y=growingDrops[0].query(queryString)['lagTime'],
                mode='markers',
                text=textOnMarker,
                hovertemplate=
                    'Doubling time: %{x}<br>'+
                    'Lag time: %{y}<br>'+
                    'Drop : %{text}', 
                marker=dict(color=metaParam.matColors[idColor]),
                name=description+" ("+str(len(growingDrops[0].query(queryString)))+" drops)"))

            figRes.add_trace(go.Scatter(        
                x=growingDrops[0].query(queryString).index,
                y=growingDrops[0].query(queryString)['paramEpsilon'],
                mode='markers',
                text=textOnMarker,
                hovertemplate=
                    'Drop : %{text}', 
                marker=dict(color=metaParam.matColors[idColor]),
                name=description+" ("+str(len(growingDrops[0].query(queryString)))+" drops)"))
                
            if idColor < len(metaParam.matColors):
                idColor+=1
        
        figRes.add_shape(
            # Ligne au delà de laquelle on ne peut pas trouver de lagTime
            dict(
                type="line",
                name='Seuil de regression',
                x0=0,
                y0=seuil,
                x1=netDF.droplet_id.max(),
                y1=seuil,
                line=dict(
                    color="black",
                    width=3)))

        fig.show(renderer=metaParam.renderer,config={'editable': True})
        figRes.show(renderer=metaParam.renderer,config={'editable': True})
        
        del fig, figRes, queryString, textOnMarker

        #Tracé des gouttes non-fittées ; cela sert de contrôle qualité même si on ne souhaite pas tracer et afficher les régressions
        idColor=0 #Il y aura une couleur par description
        failedDrops2=failedDrops.copy()
        figFailedDrops=go.Figure(layout=go.Layout(
            font=metaParam.taillePolice,
            template='seaborn', width=metaParam.W, height=metaParam.H,
            xaxis=dict(zeroline=False, title='time'), yaxis=dict(zeroline=False, title='fluo 2'),
            title={'text': 'Gouttes non-fittées | '+getSimplePath(pathOfCurrentEXP)}))
        
        for description in metaData['descriptionToWells']:
            for fDrop in failedDrops2:
                if metaData['wellToDescription'][metaData['dropletToWell'][str(fDrop)]] == description :
                    figFailedDrops.add_trace(go.Scatter(
                        x=(netDF.query("droplet_id == "+str(fDrop))['time'])/3600,
                        y=netDF.query("droplet_id == "+str(fDrop))[channel],
                        mode='lines',             
                        line=dict(color=metaParam.matColors[idColor]),
                        name=str(fDrop)))
            
            if idColor < len(metaParam.matColors):
                idColor+=1
        
        figFailedDrops.add_shape(
            # Ligne au delà de laquelle on ne peut pas trouver de lagTime
            dict(
                type="line",
                name='Seuil de regression',
                x0=0, y0=seuil,
                x1=param["maxTime"],y1=seuil,
                line=dict(color="black", width=3)))

        figFailedDrops.show(renderer=metaParam.renderer,config={'editable': True})        
        del figFailedDrops, failedDrops2

    # Export des résultats de la régression 
    if not path.exists('{}/analyseArthur/resultsOfRegression{}{}.csv'.format(pathOfCurrentEXP, channelName.capitalize(), getSimplePath(pathOfCurrentEXP))) or askOverwrite == False :
        #On cherche les gouttes qui poussent
        growingDrops[0].to_csv('{}/analyseArthur/resultsOfRegression{}{}.csv'.format(pathOfCurrentEXP, channelName.capitalize(), getSimplePath(pathOfCurrentEXP)))
    else:
        ouiOuNon=''
        while not ouiOuNon in ['y','n']:
            ouiOuNon=input('There already is a regression result. Do you want to overwrite it? (y/n)')
            if ouiOuNon=='y':
                growingDrops[0].to_csv('{}/analyseArthur/resultsOfRegression{}{}.csv'.format(pathOfCurrentEXP, channelName.capitalize(), getSimplePath(pathOfCurrentEXP)))

def regressionLineaire(xAxis, yAxis):
    k, pcov = curve_fit(linear_func, xAxis, yAxis)
    xm=xAxis.mean()
    ym=yAxis.mean()
    Rdeux = 1-((yAxis-xAxis*k[0])**2).sum()/((yAxis-ym)**2).sum()
    # Rdeux = (1-((xAxis*k-yAxis)**2).sum()/((xAxis-xAxis.mean())**2).sum()).round(3)
    # RdeuxExcel = ((xAxis-xm)*(yAxis-ym)).sum()/sqrt((((xAxis-xm)**2).sum())*(((yAxis-ym)**2).sum()))
    return k, Rdeux

def randomizeColor(iniTcolour, hueRange = 0.08, satRange = 0.08):
    """
    Renvoie une couleur légérement différente de la couleur en entrée. 
    Augmenter hueRange et satRange pour augmenter l'intensité de la modification
    """
    colour=co.Color(iniTcolour)
    originalHue=colour.get_hue()
    originalSat=colour.get_saturation()
    newHue=uniform(max(originalHue-hueRange,0), min(originalHue+hueRange,1))
    newSat=uniform(max(originalSat-satRange,0), min(originalSat+satRange,1))
    colour.set_hue(newHue)
    colour.set_saturation(newSat)
    return str(colour)

#%%
if __name__=='__main__': 
    print("Bienvenus sur l'environnnement de test des fonctions d'Arthur\n")
    arrayTest=["Un","Deux","Trois","Quatre"]

    print("Contrôle")
    print(replaceTemplateNameWithSoilName("Contrôle",*arrayTest))
    print("Sol 1 - 1_100 - sans particule")
    print(replaceTemplateNameWithSoilName("Sol 1 - 1_100 - sans particule",*arrayTest))
    print("Sol 4 - 1_1000")
    print(replaceTemplateNameWithSoilName("Sol 4 - 1_1000",*arrayTest))
    print("EGM - 1_4512")
    print(replaceTemplateNameWithSoilName("EGM - 1_4512",*arrayTest))

# %