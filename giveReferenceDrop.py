#%% 
#Ce script sert à indiquer une goutte de référence pour le calcul des net Dataframes
# Importation
import functionsArthur as fArt
from os import path, makedirs
import metaParametersSheet as metaParam 
import json
pathExperimentMD=''

#%%
#Cellule 1 : choix de l'expérience
# from experimentChoice import pathExperimentMD
pathExperimentMD=fArt.choixDossier(pathMilliDrop=metaParam.pathMilliDrop)

#%%
# Cellule 2

#1 : s'il n'y a aucune goutte de référence présente
newpath = pathExperimentMD+"/analyseArthur" #On crée un dossier dans l'expérience
if not path.exists(newpath):
    print("""Pas de dossier 'analyseArthur' trouvé. Nous en créons un""")
    makedirs(newpath)
    #On sait que si on en est là, il n'y a pas de fichier gouttes de référence

#2 : prompt 
if not path.exists(newpath+"/referenceDrops.json"):
    print('Pas de goutte de référence trouvée, veuillez en entrer une.')
    refDrop = input("Please enter a reference drop:\n")
    print('Drop '+str(refDrop)+' added successfully')
    
else:
    refDrop=json.load(open(newpath+"/referenceDrops.json"))
    print("Il existe déjà une goutte de référence pour cette expérience({})\nVoulez-vous la remplacer par une autre ? ".format(str(refDrop)))
    ouiOuNon=''
    while not ouiOuNon in ['y','n']:
        ouiOuNon=input('y/n ?')
    
    if ouiOuNon=='n':
        print('Aucune modification à faire, le script va fermer.')
    elif ouiOuNon=='y':
        refDrop = input("Please enter a new reference drop:\n")
        print('{} is the new reference drop.'.format(str(refDrop)))

fname = newpath+'/referenceDrops.json' #on écrit un nouveau fichier json
with open(fname, 'w') as outfile:
    json.dump(refDrop, outfile)

# %%


# %%
