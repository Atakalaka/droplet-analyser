#%%
#Cellule importation
pathExperimentMD=""
import json
from os import listdir

import numpy as np
import pandas as pd
import plotly.graph_objects as go

import functionsArthur as fArt
import metaParametersSheet as metaParam  # paramètres qui ne dépendent pas de l'expérience
import metaDonneesExperiences as metaD

# Ce script permet de recenser les minimum de fluo_1 pour chaque 
# expérience, en les triant par dossier (donc par gain et par 
# concentration en hydroxyapatite)

# %%
# Recensement des minimaux de fluo_1
minDFarray={'scatt' : {}, 'fluo1' : {}}
folders=[]
for rootFolder in metaParam.rootFolders:
    RFName=rootFolder.split('/')[-1]
    # On crée un dict dans lequel chaque rootFolder aura son DataFrame de min
    minDFarray['scatt'][RFName] = pd.DataFrame(columns=['EXP', 'positiveResa', 'droplet_id', 'minimum'])
    minDFarray['fluo1'][RFName] = pd.DataFrame(columns=['EXP', 'positiveResa', 'droplet_id', 'minimum'])

    folders = [rootFolder +'/'+ f for f in listdir(rootFolder)]

    for experimentPath in folders :
        experimentName=experimentPath.split('/')[-1]
        if not ("CONTAMINE" in experimentName or "ECHEC" in experimentName or "ALIGNEMENT" in experimentName or "FAIBLE SCATTERING" in experimentName) and '{' in experimentName:
            EXPShortName=fArt.getSimplePath(experimentName)
            param = json.load(open(experimentPath+"/analyseArthur/parameters.json"))

            # On charge les paramètres de la régression afin d'en extraire les gouttes positives. 
            # On s'en servira pour exclure les gouttes qui n'ont pas poussé
            # growingDrops = pd.DataFrame(pd.read_csv(experimentPath+"/analyseArthur/resultsOfRegression"+EXPShortName+".csv",
            #     sep=",", header=0,
            #     names=['drop','description'],
            #     index_col='drop',
            #     usecols=[0, 1]))
            
            growingDrops=json.load(open(experimentPath+'/analyseArthur/growingDrops.json'))
            growingDrops=[int(droplet) for droplet in growingDrops] #on convertit en entier

            for canal in ({'DF' : 'netDFPyra', 'canalName' : 'fluo_1_median', 'tMax': metaParam.tMaxFluo1*3600, 'output' : 'fluo1'},
            {'DF' : 'netDFScatt', 'canalName' : 'scattering_median', 'tMax' : metaParam.tMaxUniqueScatt*3600, 'output' : 'scatt'}):

                DFCanal=pd.DataFrame(pd.read_csv(experimentPath+"/analyseArthur/"+canal['DF']+".csv", sep=","))
                if DFCanal.time.max()>metaParam.tempsMinimal*3600: #On s'assure que l'expérience a duré plus que le temps minimal précisé dans metaParam
                    DFCanal=DFCanal[DFCanal.time < canal['tMax']]

                    metaData=fArt.importMetadata(experimentPath)
                    listDrops=fArt.dropsWell(metaD.defaultListOfDropsAndWells,experimentPath,metaData)
                    soilsNames=fArt.getSoilName(experimentName)

                    for dropArray in listDrops:
                        for drop in dropArray:
                            dropGroupe=metaData['wellToDescription'][metaData['dropletToWell'][str(drop)]]

                            # On ne garde que les gouttes qui correspondent à des puits ensemencés et hors expériences 
                            # exceptionnelles (par exemple "Sol n - 1_100", pas des Controle, pas les MES etc.)
                            if fArt.replaceTemplateNameWithSoilName(dropGroupe, *soilsNames) != "divers" :

                                if drop in growingDrops:
                                    queriedDF=DFCanal[DFCanal.droplet_id == drop] #on crée un dataframe pour aller chercher son min
                                    DFLine={
                                        'EXP' : EXPShortName, 
                                        'positiveResa' : True,
                                        'droplet_id' : drop,
                                        'minimum' : queriedDF[canal['canalName']].min()}
                                    minDFarray[canal['output']][RFName]=minDFarray[canal['output']][RFName].append(DFLine, ignore_index=True)
                                else: 
                                    queriedDF=DFCanal[DFCanal.droplet_id == drop]
                                    DFLine={
                                        'EXP' : EXPShortName, 
                                        'positiveResa' : False,
                                        'droplet_id' : drop,
                                        'minimum' : queriedDF[canal['canalName']].min()}
                                    minDFarray[canal['output']][RFName]=minDFarray[canal['output']][RFName].append(DFLine, ignore_index=True)
                else:
                    print(EXPShortName, canal['canalName'], 'trop courte', (DFCanal.time.max()/3600).round(1),"heures")
#%% Affichage
idCanal=0
for canalDF in (minDFarray['scatt'], minDFarray['fluo1']) :
    idColor=0

    #On crée un nom pour les titres
    if idCanal == 0:
        canalName="scattering"
    elif idCanal == 1:
        canalName='fluorescence de la Pyranine'
    
    for RFName in canalDF:
        xAxisNeg = canalDF[RFName][canalDF[RFName].positiveResa == False].minimum
        xAxisPos = canalDF[RFName][canalDF[RFName].positiveResa].minimum
        if canalName == 'fluorescence de la Pyranine':
            maximum=np.log10(canalDF[RFName].minimum.max())
            minimum=np.log10(canalDF[RFName].minimum.min())
            xAxisNeg, xAxisPos = np.log10(xAxisNeg), np.log10(xAxisPos)

        elif canalName == "scattering" :
            maximum=canalDF[RFName].minimum.max()
            minimum=canalDF[RFName].minimum.min()

        # binArray=np.logspace(start=np.log10(minimum), stop=np.log10(maximum))
        figHistog = go.Figure(layout=go.Layout(
            font=metaParam.taillePolice,
            autosize=False,
            barmode='stack',
            template='seaborn',
            width=metaParam.W, height=metaParam.H,
            title={'text': "Répartition des valeurs minimales de "+canalName+"<br>des gouttes - "+RFName}))

        figHistog.add_trace(go.Histogram(
            x=xAxisNeg,
            # histnorm='percent',
            xbins=dict(start=minimum, end=maximum, size=abs(maximum-minimum)/100),
            marker_color=metaParam.matColors[idColor],
            name='négatives',
            opacity=0.75))
        
        #On ajoute les gouttes positives d'une autre couleur. 
        figHistog.add_trace(go.Histogram(
            x=xAxisPos,
            # histnorm='percent',
            xbins=dict(start=minimum, end=maximum, size=abs(maximum-minimum)/100),
            marker_color=metaParam.matColors[idColor+6],
            name='positifs',
            opacity=0.75))

        idColor+=1

        figHistog.update_layout(
            xaxis_title_text='Minimum du signal de '+canalName, 
            yaxis_title_text='Nombre de gouttes',
            bargap=0.1, # gap between bars of adjacent location coordinates
        )

        figHistog.show(renderer=metaParam.renderer, config={'editable':True})
    idCanal+=1
# %% Export 
idCanal=0
exportDF=pd.DataFrame() #On crée un dataframe dans lequel on va tout stocker avant d'exporter
frames=[]

scattDF=pd.concat(minDFarray['scatt'])
scattDF.reset_index(inplace=True)
scattDF.drop(['level_1', 'positiveResa'], axis=1, inplace=True)
scattDF.rename(columns={"level_0": "lot"}, inplace=True)

fluo_1DF=pd.concat(minDFarray['fluo1'])
fluo_1DF.reset_index(inplace=True)
fluo_1DF.drop(['level_0','level_1', 'positiveResa'], axis=1, inplace=True)

exportDF=pd.merge(fluo_1DF,scattDF, on=['EXP', 'droplet_id'], suffixes=('Fluo_1','Scatt'))

exportDF.to_csv(metaParam.pathAnalyseTemporelle+'/DFMinima.csv')
# %%
