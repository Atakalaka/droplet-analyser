#%%
# décalage des numéros de run pour que le soft de Guilhem marche correctement avec les données des machines MilliDrop (bug des runs inversés) : 
import functionsArthur as fArt
from os import rename, listdir
import metaParametersSheet as metaParam  # paramètres qui ne dépendent pas de l'expérience

pathEXPToModify=fArt.choixDossier(pathMilliDrop=metaParam.pathMilliDrop)
arrayDir=listdir(pathEXPToModify)

maxProc=0
maxRaw=0
for file in arrayDir:
    #On trouve le numéor du dernier run
    if file[0:4] == 'Proc':
        maxProc=max(int(file[4:8]),maxProc)
    elif file [0:3] == 'Raw':
        maxRaw=max(int(file[3:7]),maxRaw)

i=maxProc
while i>=0:
    try:
        rename(
            pathEXPToModify+str('/Proc{}.csv').format(str(i).zfill(4)),
            pathEXPToModify+str('/Proc{}.csv').format(str(i+1).zfill(4))
            )
        i+=-1
    except:
        print("pas de Proc"+str(i).zfill(4))
        i+=-1

i=maxRaw
while i>=0:
    try: 
        rename(
            pathEXPToModify+str('/Raw{}.raw').format(str(i).zfill(4)),
            pathEXPToModify+str('/Raw{}.raw').format(str(i+1).zfill(4))
            )
        i+=-1
    except:
        print("pas de raw"+str(i).zfill(4))
        i+=-1
# %%
