#%%
pathExperimentMD=""
import json
from os import listdir
from re import findall
from sys import exc_info
from datetime import date

import pandas as pd

import functionsArthur as fArt
import metaParametersSheet as metaParam  # paramètres qui ne dépendent pas de l'expérience

def replaceDescriptionNames(sol1Name, sol2Name):

    def innerFunc(description): 
        listOfDescriptions=[
        ["Sol 1 - 1_100","Sol 1_100","sol 1_100","sol 1_100 - pikov","Souche 1 - 1_100", "Sol - 1_100"],
        ["Sol 1 - 1_1000","Sol 1_1000","sol 1_1000","Souche 1 - 1_1000", "Sol - 1_1000"],
        ["Sol 1 - 1_10000","Sol 1_10000","sol 1_10000","Souche 1 - 1_10000"],
        ["Sol 2 - 1_100", "Souche 2 - 1_100"],
        ["Sol 2 - 1_1000","Souche 2 - 1_1000"],
        ["Sol 2 - 1_10000", "Souche 2 - 1_10000"],
        ["EGM 1_50"],
        ["EGM 1_500"],
        ["Souche 1 - 1_100000"],
        ["Souche 2 - 1_100000"],
        ["Sol - 1_200"]]

        if description in listOfDescriptions[0]:
            return str("{} - 1_100").format(sol1Name)
        elif description in listOfDescriptions[1]:
            return str("{} - 1_1000").format(sol1Name)
        elif description in listOfDescriptions[2]:
            return str("{} - 1_10000").format(sol1Name)
        elif description in listOfDescriptions[3]:
            return str("{} - 1_100").format(sol2Name)
        elif description in listOfDescriptions[4]:
            return str("{} - 1_1000").format(sol2Name)
        elif description in listOfDescriptions[5]:
            return str("{} - 1_10000").format(sol2Name)
        elif description in listOfDescriptions[6]:
            return str("{} - 1_50").format(sol1Name)
        elif description in listOfDescriptions[7]:
            return str("{} - 1_500").format(sol1Name)
        elif description in listOfDescriptions[8]:
            return str("{} - 1_100000").format(sol1Name)
        elif description in listOfDescriptions[9]:
            return str("{} - 1_100000").format(sol2Name)
        elif description in listOfDescriptions[10]:
            return str("{} - 1_200").format(sol1Name)
        else:
            # return description
            return str("divers")
            # Problème quand un sol et une souche ensemble. Le plus simple serait de renommer la souche "souche 2"
            # Si elle est en deuxième position, et souche 1 sinon

    return innerFunc

searchStringSoil="\{.{1,19}\}" #entre 1 et 19 caractères entourés de deux accolades

#On créé un datafram destiné à recevoir les résultats de régression de toutes les expériences déjà analysées jusque là:
DataFrameCentralReg=pd.DataFrame(columns=['Experience', 'goutte','dilution','Sol','SolDilution', 'tempsDeDivision', 'Epsilon', 'tempsAuSeuil'])

#Chemin où écrire le fichier bilan des comptages de gouttes
destinationFolder=metaParam.pathMilliDrop+'/recapitulatifRegressions/comptage'

#Une liste du nombre de goutte pour toutes les expériences trouvées
dictBilan={}

#Là où aller chercher les dossiers expérience 
rootFolders=metaParam.rootFolders

#-------- On liste les expériences
folders=[]
for rootFolder in rootFolders:
    folders = folders + [f for f in listdir(rootFolder)]
#--------

for nomExperience in folders:
    #Si l'expèrience contient 'CONTAMINE', on ne la compte pas
    if not ("CONTAMINE" in nomExperience or "ECHEC" in nomExperience or "ALIGNEMENT" in nomExperience):
        #On crée un array avec tous les noms entre accolades (1 ou 2 en général)
        arrayNames=findall(searchStringSoil,nomExperience)
        #--------- On enlève les accolades
        length=len(arrayNames)
        if length>0:
            i=0
            while i<length: 
                arrayNames[i]=arrayNames[i][1:-1]
                i+=1
        #--------

            #On va chercher les résultats de la régression. Si on en trouve, on les ajoute à un fichier central. 
            for rootFolder in rootFolders:
                if nomExperience in listdir(rootFolder):
                    stringPathAnalArthur=str("{0}/{1}/analyseArthur").format(rootFolder,nomExperience)
                    break

            try:    
                filesAA = [f for f in listdir(stringPathAnalArthur)]

                #On cherche un fichier contenant la chaîne "resultsOfRegression" ; les résultats de la régression pour cette expérience
                for file in filesAA: 
                    if file[0:23] == "resultsOfRegressionResa":
                        stringPathResultsReg=stringPathAnalArthur+'/'+file

                #Cette fonction sert à remplacer sol 1 et sol 2 dans les noms d'expérience par le nom du sol entre crochets contenu dans le nom de l'expérience
                replaceWithSoilName=replaceDescriptionNames(sol1Name=arrayNames[0], sol2Name=arrayNames[1] if length > 1 else "someSoil")

                DFResultsOfRegression=pd.DataFrame(pd.read_csv(
                    stringPathResultsReg,
                    decimal = '.',
                    converters={'description':replaceWithSoilName}))
                
                DFResultsOfRegression.rename(inplace = True, columns={
                    'Unnamed: 0':'goutte',
                    'tDiv' : 'tempsDeDivision',
                    'paramEpsilon' : 'Epsilon',
                    'lagTime' : 'tempsAuSeuil',
                    'description' : 'SolDilution'})

                #------ On crée une colonne avec le nom de l'expérience, destinée à être ajouté au DF
                emptyColumn=['']*len(DFResultsOfRegression)
                columnExperimentName=[nomExperience[3:11]]*len(DFResultsOfRegression)
                DFResultsOfRegression["Experience"]=columnExperimentName
                DFResultsOfRegression["dilution"]=emptyColumn
                DFResultsOfRegression["Sol"]=emptyColumn
                #------

                DFResultsOfRegression=DFResultsOfRegression.drop(labels=['lastRun', 'pseudoLag', 'maximum'], axis=1)

                DataFrameCentralReg=DataFrameCentralReg.append(DFResultsOfRegression, ignore_index=True, sort=False)
            
                #On alimente un fichier qui donne le nombre total de gouttes par condition, à partir du mataData de l'exp traitée : 
                try:
                    metaData=fArt.importMetadata(str("{0}/{1}").format(rootFolder,nomExperience))

                    #Comptage du nombre de gouttes par description
                    listDropletsNumbers={}
                    for description in metaData['descriptionToWells']:
                        nmDrops=0
                        for well in metaData['descriptionToWells'][description]:
                            nmDrops += len(metaData['wellToDroplets'][well])
                        listDropletsNumbers[replaceWithSoilName(description)]=nmDrops
                    
                    dictBilan[nomExperience[3:11]]=listDropletsNumbers
                
                except : 
                    print('Erreur de comptage avec le dossier '+nomExperience)
                    print("Info sur l'erreur :")
                    print(exc_info()[0])
                    print(exc_info()[1])
                    print(exc_info()[2])

            except:
                print("\nErreur pour " +nomExperience)
                print("Info sur l'erreur :")
                print(exc_info()[0])
                print(exc_info()[1])
                print(exc_info()[2])
                pass
        else:
            print("Warning : Not a single soil name found in ",nomExperience)
# On décompose SolDilution dans deux colonnes et on enlève les lignes de contrôle : 
# Attention : s'il y a plus de deux "-", ça ne marchera pas
rowsToDrop=[]
for i, row in DataFrameCentralReg.iterrows():
    splittedLine=row['SolDilution'].split(' - ')
    if len(splittedLine)==2:
        DataFrameCentralReg.at[i,'dilution'] = splittedLine[1]
        DataFrameCentralReg.at[i,'Sol'] = splittedLine[0]
    elif len(splittedLine)==3:
        DataFrameCentralReg.at[i,'dilution'] = splittedLine[2]
        DataFrameCentralReg.at[i,'Sol'] = "{} - {}".format(splittedLine[0], splittedLine[1])
    else: 
        #S'il n'y a pas de " - " dans le nom SolDilution, on efface la ligne 
        rowsToDrop.append(i)

DataFrameCentralReg.drop(labels=rowsToDrop, inplace=True)
DataFrameCentralReg.reset_index(inplace=True, drop=True)

del rowsToDrop

#On exporte en CSV : 
DataFrameCentralReg.to_csv('{}/recapitulatifRegressions/mainregression{}.csv'.format(metaParam.pathMilliDrop, date.today().isoformat()))

#On exporte le bilan du comptage des nombres de gouttes dans un JSON
fname = str('{}/dropsPerCondition.json').format(destinationFolder) #on écrit un fichier json
with open(fname, 'w') as outfile:
    json.dump(dictBilan, outfile, indent=2)

# %%
