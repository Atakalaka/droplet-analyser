dictOfSamplesColors = {'TR 1530': '#ff0000', 'EGM 1530': '#ffc000',
                       'EGM 1530 2': '#92d050', 'TS 1530': '#00b0f0',
                       'JF 1530': '#7030a0', 'CV 010': '#ff9696',
                       'EGM 1530 3': '#ffe389', 'PS 030919': '#c7e686',
                       'PS 251119': '#9696ff', 'TS 130419': '#b17ed8',
                       'TS 291019': '#0f0f0f', 'PS 181019': '#6e6e6e',
                       'TS 230419': '#c8c8c8', 'PS 160819': '#640000',
                       'TS 020120': '#969600', 'PS 270919': '#006400',
                       'PS 151119': '#000064', 'TB 030920': '#960096',
                       'IA 300920': '#4987EB', 'PS 191020': '#F09022',
                       'PS 271020' : '#F53329', "VAB 3309CIC" : "#32DAE3",
                       "TS 080920" : "#B04A04",
                       'E coli': "#009696", "P agglomerans": "#960000",
                       "S cerevisiae": "#c8c800", "B subtilis": "#59a14f", }

souches = ('E coli', 'B subtilis', 'P agglomerans', 'S cerevisiae', "P aeruginosa")

dictOfEXPFolders = {
    'Pikovskaya_Pyra1prc_Resa5prm_Gain32': {
        # 'seuilFluo1': 0.2,
        # 'seuilFluo1': 10**(-0.5),
        # 'seuilFluo1': 10**(-1.3), #seuil exigeant
        'seuilFluo1': 10**(-0.99), #pH 5,8
        'seuilScatt': -0.19, #seuil haut
        # 'seuilScatt': -0.5, #seuil bas
        'concentration': 1
    },
    'Pikovskaya_Pyra1prc_Resa5prm_Gain32_PikovPauvre': {
        'seuilFluo1': 0.2,
        'seuilScatt': -0.06,
        'concentration': 1
    },
    'Pikovskaya5gl_Pyra1prc_Resa5prm': {
        'seuilFluo1': 0.05,
        'seuilScatt': -0.8,
        'concentration': 5
    },
    'Pikovskaya5gl_Pyra1prc_Resa5prmDindon': {
        # 'seuilFluo1': 10**0.25,
        # 'seuilFluo1': 10**(-1.19), #seuil exigeant
        'seuilFluo1': 10**(-0.40), #pH 5,8
        'seuilScatt': -0.4, #seuil haut
        # 'seuilScatt': -2, #seuil bas
        'concentration': 5
    },
    'Pikovskaya5gl_Pyra1prc_Resa5prmIguane': {
        # 'seuilFluo1': 10**(-1.19), #seuil exigeant
        'seuilFluo1': 10**(-0.40), #pH 5,8
        'seuilScatt': -0.4,
        'concentration': 5
    },
    'Pikovskaya5gl_Pyra1prc_Resa5prmAlbatros': {
        # 'seuilFluo1': 0.5,
        # 'seuilFluo1': 10**(-1.19), #seuil exigeant
        'seuilFluo1': 10**(-0.40), #pH 5,8
        'seuilScatt': -0.2, #seuil haut
        # 'seuilScatt': -2, #seuil bas
        'concentration': 5
    },
    'PikovskayaPyra1prc_Resa5prmKoala': {
        # 'seuilFluo1': 0.5,
        # 'seuilFluo1': 10**(-1.19), #seuil exigeant
        'seuilFluo1': 10**(-0.40), #pH 5,8
        'seuilScatt': 0.02, #seuil haut
        # 'seuilScatt': -2, #seuil bas
        'concentration': 5
    },
    "Difco1_10":{
        'seuilFluo1': 0.04, #pH 5,8
        'seuilScatt': 0.02, #seuil haut
        'concentration': 0
    }
}

dictOfExpData = {
    'EXP20200828': 1,
    'EXP20200522': 1,
    'EXP20200302': 1,
    'EXP20200304': 1,
    'EXP20200310': 1,
    'EXP20200513': 1,
    'EXP20200515': 1,
    'EXP20200527': 1,
    'EXP20200605': 1,
    'EXP20200610': 1,
    'EXP20200612': 1,
    'EXP20200615': 1,
    'EXP20200618': 1,
    'EXP20200624': 1,
    'EXP20200701': 1,
    'EXP20200702': 1,
    'EXP20200706': 1,
    'EXP20200709': 1,
    'EXP20200716': 1,
    'EXP20200804': 1,
    'EXP20200911': 1,
    'EXP20200918': 1,
    'EXP20200805': 1,
    'EXP20200814': 5,
    'EXP20200819': 5,
    'EXP20200826': 5,
    'EXP20200907': 5,
    'EXP20200921': 5,
    'EXP20200925': 5,
    'EXP20201002': 5,
    'EXP20201005': 5,
    'EXP20201009': 5,
    'EXP20201020': 5,
    'EXP20201116': 5,
    'EXP20201125': 5,
    'EXP20201130': 5,
    'EXP20201202': 5,
    'EXP20201204': 5,
    'EXP20201211': 5,
    'EXP20201215': 5,
    "EXP20210112": 5,
    "EXP20210114": 5,
    "EXP20210118": 5,
    "EXP20210125": 5,
    "EXP20210129": 5,
    "EXP20210203": 0,
    "EXP20210205": 0,
    "EXP20210208": 0,
    "EXP20210216": 0,
    "EXP20210226": 5,
    "EXP20210303": 5, 
    "EXP20210525": 0,
    "EXP20211129": 0,
    "EXP20211122": 0,
    "EXP20211124": 0}

defaultListOfDropsAndWells = [
        ["Sol 1 - 1_100","Sol 1_100","sol 1_100","sol 1_100 - pikov","Souche 1 - 1_100", "Sol - 1_100"],
        ["Sol 1 - 1_1000","Sol 1_1000","sol 1_1000","Souche 1 - 1_1000"],
        ["Sol 1 - 1_10000","Sol 1_10000","sol 1_10000","Souche 1 - 1_10000"],
        ["Sol 2 - 1_100", "Souche 2 - 1_100"],
        ["Sol 2 - 1_1000","Souche 2 - 1_1000"],
        ["Sol 2 - 1_10000", "Souche 2 - 1_10000"],
        ["Sol - 2_100"]]
