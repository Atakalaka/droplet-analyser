import sys
#---------Localisation système : là où sont stockées les dossiers contenant les expériences MilliDrop
if sys.platform == "win32": #sur le Windows du LCMD
    # pathMilliDrop="C:/Users/Arthur/ownCloud/donnéesTraitées01022021"
    pathMilliDrop="C:/Users/Arthur/ownCloud/donneesGenoscreen"
else: #Sur mon mac
    pathMilliDrop="/Users/arthurgoldstein/ownCloud/donneesGenoscreen"

rootFolders=[
    # pathMilliDrop+"/PikovskayaPyra1prc_Resa5prmKoala",
    # pathMilliDrop+"/Pikovskaya_Pyra1prc_Resa5prm_Gain32",
    # pathMilliDrop+"/Pikovskaya5gl_Pyra1prc_Resa5prmAlbatros",
    # pathMilliDrop+"/Pikovskaya5gl_Pyra1prc_Resa5prmDindon",
    # pathMilliDrop+"/Pikovskaya5gl_Pyra1prc_Resa5prmIguane",
    # pathMilliDrop+"/LB_Pyra1prc_resa5prm14Avril",
    pathMilliDrop+"/Difco1_10"
    ]

pathAnalyseTemporelle=pathMilliDrop+"/analyseTemporelle"

# ----------------------

remove10000=True

activatedChannels='2'

drawMainPlot=False

fluo1VsScattOn=False

displayNetFluo2Signal=True

displayNetScatteringSignal=True

launchFitting=True

stdAnalysisOn=False

displayNetFluo1Signal=False

#Affichage du bouton "Linear scale / Log scale sur la figure mainPlot"
displayLinLogButton=False

# petriVSMD :
displayMPC=True

# Temps maximal au delà duquel on arrête d'analyser la dissolution de particules, la résa et la fluo 1
tMaxUniqueScatt = 48
tMaxUniqueResa = 48
tMaxFluo1 = 48
tempsMinimal = 40 #Durée d'incubation minimale pour que l'expérience soit dans l'analyse MPC 

# Idem pour les Petri (en jours)
tMinimalPetri=10

matColors=["#D10000", "#D1A915", "#15D14A", "#0A1CD1", "#9828D0", 
            "#1DC6D1","#A6D113", "#DC3264", "#3C1AEB","#969600",
            "#958696", "#564D57", "#E0CAE3", "#7B6F7D","#D1793D",
            "#F25B2C", "#C52CF2", "#E8952A", "#640000","#8E16F5", 
            "#006400", "#000064", "#960096", "#009696", "#960000",
            "#c8c800","#59a14f", "#49fcab", "#106247", "#8d5bE4",
            "#9CFC1B","#E77903","#C6AF8D","#DE401F","#0DF764",
            "#14395B","#B23135","#90791B","#E329BA","#CDDCB4",
            "#D7699E","#7ADC4B","#7ED478","#27B0BB","#212AA5",
            "#070757","#860391","#E349C3","#F61A2F","#DB9F13", 
            "#6A4EF2","#518DFC","#55D2E6","#51FCB6", "#57F55B",
            "#9A889E","#381A24","#A6AA68","#D19FB0", "#D1CD9F",
            "#F2960C","#FC590D","#E61F17","#FC0DDC"]

taillePolice=dict(size=24)
#Pour illustration pleine page sous Word : 24

#paramètres pour la taille des graphes :
H = 800
W = 1500

# H = 900
# W = 1100

#Pour illustration pleine page sous Word : 900 x 1700 , 900 x 1100 pour demi-page

renderer="browser" #mettre browser si on veut afficher sur le browser. plotly_mimetype sinon
# Voir https://plotly.com/python/renderers/