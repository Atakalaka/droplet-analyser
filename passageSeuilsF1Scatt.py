#%%
#Cellule importation
from importlib import reload
from os import listdir

import pandas as pd
import plotly.graph_objects as go

import functionsArthur as fArt #fonctions personnelles
import metaParametersSheet as metaParam  # paramètres qui ne dépendent pas de l'expérience
import metaDonneesExperiences as metaD #informations sur mes expériences MilliDrop

#%%
metaD, metaParam=reload(metaD), reload(metaParam)
deltaTime=1

numberOfRunsF1=3

#On liste les expériences
folders=[]
for rootFolder in metaParam.rootFolders:
    folders = folders + [rootFolder +'/'+ f for f in listdir(rootFolder)]
DFpositifsF1=pd.DataFrame(columns=['EXP','dropNumber', 'timeF1'])
DFpositifsScatt=pd.DataFrame(columns=['EXP','dropNumber','timeScatt'])

for experimentPath in folders :
    #on vérifie que l'expérience contient au moins un crochet et qu'elle n'est pas dans la liste des expériences à problème
    experimentName=experimentPath.split('/')[-1] #juste la dernière partie du chemin
    shortName=fArt.getSimplePath(experimentName)
    if not ("CONTAMINE" in experimentName or "ECHEC" in experimentName or "ALIGNEMENT" in experimentName or "FAIBLE SCATTERING" in experimentName) and '{' in experimentName:
        seuilFluo1=metaD.dictOfEXPFolders[experimentPath.split('/')[-2]]['seuilFluo1']
        seuilScatt = metaD.dictOfEXPFolders[experimentPath.split('/')[-2]]['seuilScatt']

        DFF1=pd.DataFrame(pd.read_csv(experimentPath+'/analyseArthur/netDFPyra.csv', sep=","))
        DFF1=DFF1[DFF1.time < (metaParam.tMaxUniqueScatt-deltaTime)*3600]
        DFScatt=pd.DataFrame(pd.read_csv(experimentPath+'/analyseArthur/netDFScatt.csv', sep=","))

        if DFScatt.time.max()>(metaParam.tMaxUniqueScatt-0.8)*3600:
            DFScatt=DFScatt[DFScatt.time < metaParam.tMaxUniqueScatt*3600] #On enlève les valeurs avant tMaxuniqueScatt

            arraynames=fArt.getSoilName(experimentPath) #Une liste avec les noms de sol
            listOfDropsAndWells=metaD.defaultListOfDropsAndWells

            if arraynames[0] in metaD.souches: #si le premier nom est une souche
                print(arraynames[0], 'retiré')
                listOfDropsAndWells=listOfDropsAndWells[3:] #On enlève les trois premières catégories 
            if len(arraynames)>1:
                if arraynames[1] in metaD.souches:
                    listOfDropsAndWells=listOfDropsAndWells[:-3]
                    print(arraynames[1], 'retiré')

            metaData=fArt.importMetadata(experimentPath)
            #On sélectionne les gouttes qui font partie des 'sol N - 1_M' et pas des contrôles. 
            listDrops=fArt.dropsWell(listOfDropsAndWells,experimentPath,metaData)

            for dropArray in listDrops:
                for drop in dropArray:
                    # I : Partie 'gouttes qui font baisser le scat parmi celles qui font baisser la fluo'
                    DFUnderThreshF1=DFF1[(DFF1.droplet_id == drop) & (DFF1.fluo_1_median < seuilFluo1)]
                    DFUnderThreshF1.sort_values(by=['time'],inplace=True)
                    # timeThreshF1=DFF1[(DFF1.droplet_id == drop) & (DFF1.fluo_1_median < seuilFluo1)]['time'].min()
                    #Si plus de 'numberOfRunsF1' runs sont positifs : 
                    if len(DFUnderThreshF1) > numberOfRunsF1:
                    # if not isnan(timeThreshF1):
                        timeThreshF1=DFUnderThreshF1.iloc[numberOfRunsF1].time
                        dropLineI={
                        'EXP':shortName,
                        # 'dropID':"{} - {}".format(fArt.getSimplePath(experimentName), str(drop)),
                        'dropNumber':str(drop),
                        'timeF1':timeThreshF1,
                        # 'timeScatt':float('nan')
                        }
                        #On crée un dataframe qui contient les données entre timeThreshF1 - 1 heure et timeThreshF1 + deltaTime heures
                        # DFScattProx=DFScatt[(DFScatt.droplet_id == drop) * (DFScatt.scattering_median < seuilScatt)]

                        # #On le trie par temps croissants :
                        # DFScattProx.sort_values(by=['time'], ignore_index=True ,inplace=True)
                        # if len(DFScattProx)>3:
                        #     timeThreshScatt=DFScattProx.iloc[3].time #on prend le temps du quatrième run sous le seuil de scatt
                        #     dropLineI['timeScatt']=timeThreshScatt

                        DFpositifsF1=DFpositifsF1.append(dropLineI, ignore_index=True)
                        del dropLineI
                    
                    # II : Partie 'gouttes qui font baisser la fluo parmi celles qui font baisser le scatt'
                    DFUnderThresh=DFScatt[(DFScatt.droplet_id == drop) * (DFScatt.scattering_median < seuilScatt)]
                    DFUnderThresh.sort_values(by=['time'], inplace=True)
                    if len(DFUnderThresh)>3:
                        timeThreshScatt=DFUnderThresh.iloc[3].time #on prend le temps du quatrième run sous le seuil de scatt
                        dropLineII={
                            'EXP':shortName,
                            # 'dropID':"{} - {}".format(fArt.getSimplePath(experimentName), str(drop)),
                            'dropNumber':str(drop),
                            # 'timeF1':float('nan'),
                            'timeScatt':timeThreshScatt}

                        # timeThreshF1=DFF1[(DFF1.droplet_id == drop) & (DFF1.fluo_1_median < seuilFluo1)]['time'].min()
                        # if not isnan(timeThreshF1):
                        #     dropLineII['timeF1']=timeThreshF1
                        
                        DFpositifsScatt=DFpositifsScatt.append(dropLineII, ignore_index=True)
                        del dropLineII
        else:
            print(fArt.getSimplePath(experimentName), 'trop courte -', (DFScatt.time.max()/3600).round(2), 'heures')

DFMerged=pd.merge(DFpositifsScatt, DFpositifsF1, on=['EXP', 'dropNumber'], how='outer')

print('\nGouttes positives au scattering : ', DFMerged.timeScatt.count())
print('Gouttes positives à la pyranine : ', DFMerged.timeF1.count())
print('Fraction des gouttes positives au scattering parmi celles positives à la pyranine : ', 
    (DFMerged[DFMerged.timeF1.notna()].timeScatt.count()/len(DFMerged[DFMerged.timeF1.notna()])).round(3))
print('Fraction des gouttes positives à la pyranine parmi celles positives au scattering : ', (DFMerged[DFMerged.timeScatt.notna()].timeF1.count()/len(DFMerged[DFMerged.timeScatt.notna()])).round(3))

#%% display
figHistogI = go.Figure(layout=go.Layout(
    font=metaParam.taillePolice,
    autosize=False,
    template='seaborn',
    xaxis=dict(zeroline=False, dtick=1, title="""Durée en heures"""),
    yaxis=dict(title="""Fraction des gouttes (%)"""),
    width=metaParam.W, height=metaParam.H,
    title={'text': "Temps entre le passage du seuil pyranine et scattering,<br>pour les gouttes positives aux deux canaux"}))

figHistogII=go.Figure(figHistogI)

axeEcartsTempsI=(DFMerged.timeScatt-DFMerged.timeF1)/3600
minimum=axeEcartsTempsI.min()
maximum=axeEcartsTempsI.max()

idColor=2
figHistogI.add_trace(go.Histogram(
    x=axeEcartsTempsI,
    histnorm='percent',
    xbins=dict(start=minimum, end=maximum, size=abs(maximum-minimum)/100),
    marker_color=metaParam.matColors[idColor],
    opacity=0.75))

figHistogI.show(renderer=metaParam.renderer, config={'editable':True})

# axeEcartsTempsII=(DFpositifsScatt.timeScatt-DFpositifsScatt.timeF1)/3600
# minimum=axeEcartsTempsII.min()
# maximum=axeEcartsTempsII.max()

# figHistogII.update_layout(title={'text': "Temps entre le passage du seuil pyranine et scattering,<br>pour les gouttes positives au scattering"})
# idColor=3
# figHistogII.add_trace(go.Histogram(
#     x=axeEcartsTempsII,
#     histnorm='percent',
#     xbins=dict(start=minimum, end=maximum, size=abs(maximum-minimum)/100),
#     marker_color=metaParam.matColors[idColor],
#     opacity=0.75))
# figHistogII.show(renderer=metaParam.renderer)

# %%
#Affichage des gouttes qu ne sont positives que sur un des deux canaux
positifsF1Seul=DFMerged[DFMerged.timeScatt.isna()]
positifsScattSeul=DFMerged[DFMerged.timeF1.isna()]

figF1Seul=go.Figure(layout=go.Layout(
    font=metaParam.taillePolice,
    autosize=False,
    template='seaborn',
    xaxis=dict(zeroline=False, range = [0.001, 5],title="""Fluo 1""", type='log'),
    yaxis=dict(title="""Scattering net""", range = [-2, 1]),
    width=metaParam.W, height=metaParam.H,
    title={'text': "Gouttes Fluo 1 seul"}))

figScattSeul=go.Figure(figF1Seul)
figScattSeul.update_layout(title={'text': "Gouttes Scattering seul"})

idColor=0
for experimentPath in folders :
    shortName=fArt.getSimplePath(experimentPath)
    if shortName in positifsF1Seul.EXP.unique() or shortName in positifsScattSeul.EXP.unique():
        netFluo_1DF=pd.DataFrame(pd.read_csv(experimentPath+"/analyseArthur/DFFluo1.csv", sep=","))
        netFluo_1DF=netFluo_1DF[netFluo_1DF.time<metaParam.tMaxUniqueScatt*3600]
        netScattDF=pd.DataFrame(pd.read_csv(experimentPath+"/analyseArthur/netDFScatt.csv", sep=","))
        netScattDF=netScattDF[netScattDF.time<metaParam.tMaxUniqueScatt*3600]

        #On sélectionne les lignes qui correspondent à l'expérience
        gouttesF1seul=positifsF1Seul[positifsF1Seul.EXP == shortName].dropNumber.unique()
        gouttesScattSeul=positifsScattSeul[positifsScattSeul.EXP == shortName].dropNumber.unique()

        for drop in gouttesF1seul:
            xAxis=netFluo_1DF[netFluo_1DF.droplet_id == int(drop)]['fluo_1_median']
            # xAxis=(netFluo_1DF[netFluo_1DF.droplet_id == int(drop)]['time']/3600).round(2)
            yAxis=netScattDF[netScattDF.droplet_id == int(drop)]['scattering_median']

            figF1Seul.add_trace(go.Scatter(
                x=xAxis, y=yAxis, 
                hovertemplate='Time: %{x}<br>Value: %{y}<br>Drop: '+str(drop)+' '+shortName,                
                # legendgroup=str(dropArray),
                line=dict(color=fArt.randomizeColor(metaParam.matColors[idColor])),
                name=drop+' '+shortName))

        for drop in gouttesScattSeul:
            xAxis=netFluo_1DF[netFluo_1DF.droplet_id == int(drop)]['fluo_1_median']
            yAxis=netScattDF[netScattDF.droplet_id == int(drop)]['scattering_median']

            figScattSeul.add_trace(go.Scatter(
                x=xAxis, y=yAxis, 
                hovertemplate='Time: %{x}<br>Value: %{y}<br>Drop: '+str(drop)+' '+shortName,                
                # legendgroup=str(dropArray),
                line=dict(color=metaParam.matColors[idColor]),
                name=drop+' '+shortName))
        idColor+=1

figF1Seul.show(renderer=metaParam.renderer, config={'editable':True})
figScattSeul.show(renderer=metaParam.renderer, config={'editable':True})
# %%
