#%%
#Cellule importation
from importlib import reload
from plotly.subplots import make_subplots
from random import triangular

import pandas as pd
import plotly.graph_objects as go
from numpy import array, linspace, log, log10, exp, sqrt
from scipy.optimize import curve_fit
from scipy.stats import gmean

import functionsArthur as fArt
import metaParametersSheet as metaParam  # paramètres qui ne dépendent pas de l'expérience
import metaDonneesExperiences as metaD

def linear_func(x,a):
    return a*x

matColors=metaParam.matColors

metaParam=reload(metaParam)

DFExcel=pd.DataFrame(pd.read_csv(
    "/Users/arthurgoldstein/OneDrive/LCMD/PetriVSMD.csv",
    decimal = ',',
    sep=";"))

DFSoils=DFExcel[(DFExcel.Souche=="FAUX") & (DFExcel.CellulesCultivablesParMl_gouttes.notna()) & (DFExcel.petriColoniesParMl.notna()) & (DFExcel.petriColoniesParMl > 20) & (DFExcel.CellulesCultivablesParMl_gouttes != 0)].reset_index(drop=True).copy()

# %% -----------------------------------------------

DFMPC=pd.DataFrame(pd.read_csv(str('{}/MPCFluo_2.csv').format(metaParam.pathAnalyseTemporelle))).query('rarity > 0.05')
DFMPCScatt=pd.DataFrame(pd.read_csv(str('{}/MPCScattering.csv').format(metaParam.pathAnalyseTemporelle))).query('rarity > 0.05')
sortedSamplesNames=DFMPC.Sample.unique()
sortedSamplesNames.sort()

# %% --------------------------------------------------------
# -------------------- Petri VS MPC Python ------------------
# -----------------------------------------------------------

def invertDate(date):
    d1, d2, d3 = date[0:2], date[2:4], date[4:6]
    return d3+d2+d1

#On importe les DataFrames Uniques (c'est-à-dire dans lesquels chaque dilution est traitée indépendamMent): 
DFMPCUniqueFluo_2=pd.DataFrame(pd.read_csv(str('{}/MPCFluo_2Uniques.csv').format(metaParam.pathAnalyseTemporelle)))
DFMPCUniqueScatt=pd.DataFrame(pd.read_csv(str('{}/MPCScatteringUniques.csv').format(metaParam.pathAnalyseTemporelle)))
DFMPCUniqueFluo_1=pd.DataFrame(pd.read_csv(str('{}/MPCFluo_1Uniques.csv').format(metaParam.pathAnalyseTemporelle)))

#On crée deux DataFrames à partir du fichier excel, auxquels on retire les dilutions à 1/10 000 : 
DFPetriUniques=DFExcel[DFExcel.dilution != '1/10 000'][['IDExperience','dilution','sol','petriColoniesParMl','valeurConf95BasPetriCFU','valeurConf95HautPetriCFU']]
DFPetriUniquesHalos=DFExcel[DFExcel.dilution != '1/10 000'][['IDExperience','dilution','sol','petriHalosParMl','valeurConf95BasPetriHalos','valeurConf95HautPetriHalos']]

DFPetriUniques=DFPetriUniques.rename(columns={"IDExperience": "UniqueID"})
DFPetriUniquesHalos=DFPetriUniquesHalos.rename(columns={"IDExperience": "UniqueID"})

# On ajoute des colonnes aux DFMPCUniques pour qu'ils soient mergeables avec le DFPetri : 
for dataF in DFMPCUniqueFluo_2, DFMPCUniqueScatt, DFMPCUniqueFluo_1:
    for rowIndex, row in dataF.iterrows(): 
        dataF.at[rowIndex, 'EXP'] = dataF.at[rowIndex, 'EXP'][5:-5] 
        dataF.at[rowIndex, 'UniqueID']=str('{} - {} - {}').format(
            invertDate(dataF.at[rowIndex, 'EXP']),
            dataF.at[rowIndex, 'Sample'],
            dataF.at[rowIndex, 'Dilution'])
        dataF.at[rowIndex, 'sigma']=sqrt(row['sigmaSquare'])

for rowIndex, row in DFPetriUniques.iterrows(): 
    DFPetriUniques.at[rowIndex, 'dilution']=DFPetriUniques.at[rowIndex, 'dilution'].replace('/', '_')
    DFPetriUniques.at[rowIndex, 'UniqueID']
    DFPetriUniques.at[rowIndex, 'UniqueID'] += ' - {}'.format(DFPetriUniques.at[rowIndex, 'dilution']) 

for rowIndex, row in DFPetriUniquesHalos.iterrows(): 
    DFPetriUniquesHalos.at[rowIndex, 'dilution']=DFPetriUniquesHalos.at[rowIndex, 'dilution'].replace('/', '_')
    DFPetriUniquesHalos.at[rowIndex, 'UniqueID']
    DFPetriUniquesHalos.at[rowIndex, 'UniqueID'] += ' - {}'.format(DFPetriUniquesHalos.at[rowIndex, 'dilution']) 

DFFinalFluo_2 = pd.merge(DFMPCUniqueFluo_2, DFPetriUniques, how='inner', on = 'UniqueID', copy=True)
DFFinalFluo_2=DFFinalFluo_2.drop(['dilution', 'sol'], axis = 1)
DFFinalFluo_2 = DFFinalFluo_2[(DFFinalFluo_2.petriColoniesParMl.notna()) & (DFFinalFluo_2.MPC.notna()) & (DFFinalFluo_2.petriColoniesParMl > 20) ].reset_index(drop=True)

DFFinalScatt=pd.merge(DFMPCUniqueScatt, DFPetriUniquesHalos, how='inner', on = 'UniqueID', copy=True)
DFFinalScatt=DFFinalScatt.drop(['dilution', 'sol'], axis = 1)
#On enlève les NA : 
DFFinalScatt = DFFinalScatt[(DFFinalScatt.petriHalosParMl.notna()) & (DFFinalScatt.MPC.notna()) ].reset_index(drop=True)

DFFinalFluo_1=pd.merge(DFMPCUniqueFluo_1, DFPetriUniquesHalos, how='inner', on = 'UniqueID', copy=True)
DFFinalFluo_1=DFFinalFluo_1.drop(['dilution', 'sol'], axis = 1)
#On enlève les NA : 
DFFinalFluo_1 = DFFinalFluo_1[(DFFinalFluo_1.petriHalosParMl.notna()) & (DFFinalFluo_1.MPC.notna()) ].reset_index(drop=True)

#On fusionne les trois DFFinal
DFMerged=pd.merge(DFFinalFluo_2, DFFinalScatt, on = ('UniqueID'), suffixes=(None, 'Scatt'))
DFMerged=DFMerged.drop(['EXPScatt', 'DilutionScatt','SampleScatt','concentrationScatt'], axis = 1)
DFMerged=pd.merge(DFMerged, DFFinalFluo_1, on = ('UniqueID'), suffixes=(None, 'Fluo_1'))
DFMerged=DFMerged.drop(['EXPFluo_1', 'DilutionFluo_1','SampleFluo_1','concentrationFluo_1'], axis = 1)

#On calcule les fractions des solubilisateurs pour chaque mode de culture : 
for i, row in DFMerged.iterrows():
    DFMerged.at[i, 'fractionMD']=row['MPCScatt']/row['MPC']
    DFMerged.at[i, 'fractionPetri']=row['petriHalosParMl']/row['petriColoniesParMl']
    DFMerged.at[i, 'fractionMDF1']=row['MPCFluo_1']/row['MPC']


del DFMPC, DFMPCScatt, DFMPCUniqueFluo_2, DFMPCUniqueScatt, DFPetriUniques, DFPetriUniquesHalos, dataF

# %%
# ---------------------------------------------------
# ------------ Régressions linéaires ----------------
# On mène des régressions pour savoir si le nombre de halos corrèle avec le nombre 
# de gouttes qui font chutter le pH ou de gouttes qui font chutter le scattering. 
# Les r&égressions sont faites sur des données passées en log10

metaParam=reload(metaParam)
arraySoils=DFFinalFluo_2.Sample.unique()

#On génére des DataFrame qui serviront à lancer les régressions (on enlève les zéros pour le passage en log) :
DFRegressionCFU = DFFinalFluo_2[(DFFinalFluo_2.petriColoniesParMl > 0) * (DFFinalFluo_2.MPC > 0)]
DFRegressionScatt = DFFinalScatt[(DFFinalScatt.petriHalosParMl > 0) * (DFFinalScatt.MPC > 0)]
DFRegressionFluo_1 = DFFinalFluo_1[(DFFinalFluo_1.petriHalosParMl > 0) * (DFFinalFluo_1.MPC > 0)]

PetriAxis, PetriAxisScatt, PetriAxisFluo_1 = log10(DFRegressionCFU.petriColoniesParMl), log10(DFRegressionScatt.petriHalosParMl), log10(DFRegressionFluo_1.petriHalosParMl)
MDAxis, MDAxisScatt, MDAxisFluo_1 = log10(DFRegressionCFU.MPC), log10(DFRegressionScatt.MPC), log10(DFRegressionFluo_1.MPC)

#On lance les régressions : 
popt, pcov = curve_fit(linear_func, PetriAxis,MDAxis) #Colonies VS positifs résa
poptS, pcovS = curve_fit(linear_func, PetriAxisScatt, MDAxisScatt) #halos Vs Scattering
poptF1, pcovF1 = curve_fit(linear_func, PetriAxisFluo_1, MDAxisFluo_1) #halos Vs fluo_1

#On calcule les R2 :
RdeuxCFU = (1-((MDAxis-PetriAxis*popt)**2).sum()/((MDAxis-MDAxis.mean())**2).sum()).round(3)
RdeuxHalos = (1-((MDAxisScatt-PetriAxisScatt*poptS)**2).sum()/((MDAxisScatt-MDAxisScatt.mean())**2).sum()).round(3)
RdeuxF1 = (1-((MDAxisFluo_1-PetriAxisFluo_1*poptF1)**2).sum()/((MDAxisFluo_1-MDAxisFluo_1.mean())**2).sum()).round(3)

# -------------------------------------------------------
# Affichage des fractions. Cette partie doit être déplacée. 

idShape=0

arrayOfDilutions=DFFinalFluo_2.Dilution.unique()
if metaParam.remove10000:
    arrayOfDilutions=arrayOfDilutions[arrayOfDilutions != '1/10 000']

dictOfDilutions={}

for element in arrayOfDilutions:
    dictOfDilutions[element]=idShape
    idShape+=1

figCFUVSRésa = go.Figure(layout=go.Layout(
    template='seaborn', font=metaParam.taillePolice,
    width=metaParam.W, height=metaParam.H,
    xaxis=dict(zeroline=False, title='CFU par ml - Petri '),
    yaxis=dict(zeroline=False, title = 'Positifs résazurine par ml - gouttes'),
    title={'text': 'Colonies par ml (Petri) VS positifs résazurine par ml gouttes (logarithme base 10)'}))

figHomoS, figHalos= go.Figure(figCFUVSRésa), go.Figure(figCFUVSRésa)

# figFractions=make_subplots(rows =2, cols = 2,
#     vertical_spacing=0.1,shared_yaxes=True, shared_xaxes=True,
#         subplot_titles=(
#             "Fraction de solubilisateurs Petri à 1:100",
#             "Fraction de solubilisateurs Petri à 1:1000",
#             "Fraction de solubilisateurs MilliDrop à 1:100",
#             "Fraction de solubilisateurs MilliDrop à 1:1000"
#         )
#     )

figHomoS.update_layout(
    title={'text': 'Résidus de la régression linéaire'},
    xaxis=dict(zeroline=False, dtick=1, title='CFU par ml - Petri '),
    yaxis=dict(zeroline=False, dtick=1, title = 'Résidus'))
figHalos.update_layout(
    title={'text': 'Halos par ml (Petri) VS chutes scattering par ml gouttes'},
    xaxis=dict(zeroline=False, title='Halos par ml - Petri '),
    yaxis=dict(zeroline=False, title = 'Chuttes de scattering par ml - gouttes '))

figHalosVSF1Log, figHalosLog, figHalos5gl, figHalos1gl, figHalosF1 = go.Figure(figHalos), go.Figure(figHalos), go.Figure(figHalos), go.Figure(figHalos), go.Figure(figHalos)

figHalos5gl.update_layout(
    title={'text': 'Halos par ml (Petri) VS chutes scattering par ml gouttes - 5g/l'})

figHalos1gl.update_layout(
    title={'text': 'Halos par ml (Petri) VS chutes scattering par ml gouttes - 1g/l'})

figHalosF1.update_layout(
    title={'text': 'Halos par ml (Petri) VS chutes du pH'})

figHalosLog.update_layout(
    title={'text': 'Halos par ml (Petri) VS chutes scattering par ml gouttes - log10'}, 
    xaxis=dict(zeroline=False, dtick=1, title='Halos par ml - Petri '),)

figHalosVSF1Log.update_layout(
    title={'text': 'Halos par ml (Petri) VS chutes du pH par ml gouttes - log10'}, 
    xaxis=dict(zeroline=False, dtick=1, title='Halos par ml - Petri '),
    yaxis=dict(zeroline=False, dtick=1, title='chutes du pH par ml - gouttes '),)

queryString="Sample == @sol & Dilution == @dilut"

for sol in arraySoils:
    for dilut in dictOfDilutions:
        queriedDF=DFFinalFluo_2.query(queryString)
        queriedDFFractions=DFMerged.query(queryString)

        extraInfoFluo_2=queriedDF['EXP']
        xAxis, yAxis = log10(queriedDF['petriColoniesParMl']), log10(queriedDF['MPC'])

        axisFracMD, axisFracPetri = queriedDFFractions['fractionMD']*100, queriedDFFractions['fractionPetri']*100

        # figFractions.add_trace(go.Box(
        #     y=axisFracPetri, name=sol, 
        #     boxpoints='all', jitter=0.3,
        #     marker_color=metaD.dictOfSamplesColors[sol]),
        #     row=1, col=1 if dilut == '1_100' else 2)

        # figFractions.add_trace(go.Box(
        #     y=axisFracMD, name=sol,
        #     boxpoints='all', jitter=0.3,
        #     marker_color=metaD.dictOfSamplesColors[sol]),
        #     row=2, col=1 if dilut == '1_100' else 2)

        figCFUVSRésa.add_trace(go.Scatter(
            x=xAxis, y=yAxis,
            mode='markers',
            text=extraInfoFluo_2,
            marker=dict(color=metaD.dictOfSamplesColors[sol], size=12),
            error_y=dict(type='data', symmetric=False,
                array=log10(queriedDF['MPC']*exp(2*queriedDF['sigma'])) - yAxis,
                arrayminus=yAxis - log10(queriedDF['MPC']*exp(-2*queriedDF['sigma']))),
            error_x=dict(type='data', symmetric=False,
                array=log10(queriedDF['valeurConf95HautPetriCFU'])-xAxis,
                arrayminus=xAxis-log10(queriedDF['valeurConf95BasPetriCFU'])),
            hovertemplate="EXP%{text}<br>"+sol+"<br>Dilution : "+dilut,
            legendgroup=sol,
            name=sol,
            showlegend=False if dilut != "1/100" else True,
            marker_symbol=201+dictOfDilutions[dilut]
            # name=str("{} {}".format(sol,str(dilut)))
            ))
        
        figHomoS.add_trace(go.Scatter(
            x=xAxis, y=yAxis-xAxis*popt, mode='markers',
            marker=dict(color=metaD.dictOfSamplesColors[sol], size=12),
            marker_symbol=201+dictOfDilutions[dilut],
            name=str("{} {}".format(sol,str(dilut)))
            ))

        # La partie Phosphore : 
        queriedDFScatt=DFFinalScatt.query(queryString)
        queriedDFFluo_1=DFFinalFluo_1.query(queryString)
        queriedDFScatt1gl=DFFinalScatt.query(queryString+' & concentration == 1')
        queriedDFScatt5gl=DFFinalScatt.query(queryString+' & concentration == 5')

        #aS : array Scattering
        for aS in [queriedDFScatt, figHalos], [queriedDFScatt1gl, figHalos1gl], [queriedDFScatt5gl, figHalos5gl], [queriedDFFluo_1, figHalosF1]:

            xAxisS=aS[0]['petriHalosParMl']
            yAxisS=aS[0]['MPC']
            extraInfo=aS[0]['EXP']

            aS[1].add_trace(go.Scatter(
                x=xAxisS, y=yAxisS,
                text=extraInfo,
                mode='markers',
                showlegend=False,
                name=str("{} {}".format(sol,str(dilut))),
                marker=dict(color=metaD.dictOfSamplesColors[sol],size =12),
                error_y=dict(type='data', symmetric=False,
                    array=aS[0]['MPC']*exp(2*aS[0]['sigma'])-aS[0]['MPC'],
                    arrayminus=aS[0]['MPC']-aS[0]['MPC']*exp(-2*aS[0]['sigma'])),
                error_x=dict(type='data', symmetric=False,
                    array=aS[0]['valeurConf95HautPetriHalos']-aS[0]['petriHalosParMl'],
                    arrayminus=aS[0]['petriHalosParMl']-aS[0]['valeurConf95BasPetriHalos']),
                hovertemplate='EXP20%{text}<br>%{y} cellules/ml<br>Petri : %{x}<br>Sol : '+sol,
                marker_symbol=201+dictOfDilutions[dilut]))
        
        #On trace les graphes halos VS scattering ou fluo 1, mais après être passé en log. On leur ajoute une droite pour représenter la régression linéaire
        querriedDFRegressionScatt=DFRegressionScatt.query(queryString)
        xAxisSlog, yAxisSlog= log10(querriedDFRegressionScatt['petriHalosParMl']), log10(querriedDFRegressionScatt['MPC'])
        arrayErrorMDScatt={
            'haut' : log10(querriedDFRegressionScatt['MPC']*exp(2*querriedDFRegressionScatt['sigma'])) - yAxisSlog,
            'bas' : yAxisSlog - log10(querriedDFRegressionScatt['MPC']*exp(-2*querriedDFRegressionScatt['sigma']))}
        arrayErrorPetriScatt={
            'haut' : log10(querriedDFRegressionScatt['valeurConf95HautPetriHalos']) - xAxisSlog, 
            'bas' : xAxisSlog - log10(querriedDFRegressionScatt['valeurConf95BasPetriHalos'])}

        xAxisF1log, yAxisF1log = log10(DFRegressionFluo_1.query(queryString)['petriHalosParMl']), log10(DFRegressionFluo_1.query(queryString)['MPC'])

        figHalosLog.add_trace(go.Scatter(
            x=xAxisSlog, y=yAxisSlog, 
            error_y=dict(type='data', symmetric=False,
                array=arrayErrorMDScatt['haut'],
                arrayminus=arrayErrorMDScatt['bas']),
            error_x=dict(type='data', symmetric=False,
                array=arrayErrorPetriScatt['haut'],
                arrayminus=arrayErrorPetriScatt['bas']),
            mode='markers',
            name=str("{} {}".format(sol,str(dilut))),
            showlegend=False,
            marker=dict(color=metaD.dictOfSamplesColors[sol],size =12),
            marker_symbol=201+dictOfDilutions[dilut]))
        
        del arrayErrorMDScatt, arrayErrorPetriScatt, querriedDFRegressionScatt

        figHalosVSF1Log.add_trace(go.Scatter(
            x=xAxisF1log, y=yAxisF1log, 
            mode='markers',
            name=str("{} {}".format(sol,str(dilut))),
            marker=dict(color=metaD.dictOfSamplesColors[sol],size =12),
            marker_symbol=201+dictOfDilutions[dilut]))

figCFUVSRésa.add_trace(go.Scatter(
    #On place la légende par rapport à la moyenne géométrique
    x=[gmean(PetriAxis)-1], y=[MDAxis.max()-0.8],
    text=[str("Y = {}X<br>R² = {}").format(str(popt[0].round(3)),str(RdeuxCFU))],
    mode="text", showlegend=False,
    textfont=dict(size=16, color="black")))

figHalosLog.add_trace(go.Scatter(
    #On place la légende par rapport à la moyenne géométrique
    x=[gmean(PetriAxisScatt)-1], y=[MDAxisScatt.max()-0.8],
    text=[str("Y = {}X<br>R² = {}").format(str(poptS[0].round(3)),str(RdeuxHalos))],
    mode="text", showlegend=False,
    textfont=dict(size=16, color="black")))

figHalosVSF1Log.add_trace(go.Scatter(
    #On place la légende par rapport à la moyenne géométrique
    x=[gmean(PetriAxisFluo_1)-1], y=[MDAxisFluo_1.max()-0.8],
    text=[str("Y = {}X<br>R² = {}").format(str(poptF1[0].round(3)),str(RdeuxF1))],
    mode="text", showlegend=False,
    textfont=dict(size=16, color="black")))

# fig.update_layout(xaxis_type="log", yaxis_type="log")

figCFUVSRésa.add_shape(
    # Ligne régression linéaire
    dict(type="line",
        x0=PetriAxis.min(), y0=PetriAxis.min()*popt[0],
        x1=PetriAxis.max(), y1=PetriAxis.max()*popt[0],
        name="regression",
        line=dict(color="black",width=3)))

figHalosLog.add_shape(
    # Ligne régression linéaire
    dict(type="line",
        x0=PetriAxisScatt.min(), y0=PetriAxisScatt.min()*poptS[0],
        x1=PetriAxisScatt.max(), y1=PetriAxisScatt.max()*poptS[0],
        name="regression",
        line=dict(color="black",width=3)))

figHalosVSF1Log.add_shape(
    # Ligne régression linéaire
    dict(type="line",
        x0=PetriAxisFluo_1.min(), y0=PetriAxisFluo_1.min()*poptF1[0],
        x1=PetriAxisFluo_1.max(), y1=PetriAxisFluo_1.max()*poptF1[0],
        name="regression",
        line=dict(color="black",width=3)))

# figure résazurine et CFU :
figCFUVSRésa.show(config={'editable': True}, renderer=metaParam.renderer)

#Graphe homoscédasticité
figHomoS.show(config={'editable': True}, renderer=metaParam.renderer)

# # La partie display phosphore
maxAxisHalos=max(DFSoils.solubilisateursParMl_gouttes.max(),DFSoils.petriHalosParMl.max())

# allHaloFigs = [figHalos,figHalos1gl,figHalos5gl, figHalosF1]
allHaloFigs = [figHalos, figHalosF1]

#On ajoute une ligne égalité à toutes les figures halos (hors log)
ligne=dict(# Ligne egalité
    type="line", line=dict(color="red",width=3),
    x0=1, y0=1,
    x1=maxAxisHalos+100, y1=maxAxisHalos+100)
list(map(lambda x:x.add_shape(ligne),allHaloFigs))
del ligne

# On affiche les figures halos (hors log): 
list(map(lambda x:x.show(editable=True, renderer=metaParam.renderer),allHaloFigs))
list(map(lambda x:x.update_layout(xaxis_type="log", yaxis_type="log"),allHaloFigs))
list(map(lambda x:x.show(editable=True, renderer=metaParam.renderer),allHaloFigs))

figHalosLog.show(config={'editable': True}, renderer=metaParam.renderer)
figHalosVSF1Log.show(config={'editable': True}, renderer=metaParam.renderer)

# figFractions.update_yaxes(type="log", row=1, col=1)
# figFractions.update_yaxes(type="log", row=2, col=1)
# figFractions.update_yaxes(type="log", row=1, col=2)
# figFractions.update_yaxes(type="log", row=2, col=2)

# figFractions.show(editable=True, renderer=metaParam.renderer)

# %%
