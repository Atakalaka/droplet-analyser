#%%
#Cellule importation
from importlib import reload

import pandas as pd
import plotly.graph_objects as go
from numpy import log, log10, exp
from scipy.optimize import curve_fit
from scipy.stats import gmean

import functionsArthur as fArt
import metaParametersSheet as metaParam  # paramètres qui ne dépendent pas de l'expérience
import metaDonneesExperiences as metaD

def linear_func(x,a):
    return a*x

matColors=metaParam.matColors

metaParam=reload(metaParam)

DFExcel=pd.DataFrame(pd.read_csv(
    # "C:/Users/Arthur/ownCloud/Divers/PetriVSMDII.csv",
    "/Users/arthurgoldstein/OneDrive/LCMD/PetriVSMD.csv",
    decimal = ',',
    sep=";"))

# %%  Partie Souches pures
# Régression linéaire, avec origine y=x=0
DFStrains=DFExcel[DFExcel.Souche=="VRAI"].reset_index(drop=True).copy()

PetriAxis=log10(DFStrains.petriColoniesParMl)
MDAxis=log10(DFStrains.CellulesCultivablesParMl_gouttes)

poptS, pcovS = curve_fit(linear_func, PetriAxis,MDAxis) 
# sigma=DFStrains.valeurConfBasResa/MDAxis

Rdeux=(1-((MDAxis-PetriAxis*poptS)**2).sum()/((MDAxis-MDAxis.mean())**2).sum()).round(3)

figSouches = go.Figure(layout=go.Layout(
    template='seaborn',
    font=metaParam.taillePolice,
    width=metaParam.W, height=metaParam.H,
    xaxis=dict(zeroline=False,dtick=1 ,title='CFU par ml - Petri '),
    yaxis=dict(zeroline=False,dtick=1, title = 'Positifs résazurine par ml - gouttes'),
    title={'text': 'Colonies par ml Petri VS positifs résazurine par ml en gouttes - souches - log base 10'}))

arraySoils=DFStrains.sol.unique()
arrayOfDilutionsStrains=DFStrains.dilution.unique()
dictOfDilutionsStrains={}

idShape=0
for element in arrayOfDilutionsStrains:
    dictOfDilutionsStrains[element]=idShape
    idShape+=1

idColor=0
for sol in arraySoils:
    for dilut in dictOfDilutionsStrains:

        queryString="sol == @sol & dilution == @dilut"

        queriedDF=DFStrains.query(queryString)

        xAxis=log10(queriedDF['petriColoniesParMl'])
        yAxis=log10(queriedDF['CellulesCultivablesParMl_gouttes'])

        figSouches.add_trace(go.Scatter(
            x=xAxis, y=yAxis,
            mode='markers',
            marker=dict(color=metaD.dictOfSamplesColors[sol], size=14),
            error_y=dict(
                type='data',
                symmetric=False,
                array=log10(queriedDF['valeurConfHautResa'])-log10(queriedDF['CellulesCultivablesParMl_gouttes']),
                arrayminus=log10(queriedDF['CellulesCultivablesParMl_gouttes'])-log10(queriedDF['valeurConfBasResa']),
                visible=True),
            error_x=dict(
                type='data',
                symmetric=False,
                array=log10(queriedDF['valeurConf95HautPetriCFU'])-log10(queriedDF['petriColoniesParMl']),
                arrayminus=log10(queriedDF['petriColoniesParMl'])-log10(queriedDF['valeurConf95BasPetriCFU']),
                visible=True),
            
            # legendgroup=sol,
            marker_symbol=201+dictOfDilutionsStrains[dilut],
            name=str("{} {}".format(sol,str(dilut)))
            ))

    idColor+=1

figSouches.add_trace(go.Scatter(
    #On place la légende par rapport à la moyenne géométrique
    # x=[gmean([PetriAxis.min(),PetriAxis.max()])],
    # y=[gmean([PetriAxis.min()*Rdeux,PetriAxis.max()*Rdeux])],
    x=[(PetriAxis.min()+PetriAxis.max())/2],
    y=[(PetriAxis.min()+PetriAxis.max())*Rdeux/2+0.6],
    text=[str("Y = {}X<br>R² = {}").format(str(poptS[0].round(3)),str(Rdeux))],
    mode="text",
    showlegend=False,
    textfont=dict(
        size=16,
        color="black"
    )
))

figSouches.add_shape(
    # Ligne régression linéaire
    dict(type="line",
        x0=PetriAxis.min(), y0=PetriAxis.min()*poptS[0],
        x1=PetriAxis.max(), y1=PetriAxis.max()*poptS[0],
        line=dict(
            color="black",
            width=3
        )))
# figSouches.update_layout(xaxis_type="log", yaxis_type="log")
# figSouches.show(renderer=metaParam.renderer)

del MDAxis, PetriAxis, Rdeux, dictOfDilutionsStrains, arrayOfDilutionsStrains, figSouches