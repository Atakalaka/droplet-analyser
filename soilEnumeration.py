#%%
#This script tells you how many times each soil has been studied, and at which date

# Comptage des sols étudiés dans les dossiers MD. 

from importlib import reload
from os import listdir
from re import findall

import plotly.graph_objects as go
import metaParametersSheet as metaParam

listExperiments={}
searchStringSoil="\{.{1,19}\}" #entre 1 et 19 caractères entourés de deux accolades

#On liste les expériences
folders=[]

for rootFolder in metaParam.rootFolders:
    folders = folders + [f for f in listdir(rootFolder)]    

for nomExperience in folders:
    #Si l'expèrience contient 'CONTAMINE', on ne la compte pas
    if not ("CONTAMINE" in nomExperience or "ECHEC" in nomExperience or "ALIGNEMENT" in nomExperience):
        #On crée un array avec tous les noms entre accolades (1 ou 2 en général)
        arrayNames=findall(searchStringSoil,nomExperience)

        #On enlève les accolades
        i=0
        length=len(arrayNames)
        while i<length: 
            arrayNames[i]=arrayNames[i][1:-1]
            i+=1
        
        for soil in arrayNames:
            if soil in listExperiments:
                listExperiments[soil].append(nomExperience[3:11])
            else:
                listExperiments[soil]=[nomExperience[3:11]]


for key, value in sorted(listExperiments.items()):
    print(key, ' : ', value)

# %%
